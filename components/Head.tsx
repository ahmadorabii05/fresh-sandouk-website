import Head from 'next/head';
import React from "react";

function AppHead() {
  return (
    <>
      <Head>
        <title>Buy Groceries online in Dubai, UAE</title>
        <meta name="description" content="Kibsons, Buy Groceries online in Dubai, UAE" />
      </Head>
      <div>
      </div>
    </>
  );
}

export default AppHead
