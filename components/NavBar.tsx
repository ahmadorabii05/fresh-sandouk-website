import React, { useState, useEffect } from "react";

import ContainerRow from "./containerRow";
import DesktopNavBar from "./desktopNavBar";
import MobileNavBar from "./mobileNavBar";
import Categories from "./categories";
import { useSelector } from "react-redux";
import { getCurrentUser } from "@/network/auth";
import { useDispatch } from "react-redux";
import { saveUser } from "@/store/reducers/userReducer";

function NavBar() {
  const [scrolled, setScrolled] = useState(false);
  const dispatch = useDispatch()
  useEffect(() => {
    getCurrentUser().then((res:any)=>{
      if(res && res.data && res.data.data){
        const user = res.data.data
        dispatch(saveUser({user}))
      }
    })
    const handleScroll = () => {
      if (window.pageYOffset > 170) {
        setScrolled(true);
      } else {
        setScrolled(false);
      }
    };
    window.addEventListener("scroll", handleScroll);
    return () => window.removeEventListener("scroll", handleScroll);
  }, []);

  return (
    <div className="nav-wrapper-main">
      <div className="only-mobile">
        <div style={{ height: "50px" }}></div>
        <MobileNavBar />
      </div>
      <div className="only-desktop">
        <div className={`sticky-nav${scrolled ? " scrolled" : ""}`}>
      <ContainerRow />
          <div className="container">
            <DesktopNavBar />
            {scrolled && (
              <div className="nav-bottom-menu">
                <div className="container">
                  <Categories onlyLabels={true} />
                </div>
              </div>
            )}
          </div>
        </div>
      </div>
    </div>
  );
}

export default NavBar;
