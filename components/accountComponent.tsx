import React from "react";
import { NextPage } from "next";
import { useRouter } from "next/router";
import heartImage from "../public/images/heart.png";
import shoppingList from "../public/images/shoppingList.png";
import shoppingCard from "../public/images/shoppingCard.png";
import vouchers from "../public/images/vouchers.png";
import wallet from "../public/images/wallet.png";
import statement from "../public/images/statement.png";
import orders from "../public/images/orders.png";
import regulars from "../public/images/regulars.png";

import accountpoints from "../public/images/accountpoints.png";
import rewardpoints from "../public/images/rewardpoints.png";
import referFriends from "../public/images/referFriends.png";
import lock from "../public/images/lock.png";
import user from "../public/images/user.png";
import address from "../public/images/address.png";
import deleteAccountIcon from "../public/images/delete.png";

import returnRecycle from "../public/images/return.png";

import win from "../public/images/win.png";

import Image from "next/image";
import Categories from "./categories";
import AccountMenu from "./accountMenu";
import withAuth from "@/helpers/withAuth";

const Accountcomponent = ({ Component, title }: any) => {
  const WrappedComponent: NextPage = () => {
    return (
      <div className="wrapper">
        <div className="only-mobile">
        </div>
        <div className="only-desktop">
        <Categories />
        </div>
        <div className="d-flex">
          <div className="only-desktop left-section">
            <AccountMenu />
          </div>
          <div className="right-section">
            <Component icon={heartImage} />
          </div>
        </div>
        <br />
        <br />
      </div>
    );
  };

  return withAuth(WrappedComponent);
};


export default  Accountcomponent;
