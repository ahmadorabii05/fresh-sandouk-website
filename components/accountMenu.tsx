import React from "react";
import { NextPage } from "next";
import { useRouter } from "next/router";
import heartImage from "../public/icons/My Favorites.png";
import shoppingList from "../public/icons/My Shopping List.png";
import shoppingCard from "../public/icons/My Shopping Cart.png";
import vouchers from "../public/images/vouchers.png";
import wallet from "../public/images/wallet.png";
import statement from "../public/images/statement.png";
import orders from "../public/icons/My Orders.png";
import regulars from "../public/images/regulars.png";
import accountpoints from "../public/images/accountpoints.png";
import rewardpoints from "../public/images/rewardpoints.png";
import referFriends from "../public/images/referFriends.png";
import lock from "../public/icons/Change Password.png";
import user from "../public/icons/Update My Profile.png";
import address from "../public/icons/My address.png";
import deleteAccountIcon from "../public/icons/Delete Account.png";
import returnRecycle from "../public/images/return.png";
import win from "../public/images/win.png";
import Image from "next/image";
import Categories from "./categories";
import useTranslation from "next-translate/useTranslation";

function AccountMenu() {
  const router = useRouter();
  const {t} = useTranslation('common')
  return (
    <>
      <div className="">
        <div className="acc-left-menu">{t("My Baskets")}</div>
        <div
          className="acc-sub-menu"
          onClick={() => router.push("/account/myfavorites")}
        >
          <Image className="small-image" src={heartImage} alt={t("heartImage")} />
          <span className="mx-1"></span>
          <span>{t("My Favorites")}</span>
        </div>
        <div
          className="acc-sub-menu"
          onClick={() => router.push("/account/myshoppinglist")}
        >
          <Image className="small-image" src={shoppingList} alt={t("shoppingList")} />
          <span className="mx-1"></span>
          <span>{t("My Shopping List")}</span>
        </div>
        <div

          className="acc-sub-menu"
          onClick={() => router.push("/myshopcart")}
        >
          <Image className="small-image" src={shoppingCard} alt={t("shopping card")}  />
          <span className="mx-1"></span>
          <span>{t("My Shopping Cart")}</span>
        </div>
        {/* <div
          className="acc-sub-menu"
          onClick={() => router.push("/account/myvouchers")}
        >
          <Image className="small-image" src={vouchers} alt={t("voucher")}/>
          <span className="mx-1"></span>
          <span>{t("My Vouchers")}</span>
        </div> */}
        <div className="acc-left-menu">{t("ORDER INFO")}</div>
        {/* <div
          className="acc-sub-menu"
          onClick={() => router.push("/account/mywallet")}
        >
          <Image className="small-image" src={wallet}  alt={t("wallet")}/>
          <span className="mx-1"></span>
          <span>{t("My Wallet")}</span>
        </div> */}
        {/* <div
          className="acc-sub-menu"
          onClick={() => router.push("/account/mystatement")}
        >
          <Image className="small-image" src={statement} alt={t("statement")} />
          <span className="mx-1"></span>
          <span>{t("My Statement")}</span>
        </div> */}
        <div
          className="acc-sub-menu"
          onClick={() => router.push("/account/myorders")}
        >
          <Image className="small-image" src={orders} alt={t("my orders")} />
          <span className="mx-1"></span>
          <span>{t("My Orders")}</span>
        </div>
        {/* <div
          className="acc-sub-menu"
          onClick={() => router.push("/account/myregulars")}
        >
          <Image className="small-image" src={regulars} alt={t("my regulars")} />
          <span className="mx-1"></span>
          <span>{t("My Regulars")}</span>
        </div> */}
        {/* <div className="acc-left-menu">{t("MY PRIVILEGES")}</div> */}
        {/* <div
          className="acc-sub-menu"
          onClick={() => router.push("/account/myaccountpoints")}
        >
          <Image className="small-image" src={accountpoints} alt={t("my previllages")}/>
          <span className="mx-1"></span>
          <span>{t("Account Points")}</span>
        </div> */}
        {/* <div
          className="acc-sub-menu"
          onClick={() => router.push("/account/myrewardpoints")}
        >
          <Image className="small-image" src={rewardpoints} alt={t("reward points")} />
          <span className="mx-1"></span>
          <span>{t("Reward Points")}</span>
        </div> */}
        {/* <div
          className="acc-sub-menu"
          onClick={() => router.push("/account/refertofriends")}
        >
          <Image className="small-image" src={referFriends} alt={t("refer to friend")} />
          <span className="mx-1"></span>
          <span>{t("Refer Friends")}</span>
        </div> */}
        {/* <div
          className="acc-sub-menu"
          onClick={() => router.push("/account/returnbox")}
        >
          <Image className="small-image" src={returnRecycle} alt={t("recycle")} />
          <span className="mx-1"></span>
          <span>{t("Return & Recycle Service")}</span>
        </div> */}
        {/* <div className="acc-left-menu">{t("Competition")}</div>
        <div
          className="acc-sub-menu"
          onClick={() => router.push("/competition")}
        >
          <Image className="small-image" src={win} alt={t("trip")} />
          <span className="mx-1"></span>
          <span>{t("Win a trip to South Africa")}</span>
        </div> */}
        <div className="acc-left-menu">{t("User Profile")}</div>
        <div
          className="acc-sub-menu"
          onClick={() => router.push("/account/myaddresses")}
        >
          <Image className="small-image" src={address} alt={t("addresses")} />
          <span className="mx-1"></span>
          <span>{t("My Addresses")}</span>
        </div>
        <div
          className="acc-sub-menu"
          onClick={() => router.push("/account/myprofile")}
        >
          <Image className="small-image" src={user} alt={t("user")} />
          <span className="mx-1"></span>
          <span>{t("Update My Profile")}</span>
        </div>
        <div
          className="acc-sub-menu"
          onClick={() => router.push("/account/changepassword")}
        >
          <Image className="small-image" src={lock} alt={t("lock")} />
          <span className="mx-1"></span>
          <span>{t("Change Password")}</span>
        </div>
        <div
          className="acc-sub-menu"
          onClick={() => router.push("/account/deleteaccount")}
        >
          <Image className="small-image" src={deleteAccountIcon} alt={t("Delete Account")} />
          <span className="mx-1"></span>
          <span>{t("Delete Account")}</span>
        </div>
      </div>
    </>
  );
}

export default  AccountMenu;
