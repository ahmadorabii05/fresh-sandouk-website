 

import React from "react";
import Image from 'next/image'
import mobileFooterHome from '../public/images/mobileFooterHome.svg'
import mobileFooterHeart from '../public/images/mobileFooterHeart.svg'
import mobileFooterSearch from '../public/images/mobileFooterSearch.svg'
import mobileFooterCart from '../public/images/mobileFooterCart.svg'
import mobileFooterUser from '../public/images/mobileFooterUser.svg'
import { useSelector } from "react-redux";
import Link from 'next/link'

function BottomMobileMenu({isLoading = true}) {
    const cartItems = useSelector((state:any) => state.cart.cartItems);
    const numItemsInCart = cartItems.length;
  return (
    <div className="bottom-mobile-menu">
        <div className="bg-blue bg-navy d-flex justify-content-between">
            <Link href={"/"} className="ptr mobile-menu-icon-btn">
                <Image src={mobileFooterHome} alt="mobileFooterHome" />
            </Link>
            <Link href={"/account/myfavorites"} className="ptr mobile-menu-icon-btn">
                <Image src={mobileFooterHeart} alt="mobileFooterHeart" />
            </Link>
            <Link  href={"/searchPage"}  className="ptr mobile-menu-icon-btn">
                <Image src={mobileFooterSearch} alt="mobileFooterSearch" />
            </Link>
            <Link  href={"/myshopcart"} className="ptr mobile-menu-icon-btn">
                <div className="count-badge">{numItemsInCart}</div>
                <Image src={mobileFooterCart} alt="mobileFooterCart" />
            </Link>
            <Link  href={"/account"} className="ptr mobile-menu-icon-btn">
                <Image src={mobileFooterUser} alt="mobileFooterUser" />
            </Link>
        </div>
    </div>
  );
}

export default BottomMobileMenu;
