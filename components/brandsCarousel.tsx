import React,{useEffect,useState} from "react";
import { Carousel } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import { useSelector } from "react-redux";
import Image from 'next/image'

const BrandsCarousel = () => {
  const [groups,setGroups]:any = useState([]);
  const brands:any = useSelector((state: any) => state.home.brands);
  let i = 0;


  useEffect(()=>{
    // if(!groups.length){
    //   let grps:any = []
    //   while (i < brands.length) {
    //     grps.push(brands.slice(i, i + 60));
    //     i += 60;
    //   }
    //   setGroups(grps)
    // }
  })
  return (
    <Carousel>
      {groups.map((group:any, index:any) => {
        return (
          <Carousel.Item key={index}>
            <div className="carousel-page-container">
              {group.map((brand: any, idx: any) => (
                <div className="carousel-brand" key={idx}>
                    <img src={brand.image} alt={brand.image} />
                  <div className="brand-name" key={idx}>{brand.name}</div>
                </div>
              ))}
            </div>
          </Carousel.Item>
        );
      })}
    </Carousel>
  );
};

export default BrandsCarousel;
