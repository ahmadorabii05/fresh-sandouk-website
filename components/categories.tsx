import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { useRouter } from "next/router";
import Image from "next/image";
import axios from "axios";
import { getAllCategories, getAllTags } from "@/network/categories";
import prepareErrorResponse from "@/errorHandlerLayer/errorHandler";
import { toast } from "react-toastify";

function Categories({ onlyLabels = false }) {
  const [activeIndex, setActiveIndex] = useState<number | null>(null);
  const [categories, setCategories] = useState([]);
  const [tags, setTags] = useState([]);
  const router = useRouter();

  // const getColorByIndex = (index: number) => {
  //   const colorMap: any = {
  //     0: "#FF6384", // Raspberry Pink
  //     1: "#FFCE56", // Mango Yellow
  //     2: "#36A2EB", // Blueberry Blue
  //     3: "#4BC0C0", // Honeydew Green
  //     4: "#FF9F40", // Orange Orange
  //     5: "#9370DB", // Grape Purple
  //     6: "#32CD32", // Kiwi Green
  //     7: "#FFD700", // Pineapple Yellow
  //     8: "#FF69B4", // Watermelon Pink
  //     9: "#00CED1", // Cantaloupe Teal
  //     10: "#8B4513", // Coconut Brown
  //     11: "#8A2BE2", // Plum Purple
  //     12: "#3CB371", // Lime Green
  //     13: "#FF7F50", // Coral Orange
  //     14: "#6A5ACD", // Blueberry Dark Blue
  //     15: "#FFA500", // Mandarin Orange
  //     16: "#9932CC", // Fig Purple
  //     17: "#2E8B57", // Avocado Green
  //     18: "#FF6347", // Tomato Red
  //     19: "#48D1CC", // Honeydew Medium Blue
  //   };

  //   return colorMap[index] || "black"; // Default to black if index is not in the map
  // };

  const handleMouseEnter = (index: number) => {
    setActiveIndex(index);
  };

  const handleMouseLeave = () => {
    setActiveIndex(null);
  };

  useEffect(() => {
    getAllCategories()
      .then((res) => {
        const docs: any = res.data.data.docs;
        const alldocs: any = [...docs];
        setCategories(alldocs);
        getAllTags()
          .then((res) => {
            const docs = res.data.data.docs;
            setTags(docs);
          })
          .catch((err) => {
            toast.error(prepareErrorResponse(err));
          });
      })
      .catch((err) => { });


  }, []);


  const getCategories = () => {
    return (
      <div className="categories-array">
        {[...categories].map((category: any, index: any) => {
          return (
            <div
              style={{
                backgroundColor: category.color,
              }}
              className="category-item"
              key={index}
              onMouseEnter={() => handleMouseEnter(index)}
              onMouseLeave={handleMouseLeave}
            >
              <div
                className="p-relative text-center"
                onClick={() => router.push("/category/" + category.id)}
              >
                {!onlyLabels ? (
                  <img
                    src={category.imageUrl}
                    alt="Category image"
                    className={onlyLabels ? "cat-img" : "cat-img"}
                  />
                ) : (
                  ""
                )}
                <div className="catname text-center">{category.name}</div>
                {activeIndex === index && (
                  <div
                    style={{ color: category.color }}
                    className="cat-menu-arrow"
                  ></div>
                )}
              </div>

              {activeIndex === index && (
                <div className="menu-pop">
                  <div className="row w-100">
                    <div className="col-md-2 p-3 pl-5">
                      {tags &&
                        tags.map((tag: any, itemIndex: any) => (
                          <div
                            key={itemIndex}
                            className="collection-item"
                            onClick={() =>
                              router.push(
                                "/category/shopping/tag/" +
                                category.id +
                                "/" +
                                tag.name
                              )
                            }
                          >
                            <img
                              className="coll-img"
                              src={tag.imageUrl}
                              alt="image"
                            />
                            <div className="coll-name">{tag.name}</div>
                          </div>
                        ))}
                    </div>
                    <div className="col-md-10 p-3 items-container-grid-4">
                      {category.subCategories &&
                        [
                          ...category.subCategories,



                        ].map((subcategory: any, itemIndex: any) => (
                          <div
                            key={itemIndex}
                            className="item-item mx-2"
                            onClick={() =>
                              router.push(
                                "/category/shopping/subcategory/" +
                                category.id +
                                "/" +
                                subcategory.id
                              )
                            }
                          >
                            <img
                              className={"item-img "}
                              src={subcategory.imageUrl}
                              alt={"Item Image"}
                            />
                            <div className="item-name">{subcategory.name}</div>
                          </div>
                        ))}
                    </div>
                  </div>
                </div>
              )}
            </div>
          );
        })}
      </div>
    );
  };

  return (
    <div className="component-container-alt">
      <div className="categories-container mb-2">{getCategories()}</div>
    </div>
  );
}

export default Categories;
