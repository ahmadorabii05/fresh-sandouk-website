import Image from "next/image";

import React, {useEffect, useState} from "react";
import orderNow from "../public/icons/Order Now and get your delivery today.png";
import phoneIcon from "../public/icons/Phone .png";
import deliveryIcon from "../public/icons/Free Delivery All Over UAE.png";
import facebookIcon from "../public/icons/facebook.png";
import instagramIcon from "../public/icons/instagram.png";
import userIcon from "../public/icons/My Account.png";
import Link from "next/link";
import {useDispatch, useSelector} from "react-redux";
import Cookies from "js-cookie";
import {useRouter} from 'next/router'
import {signoutUser} from "@/store/reducers/userReducer";

import useTranslation from "next-translate/useTranslation";

const ContainerRow = (props: any) => {
    const {t} = useTranslation('common');

    const [isOpen, setIsOpen] = useState(true);
    const [user, setUser]: any = useState({user: {id: ''}})
    const [locale, setLocale] = useState(Cookies.get('language') || 'en');
    const userRef = useSelector((state: any) => state.user.user)
    const dispatch: any = useDispatch();
    const router = useRouter();
    const toggleDropdown = () => {
        setIsOpen(!isOpen);
    };
    useEffect(() => {
        setUser(userRef)
    }, []);


    useEffect(() => {
        setUser(userRef)
            //  const newLocale = Cookies.get('language') === 'en' ? 'ar' : 'en';
      //  Cookies.set('language', newLocale);
    });

    const signOut = () => {
        dispatch(signoutUser({}))
    }


    const changeLocale = () => {
        const l = router.locale == 'en' ? 'ar' : 'en'
        if (l == "en") {
            document.documentElement.classList.remove('arabic-locale');
        } else {
            document.documentElement.classList.add('arabic-locale');
        }
        Cookies.set('language', l);
        router.push(router.asPath, undefined, {locale: l});
    };

    return (
        <>
            <div className="container-row header-text images-new">
                <div className=" w-20 d-flex v-center" style={{backgroundColor: '#78af2b'}}>
                    <div className="label-input-icon">
                        <Image className="cns-image" alt="image" src={orderNow}/>
                    </div>
                    <div>
                        <div>{t('orderNow')}</div>
                        <div>{t('same day free delivery')}</div>
                    </div>
                </div>
                <div className="middle-items w-80">
                    <div className="tag bg-navy" style={{backgroundColor: '#ffd631'}}>
                        <div className="label-input-icon">
                            <Image className="cns-image" alt="image" src={deliveryIcon}/>
                        </div>
                        <div>{t('Free delivery all over UAE')}</div>
                    </div>
                    <div className="divider-container"></div>

                    <div className="tag social" style={{backgroundColor: '#e73331'}}>
                        <div className="label-input-icon fb">
                            <a href="https://www.facebook.com/freshsandouk" target="_blank">
                                <Image className="cns-image" alt="facebook" src={facebookIcon}/>
                            </a>
                        </div>
                        <div className="label-input-icon insta">
                            <a href="https://www.instagram.com/freshsandouk" target="_blank">
                                <Image className="cns-image" alt="instagram" src={instagramIcon}/>
                            </a>
                        </div>
                        <div className="curser-pointer"
                             onClick={changeLocale}>{router.locale === 'en' ? 'العربية' : 'English'}</div>
                    </div>
                    <div className="divider-container"></div>
                    <div className="tag bg-navy phone" style={{backgroundColor: '#bad25f'}}>
                        <a href="#contact-footer">
                            <div className="d-flex v-center">

                                <div className="label-input-icon phone">
                                    <Image className="cns-image" alt="phone" src={phoneIcon}/>
                                </div>
                                <div>{t('800Kibsons')}</div>
                            </div>
                        </a>
                    </div>
                    <div className="divider-container"></div>

                    {(user && user.id) ?
                        <div className="tag bg-navy dropdown" section-box style={{backgroundColor: '#f39323'}}>
                            <button className="dropbtn">
                                <div className="label-input-icon user">
                                    <Image className="cns-image" alt="login" src={userIcon}/>
                                </div>
                                {user.info.general.name}
                            </button>
                            <div className="dropdown-content">
                                <Link href={''} onClick={signOut}>{t('Logout')}</Link>
                            </div>
                        </div>
                        :
                        <div className="tag dropdown section-box" style={{backgroundColor: '#f39323'}}>
                            <button className="dropbtn">
                                <div className="label-input-icon user">
                                    <Image className="cns-image" alt="login" src={userIcon}/>
                                </div>
                                {t('LOGIN / REGISTER')}
                            </button>
                            <div className="dropdown-content">
                                <Link href="/auth/login">{t('Login')}</Link>
                                <Link href="/auth/register">{t('Register')}</Link>
                            </div>
                        </div>
                    }

                    <div className="tag bg-red section-box px-4 text-center " style={{
                        backgroundColor: '#a30b30',
                        maxWidth: '140px',
                        minWidth: '140px',
                        fontSize: 'x-small'
                    }}>
                        {t('We Think Inside Your Box')}
                    </div>
                </div>
            </div>
        </>
    );
}

export default ContainerRow;

export const getStaticProps = async (context: any) => {
    return {
        props: {context},
    };
};
