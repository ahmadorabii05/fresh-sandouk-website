import Image from "next/image";
import Link from "next/link";
import React, { useState, useEffect } from "react";
import searchIcon from "../public/icons/search.svg";
import cartIcon from "../public/icons/cart.png";
import favIcon from "../public/icons/favorite.png";
import arabicIcon from "../public/images/arabic kibsons logo.png";
import englishIcon from "../public/images/english kibsons logo.png";

import { useRouter } from "next/router";
import { useSelector } from "react-redux";

import { useDispatch } from "react-redux";
import { signoutUser } from "@/store/reducers/userReducer";
import { searchForProductsRequest } from "@/network/categories";
import prepareErrorResponse from "@/errorHandlerLayer/errorHandler";
import { toast } from "react-toastify";
import ContainerRow from "./containerRow";
import useTranslation from "next-translate/useTranslation";

function DesktopNavBar() {
  const { t } = useTranslation('common')

  const [showOverlay, setShowOverlay] = useState(false);
  const user = useSelector((state: any) => state.user.user);
  const [searchQuery, setSearchQuery] = useState('');

  const [currentUser, setCurrentUser] = useState({ id: "" });
  const [foundItems, setFoundItems] = useState([]);
  const dispatch: any = useDispatch();
  function handleFocus() {
    setShowOverlay(true);
  }

  function handleBlur() {
    setFoundItems([]);
    setShowOverlay(false);
  }

  const highlightMatchingLetters = (itemName: any, searchQuery: any) => {
    const regex = new RegExp(searchQuery, "gi");
    return itemName.replace(
      regex,
      (match: any) => `<span style="color: #81C259">${match}</span>`
    );
  };

  const handleSearchInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const query = e.target.value;
    setSearchQuery(query);
    searchForItems()
  };
  const handleItemClick = (e: any, item: any) => {
    setSearchQuery('')
    router.push('/product/details/' + item.id)
  };


  const searchForItems = () => {
    searchForProductsRequest(searchQuery)
      .then((res) => {
        const docs = res.data.data.docs;
        setFoundItems(docs);
      })
      .catch((err) => {
        toast.error(prepareErrorResponse(err));
      });
  };
  useEffect(() => {
    setCurrentUser(user);
  }, [user]);

  const handleSearchSubmit = (e: any) => {
    e.preventDefault()
    setFoundItems([]);
    setShowOverlay(false);
    setSearchQuery('');
    router.push('/category/shopping/search/query/' + searchQuery)

  }
  const logout = () => {
    dispatch(signoutUser({}));
    setCurrentUser({ id: "" });
  };


  const router = useRouter();
  const cartItems = useSelector((state: any) => state.cart.cartItems);
  const numItemsInCart = cartItems.length;
  const favorites = useSelector((state: any) => state.favorites.favorites);
  const numItemsInFavorite = favorites.length;
  const totalAmount = cartItems.reduce((accumulator: any, product: any) => {
    const price = product.finalPriceAfterDiscount;
    return accumulator + price * product.quantityInCart;
  }, 0);
  const totalAmountWithoutDiscount = cartItems.reduce((accumulator: any, product: any) => {
    const price = product.price;
    return accumulator + price * product.quantityInCart;
  }, 0);
  return (
    <div className="main-navbar my-2">

      <div className="d-flex v-center justify-content-between">
        <Link href="/">
          <Image
            className="logo1"
            src={router.locale == "ar" ? arabicIcon : englishIcon}
            alt="logo 1"
          />
        </Link>
        {/* <img
          className="logo2"
          alt="logo2"
          src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGMAAABgCAYAAAAJr8w7AAAACXBIWXMAAAM2AAADNgGC9MelAAARG0lEQVR4nO1dv28byxFe2SoDUK9Jk8JnvBQuAopGujSi8erANJAmAgJRf4H40qQU/ReI7gKkMA0ErBKYRv4Ak0CKdD4KKVzEeEcgKYPH6+0oWOIb+uNw9273eNZPD0BIJO9u92Zmv5mdH8edi4sL85Wq0ajRbBljEmNMi179w/x8WOWCu1/lUE6jRjMB09vE/H3PiUnVcb4KQ9Go0WyD2azxjYhLtKuOfWeFoSBGNP5B4OlTY0xqjMnw177GxpiDbVbGrbcZkRCjaUYMn9j/D/PzzDNO3xhzirffHObni9i53qqVsQXEzMH0iTD/MD9PI4fn41u4VhTdSGHUDTFVtNhBvGJunzAuC2LqILuSRo2mXKmS3bg2wrhiiKmLZlCWVpXrXbowtoCYnDyXuiGmLkqvpTBqgJiUNf5LQkyNJHNs2PuPnXMtwqgJYlIw/aogpg6akHubKKNeSlHCqBNiDvPzaG/jBhAzvx3rUTmFcUchZmuy9zlqNHOgQrRHtfsVYsIIfGIejQ/z84Hj5BRhkWgjblfGsARq7grELMkTFvfxp0gYoUiyol0wWAa7MxAzajT3lO2zrz8bY/7kOSXnDaQKfzCt+GUFG4MWu7i4laTFvEr+8XUncTNHjWbPGNMpgGIRBIdMutByq5gh4XFmflIgtA3aVZJs3xYIGjWaHcCIXfXHgONEFA/kCpnsqOuI8xIEO5Z/FBZpIbQeRLsOSd4oQui6DUaw5oogppQG5Xt9Eqh4VWBnjrGjEk27KsB17WCKvL09fDRRTGwpbRcBiR3s01dVIqt6IxciDLHDUcot+4ytAlx1E6Bh7PFiTkeN5rPD/FyWv2XOU/N5f2RjVT18N2XBaQiBoNm1Xzjsgs5ThMCO2OHQsP6SRBjZZQoDzO6AAT1HsG9BNzInjRbDO4RRXjhgtk3GuWc2SSDkCK9CsmPQRi4UdlZzirHD99TJDbh8tRA0z0VDLP0j/L9Gyp22+5o2NFaY24CXY5QwunTMKw++82cWEV4ZY55bG2KMeeiZr5wTCjsaDoNIhKExeCuymD1qNG1y/a1HuOyZPIXLqWnqYACvoCWDlOCOaFXsecZmYXQO8/PuYX5ua50mBXsqOScIdpQSBNsNEYYOcHnJajuY3XEdA9w+pY9a6nu+fo6/9np60jKnfYw5oFU0V0wVweV0TWtHUkAiUxVGrcFO4Dkz/I1bGUojyiY4BrNfw2vRpGFHT4bfi0CXdkAdx0x7a4w5Ia0fKDuzyiNg/iIcq8nWaHcdxy7Hh6B7VtijRnOCFa1JzsnJqysjmX80TBkPLLiImWQ9m6HAAbTmQJ2jryeaJZ7OC7w/UHDF48xpfpbOFIPXtB325TneWwG9lHkqCDmBoM/w/4FxaD/m+fAwP98jL66M1hJNISewMGSSmpmatFE8gvbtKRjxLVN5L9fp43ij4IrHGYLBTwiGXhJUavfTMrCvjpd5Jkqwduw3EN4zY8xjl/dTIUbH1wgSBuczQneaPKkZhQoy5VJKTGclXDBCjOCC/HxxZQWu2i6XEvsE+/87XGOI9047gONlbyB7kK5sBGNCPxB8i5JG45LdeHSiiYURGuDizwfA0DMShIWfMZiw2oxBs3iVnCpDL7SEK+QKUl0yiYjB9zTmEDfr3As4xi0kKEyXNoJWob6hVT+BsnWKrlsl0eSCKVMyyJobDKYdExwI7ru0VXtSU9gMy9wnDu9KxnrAbirGfEUbQtHYx4GR1SXBcHfVxx0oyR5W1DO6H7tD7+DzkMBhlBFfrQzAguxOy05eO84G4kaNZgp4kQmwMGSZynWnLqaBMa9J40Ubc71a7f4g5Abp2n0oD7vkHTCdPTkZY0Ar3EiMCve5DztTRlGJpnvqPYcdimjjOCsETkMqgyfHif1wQiA8lRfQ+gk8nW/gxQTlBaxA4TmtnAHa+zxV7njmYJSMM4Sb+w6rPaPv57iWc6+lri/zKl0duiAhNMAlxzXgLvoKyaaC+cpd9DL2MD937cbXCMztUHrUjvEYhlkMekJZOWHEHNotxte+jthhIcdBjH0K/Jf5pyScIWJPIfa1NOLrWxllO80g+4LryY0tYFuexLRZWe1z4HobBlzg7xhjiWfWQTxrQscbMvQ6v6ENrAhmDEXq0rUkQNnGmN7V4Qj1F5JeGaGhgiC3zYHrXs2gSK5Bxk0Y1sNcWIAyvt1/DLBSBPvt5u3HUaM5RX+d2Ko5BRf3AVcCqzacnlEuXOyD7MZz2IrvJT6G1RBimIMTTRvNMjSBF0WQYZe6dOzEluiIpgvDLcZjU5bTnmMK4fSA92sNKJjnnCD1ud3oYUV3SbCWuT+qKpc2bMVjMJ7z4TOM2a6jKMOGWACj88P8vNDFdRWxBSWalFfimkRCS7kP6BBmdvDdEIKxglgJH2GRM2U4dWZuhtVwDIZJFMC6n13qJPoVju+R8BO4pyLshaPuq65agOBEk0sYlRJNuHlOiQ5wjR4Vdcl3KbyRhHbXq1UI6BHtFO9HC8PO8ync6hY8m4/GmL9RNs+urp9StYdcP2ojWHLfiSkOlwQnmlzCkDSm11OCNndF2wENVgt7lIFrQ/tWmg0mJcQIwWjZ7DE9II03MKRc6yQCZZx9gk1aC16QBPUq9WU77nut+nLn/r1fGmN+0nj07V+NMb/xnBacd3cJgxPwvpMzaPtQhQUkJdrD/ykVPJzR+RJEbOMaNuhnN1k9MFxWw4SOP8HLwFZwkFBDTHB5jIt0gffO7v2fX3z89DN96MWn/y3/5u8//Nt3rZiOJh9MCTk9JWir3ZydULBvDiacUEZOGCTM7wrToNEJjK5lrL3WCQ3zBpFXQzEwXVq6VW1vaIH3xcdPy787u/f/efHx04cKrWlBdnhDGNjgyNsiSfYBVUOK4/ShsUdm3c9ebq6U9k4pnGL3ErzC1jy0qo9/ECJ424AY1/E7u/f/c/Hx079qbE0L6mjy9WdMyxrMYSsklmRgvOWztypnsNQe1c0z5Jw28H0reDGfIUaYvoQYY4wXYlzdr7/977u6W9OCOpp8wpAAV2GiCYG0N5JvNusQxjSAsDI6d1ttZ4hp7eze//bi46df6ONqgBjf+Cx0XVinKaijySeMmJLGHrCfYWVtswjvqpLf7oIYl5IQ02uFGBpfFC112JUyoQZFLIpWhlBhgKtmnz0IYohqhRhpiFGrtgXYlUiy5FWehTYIhSaaQoQRVUkdQtcAYrgnQ/YOUrW+z/sS3ufgo4z2QKtSoAChlHY0OYURmWjy0lVDDBOYNnRAzBTKtpDog2OnPKd5S3jD2ksuQQ2pqilMNBV1u2YxwrhqiDGfhd+jCGkq9gtanFFU4KXOOMIeWGekrVavlCK1FGo8p3soo1I7XCSM0gCX3TXv7N7/7jIhRo3fgbb3qYDhAbnVujihQ+e+dCiaMIgLJeZYNQ3lCWW0KQ2hUjtctjJk4s4AV+PRt9/l7z8sBfGFIaaDDWWqciQrJlEZkDO/7qApKlE4/sYRg57cM8Z/jRz6mGJlfUIDPbc1CuloKhJGaaIpf//hdxKmqGOjRAHIhQrRL6Q+C1CxrKuiG2zhmBwMllU4KFiFmY4mU/QhUcqnc/4S3jhVLdhlVJho8gojpKOpQlJpw4uxxQZ0SJtKLDuqIUZoWTBHueelcaXdf58K605UYw1TirCNDoZurBjwIieh9KEwsXunwo4mnQPXFF1JzYT8tV3WCYVJXhImj1XZvgQcc0R/l9+BKVJh/j2gSQqaOdQyRsfuYyqlcfVqm4Kapgz3vcYwVKiIMzCu2Ii6qgNzfVn27JCgRJMULHOpDjB8SDVQcvO+mqk9TPINJn1GIXpDruFQKjMgWFGYhHLhUvX3tMDTSck4ryi2HsvENV4WJprKVkZoR9MZKsNTOq5PuWWebILMXFf1eawKoiHUGZJHwpwJXWtMq0j8dnFBXyPn/QM+d64M2JwkpDRIyN4bWgiWc8bKt8J+Z1MCyHcXUWFHU5kwQktNxJXcR4NKD3g8V+fOoP3vCK5EeLJaush1CJOl+iPj46g6g6F0Ahh7jtfDiBJ+J4H5FhIXEPJbilS/xsp6hvEKxyrraAqBKaGiSmqBkFcQgmT1OmA8hxL2wbCJmhwXiQ0xVgfJqiFl/7gIekFdqwu899mIQqKC5x6t6OfkdU0xpwkUbq0oL0Lo3kRToTAiEk0Z/T0GQ8bwQmak5bLDXVDGr0VVf3Pl0k6oLHIhQnTMM8qYkgvdo/iSlOzMMf9EeVoL2eQBilNi7A8IH4U8B92baAp5+Fdpookr85BGnZBhXMV7KK7fB5SJkH7vaSXTTz0IdqVVP4XMYwAlkMdWtGkl2vGPXcyUvQzuSzaXxzhfelEeIJeflOzMvYmmEGGEJJrWyiR16zBWQ6K0WuJEKZb5mYkkaKhEXVtU/9SnEqEZPj9CzKlFSiEKZpViVqDVslnLKMIwASz2oVwtQHJHPZVBkzfRFCKM0gAXRXldAhvDSEtMasdxTCGp6O+YhJ2QMV1VF0KTJbbWRwhjQiucy4d45fhIYl49vn/AXUcpWdnq9drh0JUhVJRoGrs6QTH5KPeRKsxF67n8ciFwRlGCZekkmPNSNed3KKI8J1uV47OVl6brxGjlyeruUBmP/fyP+PwpTpmWORBFiaZYYXgTTTH+usGNYkUlWNYysX8YY/6A/2eYtNS/up5jO6OYFTdnCpP5kRQpMTyllSxeoLjlC6qGzIjBp2rc92gxa7FTEkDORFOpMOpINFE/RYuM51ytNElpPsL+QMIcF6bYYxJ3WZoup4AT8QRzyuIdQJnaxJAETN+Dhr+ma+c4L+XchYPpsaERZ6Ip9FGqQYkmFQhcEL53oVU53VgGbW6gYM1XSD0ryo4RhLxAi0Cqz6Uu2QmCgAOGJwhaIEjsU6b2DjG5izJy2uFQYYQkmvqO7lVulDwFs3p0zh4EZMMe4l3psLcvFcpzW47r0NiUz4W7Kzv3v0ApON/NqzSKHA+aHBd4Z047HLMyhIE+psgA0icxwO6ZvY02MLlF8CCu4IHU03qeJ+Xb5xT1IQr0LY+BkNnJ+Hvg/a/I86BJn9sfIoyVHQ4VRkhHkxzTJu8ll+I14Pc+QiUCV3sQrOSc+zCkPXIUfK1eSyLvZCO5Bc2sXCzneJat9uyYuBknLXJxlR1ebWqDhBGYaBKGH5AxNchZtGkn/5BhiG54QsLQm0ZT1IalElTRFPksW4N7yWgTWOXXCjYSTTHPQg+ppBaGS8OjBPrWnnaACGiLrsW775zdZwj5WWAFRiFFQoyhRymlBZ5UVdqwwzHCCEk0SYRTmlw4yCfMfKvO+TWEJx7YUGtZlTD4lhCTXcIjZTfscIwwSjuaKK88oMxdTm0DdSzvNboiiKmDtBGPFsbayQXHSIh5oGJJlX9Q8JpBzNbk6miqKgxvR1OVQKCmGwAxddGaHQ4WRkSiKZi2gJgV86/Zby7FUlZJGKCQRNMGVfhxlJmD6bWUhF4zYjucxAojK0s0ScWf0vgyiJGEzU2CmGjSxeFKqaOFId5SUa3QwCOs2wYxXtL9J/REtyJqVxGGkC/RJL0Otx1igvtPPKS9vXQbYTgTTShAq1Quc53JAzGxv8RW6O1FCUM9UbOWPr7rRhUhRmirDWWVH010pgxvGimIaQVsIpm+yIayijAq/TbEVZKnoTLk5+8udUNZRRghiaYrIUCM1vZLgZg6aCth1LUTj6XrCDF1ULQwHL1pX5RuCsTUQVV/9bj232i66RBTB1UVRqVH55lbDDF1UFVhhCSa7hTE1EHbCENIngl7pyGmDqpDGDqn7SMdFr/VP1ldhTYeMhxKnt8mMnctLF4nVV0Zhpoq70RY/IuTMeb/6ohNYtXPZ+0AAAAASUVORK5CYII="
        /> */}
      </div>
      <form className="input-wrapper mx-3" onSubmit={handleSearchSubmit}>

        <input

          className="kibsons-input"
          placeholder={t("findProducts")}
          onFocus={handleFocus}
          onChange={handleSearchInputChange}
          onBlur={handleBlur}
          value={searchQuery}
        />

        <button className="search-icon" type="submit">
          <Image alt="image" src={searchIcon} />
        </button>
        <div className="ui-list-searched">
          {foundItems.map((item: any) => {
            return (
              <div
                key={item.id}
                className="found text-muted fs-sm"
                // onClick={e=>handleItemClick(e,item)}
                onMouseDown={(e) => handleItemClick(e, item)}

                dangerouslySetInnerHTML={{
                  __html: highlightMatchingLetters(item.name, searchQuery),
                }}
              />
            );
          })}
        </div>
      </form>
      {showOverlay && <div className="overlay" />}
      <div className="menu-items mx-3">
        <div className="ml-auto d-flex justify-content-between p-relative">
          <span className="nav-item">
            <Link href={"/howitworks"}>{t("howItWorks")}</Link>{" "}
            <span className="mx-1">|</span>{" "}
          </span>
          <span className="nav-item">
            <Link href={"/ourhistory"}>{t("Our history")}</Link>{" "}
            <span className="mx-1">|</span>{" "}
          </span>
          {/* <span className="nav-item">
            <Link href={"/freshSandoukCare"}>{t("Kipsons Cares")}</Link>{" "}
            <span className="mx-1">|</span>{" "}
          </span> */}
          {/* <span className="nav-item">
            <Link href={"/competition"}>{t("Competitions")}</Link>
            <span className="mx-1">|</span>
          </span> */}
          <button
            className="nav-item"
            onClick={() => router.push("/account/myfavorites")}
          >
            {t("My Account")} <span className="mx-1">|</span>
            <div className="my-account-menu-wrapper">
              <div className="my-account-menu">
                <div className="d-flex justify-content-between">
                  <div className="x">
                    <h5>{t("MY BASKETS")}</h5>
                    <Link href={"/account/myfavorites"} className="lnk">
                      {t("My Favorites")}{" "}
                    </Link>
                    <Link href={"/account/myshoppinglist"} className="lnk">
                      {t("My Shopping List")}{" "}
                    </Link>
                    <Link href={"/myshopcart"} className="lnk">
                      {t("My Shopping Cart")}{" "}
                    </Link>
                    {/* <Link href={"/myvouchers"} className="lnk">
                      {t("My Vouchers")}{" "}
                    </Link> */}
                  </div>
                  <div className="x">
                    <h5>{t("ORDER INFO")}</h5>
                    {/* <Link href={"/account/mywallet"} className="lnk">
                      {t("My Wallet")}{" "}
                    </Link>
                    <Link href={"/account/mystatement"} className="lnk">
                      {t("My Statement")}{" "}
                    </Link> */}
                    <Link href={"/account/myorders"} className="lnk">
                      {t("My Orders")}{" "}
                    </Link>
                    {/* <Link href={"/account/myregulars"} className="lnk">
                      {t("My Regulars")}{" "}
                    </Link> */}
                  </div>{" "}
                  {/* <div className="x">
                    <h5>{t("COMPETITION")}</h5>
                    <Link href={"/competition"} className="lnk">
                      {t("Win a trip to South Africa")}
                    </Link>
                    <Link href={"/account/myrewardpoints"} className="lnk">
                      {t("Reward Points")}{" "}
                    </Link>
                    <Link href={"/account/refertofriends"} className="lnk">
                      {t("Refer Friends")}{" "}
                    </Link> 
                    <Link href={"/account/returnbox"} className="lnk">
                      {t("Return & Recycle Service")}{" "}
                    </Link>
                  </div>{" "} */}
                  <div className="x">
                    <h5>{t("USER PROFILE")}</h5>
                    <Link href={"/account/myaddresses"} className="lnk">
                      {t("My Addresses")}{" "}
                    </Link>
                    <Link href={"/account/myprofile"} className="lnk">
                      {t("Update My Profile")}
                    </Link>
                    <Link href={"/account/changepassword"} className="lnk">
                      {t("Change Password")}{" "}
                    </Link>
                    <Link href={"/account/deleteaccount"} className="lnk">
                      {t("Delete My Account")}{" "}
                    </Link>
                  </div>
                </div>
              </div>
            </div>
          </button>
          {currentUser && currentUser.id ? (
            <span className="nav-item" onClick={() => logout()}>
              {t("Logout")}
            </span>
          ) : (
            <span className="nav-item">
              <Link href="/auth/login">{t("User login")}</Link>
            </span>
          )}
          {/* Add additional navigation items here */}
        </div>
      </div>
      <div className="cart-and-fav">
        <div className="d-flex">
          <div
            className="p-relative link"
            onClick={() => router.push("/myshopcart")}
          >
            <span className="counter c-nav">{numItemsInCart}</span>
            <Image alt="image" className="cart-icon" src={cartIcon} color="red" />
          </div>
          <div
            className="p-relative link"
            onClick={() => router.push("/account/myfavorites")}
          >
            <span className="counter c-white">{numItemsInFavorite}</span>
            <Image alt="image" className="fav-icon" src={favIcon} />
          </div>
        </div>
        <div className="text-center bold-text f-gill">
          {t("AED")} {totalAmount.toFixed(2)}
        </div>
        {
          totalAmountWithoutDiscount != totalAmount &&
          <div className="text-center bold-text f-gill">
            {t("AED")}{" "}
            <span className=" text-through">
              {totalAmountWithoutDiscount.toFixed(2)}
            </span>
          </div>
        }
      </div>
    </div>
  );
}

export default DesktopNavBar;
