import React from "react";
import Image from "next/image";
import moneyIcon from "../public/icons/money.png";
import walletIcon from "../public/icons/wallet.png";
import deliveryIcon from "../public/icons/delivery1.png";
import rewardIcon from "../public/icons/reward.png";
import meetIcon from "../public/icons/meet.png";
import FacebookIcon from "./icons/FacebookIcon";
import Instagram from "./icons/instagramIcon";
import InstagramIcon from "./icons/instagramIcon";
import WhatsAppIcon from "./icons/WhatsAppIcon";
import PhoneIcon from "./icons/PhoneIcon";
import EnvelopeIcon from "./icons/envelope";
import LocationIcon from "./icons/locationIcon";
import LogoFt from "../public/icons/logo-ft.png";
import DeliveryLight from "../public/icons/delivery-light.png";
import Account from "../public/icons/account.png";
import Link from "next/link";

import useTranslation from "next-translate/useTranslation";

const Footer = () => {
  const {t} = useTranslation('common')

  return (
    <div className="footer">
      <div className="footer-p-1 p-3">
        <div className="footer-content">
          <div className="row1 p-2">
            <div className="footer-image-label">
              <Image alt="image" className="ft-image" src={moneyIcon} />
              <div className="ft-img-title">{t('payment')}</div>
            </div>
            <div className="footer-image-label">
              <Image alt="image" className="ft-image" src={walletIcon} />

              <div className="ft-img-title">{t('guaranteed')}</div>
            </div>
            <div className="footer-image-label">
              <Image alt="image" className="ft-image" src={deliveryIcon} />

              <div className="ft-img-title">{t('delivery')}</div>
            </div>
            <div className="footer-image-label">
            <Image alt="image" className="ft-image" src={meetIcon} />

              <div className="ft-img-title">{t('quality')}</div>
            </div>
            <div className="footer-image-label">
            <Image alt="image" className="ft-image" src={rewardIcon} />
              <div className="ft-img-title">{t('rewards')}</div>

            </div>
          </div>
          <hr />
          <div className="d-flex justify-content-between">
            <div className="footer-info">
              <h5 className="footer-bordo">{t('followUs')}</h5>
              <div>
                <a
                  style={{ color: "black", textDecoration: "none" }}
                  href="https://www.instagram.com/freshsandouk"
                  target={"_blank"}
                >
                  <span>
                    <InstagramIcon />
                  </span>
                  @freshSandouk
                </a>
              </div>
              <div>
                <a
                  style={{ color: "black", textDecoration: "none" }}
                  href="https://www.facebook.com/freshsandouk"
                  target={"_blank"}
                >
                  <span>
                    <FacebookIcon />
                  </span>
                  @freshSandouk
                </a>
              </div>
              <div>
              <a
                style={{ color: "black", textDecoration: "none" }}
                href="https://wa.me/00971526331777"
                target="_blank"
                rel="noopener noreferrer"
                >
                <span>
                  <WhatsAppIcon />
                </span>
                 <span dir="ltr">{t("Whatsapp Phone number")}</span>
              </a>
              </div>
            </div>
            <div className="footer-info">
              <h5 className="footer-bordo" id="contact-footer">{t("contactUs")}</h5>
              <div>
                <div>
                  <PhoneIcon />
                  FreshSandouk (+971526331777)
                </div>
                <div>
                  <EnvelopeIcon />
                  sales@freshsandouk.com
                </div>
                <div>
                  <LocationIcon />
                  Store Number 33, Ras Al khor Industrial 2,P.O Box: 96120, Dubai UAE
                  {/* {t("location")} */}
                </div>
              </div>
            </div>
            {/* <div className="footer-info">
              <h5 className="footer-bordo">
                {t("INTERESTED IN PURSUING A CAREER AT KIBSONS?")}
              </h5>
              <div>
                <div>Please share your details <a href="https://www.kibsons.com/careers/" target="_blank" className="blue darkblue bold">here</a></div>
              </div>
              <h5 className="footer-bordo mt-2">
                {t("supplier")}
              </h5>
              <div>
              <div>Please share your details <a href="https://forms.monday.com/forms/fb207f5aef1b514f7fb02b161fc669e8?r=use1" target="_blank" className="blue darkblue bold">here</a></div>
              </div>
            </div> */}

          </div>
        </div>
      </div>
      <div className="footer-p-2 p-5">
        <div className="footer-content">
          <div className="d-flex  justify-content-between pt-3">
            <div className="info-1 premiem-text">
              <h3 className="fs-24">{t("premiumProducts")}</h3>

              <div className="divider-red"></div>
              <p className="fs-13">
                {t("premiumProductsDetails")}
              </p>
            </div>

            <div className="info-1">
              {/* <Image alt="image" className="ft-image" src={Account} /> */}

              <h4 className="text-uppercase fs-20">{t("myAccount")}</h4>
              <div>
                <Link className="text-white fs-15" href={"/account/myorders"} >
                  {t("myAccount")}
                </Link>
                <Link className="text-white fs-15" href={"/account/myshoppinglist"}>{t("shoppingList")}</Link>
                <Link className="text-white fs-15" href={"account/myprofile"}>{t("accountDetails")}</Link>
                {/* <Link className="text-white fs-15" href={"/account/refertofriends"}>{t("referAFriend")}</Link> */}
                <Link className="text-white fs-15" href={"/account/myrewardpoints"}>{t("loyaltyPoints")}</Link>
              </div>
            </div>
            <div className="info-1">
              {/* <Image alt="image" className="ft-image" src={DeliveryLight} /> */}

              <h4 className="text-uppercase fs-20">{t("homeDelivery")}</h4>
              <div>
                <Link className="normal" href={"/howitworks"}>
                  {t("howItWorks")}
                </Link>
                <div>{t("freeDelivery")}</div>
                <Link className="text-white fs-15" href={"/faq"}>{t("faq")}</Link>
              </div>
            </div>
            <div className="info-1">
              {/* <Image alt="image" className="ft-image" src={LogoFt} /> */}
              <h4 className="text-uppercase fs-20">{t("aboutKibsons")}</h4>
              <div>
                <Link className="text-white fs-15" href={"/ourhistory"}>{t("ourHistory")}</Link>
                <Link className="text-white fs-15" href={"/blog"}>{t("blog")}</Link>
                <Link className="text-white fs-15" href={"/socialresponsibility"}>
                  {t("socialResponsibility")}
                </Link>
              </div>
            </div>
          </div>
          <div className="divider"></div>
          <div className="rights d-flex justify-content-between text-white fs-15 fs-13">
            <div>
              {t("Copyright")}
            </div>
            <Link className="text-white fs-15" href={'/termsAndConditions'}>{t("terms")}</Link>
            <div>{t("languageFooter")}</div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Footer;
