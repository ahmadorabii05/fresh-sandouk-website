import {
  useLoadScript,
  GoogleMap,
  MarkerF,
  Circle,
} from '@react-google-maps/api';
import type { NextPage } from 'next';
import { useMemo, useState } from 'react';
import usePlacesAutocomplete, {
  getGeocode,
  getLatLng,
} from 'use-places-autocomplete';

const MapContainer = ({callBack}:any) => {
  const [lat, setLat] = useState(27.672932021393862);
  const [lng, setLng] = useState(85.31184012689732);

  const libraries = useMemo(() => ['places'], []);
  const mapCenter = useMemo(() => ({ lat: lat, lng: lng }), [lat, lng]);

  const mapOptions = useMemo<google.maps.MapOptions>(
    () => ({
      disableDefaultUI: true,
      clickableIcons: true,
      scrollwheel: false,
    }),
    []
  );

  const { isLoaded } = useLoadScript({
    googleMapsApiKey: 'AIzaSyCDDBMtT_irrnFZcELYcbPamL0OQZ3D568' as string,
    libraries: libraries as any,
  });

  const confirmPin = () => {
    callBack(mapCenter)
  };

  const skip = () => {
    callBack(null)

  };
  if (!isLoaded) {
    return <p>Loading...</p>;
  }

  return (
    <div>
      <div>
        {/* render Places Auto Complete and pass custom handler which updates the state */}
        <PlacesAutocomplete
          onAddressSelect={(address) => {
            getGeocode({ address: address }).then((results) => {
              const { lat, lng } = getLatLng(results[0]);

              setLat(lat);
              setLng(lng);
            });
          }}
          setLat={setLat} // Pass setLat function to PlacesAutocomplete
          setLng={setLng} // Pass setLng function to PlacesAutocomplete
        />
      </div>
      <GoogleMap
        options={mapOptions}
        zoom={14}
        center={mapCenter}
        mapContainerStyle={{ width: '100%', height: '50vh' }}
        onLoad={(map) => console.log('Map Loaded')}
      >
        <MarkerF position={mapCenter} onLoad={() => console.log('Marker Loaded')} />
      </GoogleMap>
      <button className='mapbtn confirm-pin' onClick={confirmPin}>Confirm Pin</button>
      <button className='mapbtn skip' onClick={skip}>Skip</button>
    </div>
  );
}; 

const PlacesAutocomplete = ({
  onAddressSelect,
  setLat, // Add setLat prop
  setLng, // Add setLng prop
}: {
  onAddressSelect?: (address: string) => void;
  setLat: (lat: number) => void;
  setLng: (lng: number) => void;
}) => {
  const {
    ready,
    value,
    suggestions: { status, data },
    setValue,
    clearSuggestions,
  } = usePlacesAutocomplete();

  const renderSuggestions = () => {
    return data.map((suggestion) => {
      const {
        place_id,
        structured_formatting: { main_text, secondary_text },
        description,
      } = suggestion;

      return (
        <li
          key={place_id}
          onClick={() => {
            setValue(description, false);
            clearSuggestions();
            onAddressSelect && onAddressSelect(description);
          }}
        >
          <strong>{main_text}</strong> <small>{secondary_text}</small>
        </li>
      );
    });
  };

  const locateMe = () => {
    const successHandler = (position: any) => {
      const { latitude, longitude } = position.coords;
      setLat(latitude);
      setLng(longitude);
    };

    const errorHandler = (error: any) => {
      console.error('Error getting user location:', error);
    };

    const options = {
      enableHighAccuracy: true,
      timeout: 5000,
      maximumAge: 0,
    };

    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(successHandler, errorHandler, options);
    } else {
      console.error('Geolocation is not supported by your browser');
    }
  };

  return (
    <div className='map-locations-actions justify-content-between'>
      <div>
        <input
          value={value}
          disabled={!ready}
          onChange={(e) => setValue(e.target.value)}
          placeholder="search for place"
        />

        {status === 'OK' && (
          <ul className={'suggestionWrapper'}>{renderSuggestions()}</ul>
        )}
      </div>
      <div className='locate-me'>
        <button className='locate-me' onClick={locateMe}>Locate me</button>
      </div>
    </div>
  );
};

export default MapContainer;
