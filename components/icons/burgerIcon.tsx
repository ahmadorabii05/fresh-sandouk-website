import * as React from "react"

const BurgerIcon = (props:any) => (
  <svg
    stroke="currentColor"
    fill="currentColor"
    strokeWidth={0}
    viewBox="0 0 512 512"
    className="mobile-header-hamburger"
    color="#FFF"
    height={30}
    width={30}
    xmlns="http://www.w3.org/2000/svg"
    style={{
      color: "#fff",
    }}
    {...props}
  >
    <path
      fill="none"
      strokeLinecap="round"
      strokeMiterlimit={10}
      strokeWidth={48}
      d="M88 152h336M88 256h336M88 360h336"
    />
  </svg>
)

export default BurgerIcon
