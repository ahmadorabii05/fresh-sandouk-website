import { useRouter } from 'next/router';
import { useSelector } from 'react-redux';
import {useEffect,useState} from 'react'
const IsLoggedIn = ({ children }:any) => {
  const router = useRouter();
  const { user,token } = useSelector((state:any) => state.user);
  const [isLoggedIn,setIsLoggedIn] = useState(false)
  useEffect(()=>{
    if(user && user.id && token){
      setIsLoggedIn(true)
    }
  })
  if (isLoggedIn) {
    // redirect the user to the login page
    router.push('/');
    return null;
  }

  // allow access to the protected route
  return children;
};

export default IsLoggedIn;
