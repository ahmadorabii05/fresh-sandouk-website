import React from "react";
import Image from 'next/image'
import logoImage from '../public/icons/karaz.png'

function AppLoader({isLoading = true}) {
  if(!isLoading){
    return <div></div>
  }
  return (
    <div className="app-loader">
      <div className="spinner-grow" role="status">
</div>
    </div>
  );
}

export default AppLoader;
