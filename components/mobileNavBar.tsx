import React, { useState, useRef, useEffect } from "react";
import BurgerIcon from "./icons/burgerIcon";
import PhoneIcon from "./icons/PhoneIcon";
import logo from "../public/icons/logo-mobile.png";
import deliveryImage from "../public/images/delivery.jpg";
import phoneWhite from "../public/icons/phoneWhite.png";
import Image from "next/image";
import UserIcon from "./icons/userIcon";
import { Collapse, Button } from "react-bootstrap";
import { useSelector } from "react-redux";
import RightArrow from "./icons/rightArrow";
import leftArrow from "../public/leftArrow.svg";

import Link from 'next/link'
import { useRouter } from "next/router";
import Cookies from "js-cookie";
import { getAllCategories } from "@/network/categories";
import { toast } from "react-toastify";
import prepareErrorResponse from "@/errorHandlerLayer/errorHandler";

function MobileNavBar() {
  const [showSidebar, setShowSidebar] = useState(false);
  const sidebarRef = useRef(null);
  const [openOne, setOpenOne] = useState(true);
  const [openTwo, setOpenTwo] = useState(false);
  const router = useRouter();
  const [isAccountTopHeader, setIsAccountTopHeader] = useState(false);
  const [pageTitle, setPageTitle] = useState("");
  const user = useSelector((state:any)=>state.user.user)
  const [currentUser,setCurrentUser]:any = useState({id:''}) 
  const [categories,setCategories]:any = useState([])
  const toggleSidebar = () => {
    setShowSidebar(!showSidebar);
    if(!(categories && categories.length)){
      getAllCategories().then(res=>{
        setCategories(res.data.data.docs)
      }).catch(err=>{
        toast.error(prepareErrorResponse(err))
      })
    }
  };

  useEffect(() => {
    setCurrentUser(user)

    if (router.asPath.includes("account")) {
      setIsAccountTopHeader(true);
    } else {
      setIsAccountTopHeader(false);
    }

    setPageTitle(
      router.asPath
        .split("/")
        .filter((path) => path !== "")
        .map((path) =>
          path.charAt(0).toUpperCase() + path.slice(1).toLowerCase()
        )
        .join(" / ")
    );
  }, [router.asPath,user]);

  const handleClick = (cat:any) =>{
    router.push('/category/'+ cat.id)
    setShowSidebar(false)
  }

  const handleLogState = (isLoggedIn:boolean) =>{
    if(isLoggedIn){
    }else {

    }
  }
  const collapseItems = () => {
    return (
      <>
        <Button
          className="btn-collapse"
          onClick={() => setOpenOne(!openOne)}
          aria-controls="collapse-one"
          aria-expanded={openOne}
        >
          Categories
        </Button>
        <Collapse in={openOne} >
          <div id="collapse-one">
            {categories.map((cat: any,index:number) => {
              return <div className="cat-item" key={index} onClick={()=>handleClick(cat)} >
                <img src={cat.imageUrl} className="cat-img"  alt="category image" />
                <div className="catname">{cat.name}</div>
                <RightArrow />
              </div>;
            })}
          </div>
        </Collapse>
        <Button
          className="btn-collapse"
          onClick={() => setOpenTwo(!openTwo)}
          aria-controls="collapse-two"
          aria-expanded={openTwo}
        >
          Featured Category
        </Button>
        <Collapse in={openTwo}>
          <div id="collapse-two"></div>
        </Collapse>
      </>
    );
  };


  return (
    <nav className="my-nav">
      <div className="navbar nav-mobile">
        <div className={"main-menu-items " + (!isAccountTopHeader ? " justify-content-between" : "justify-content-start") }>
          <div className="d-flex v-center">

          <div className="burger-menu" onClick={toggleSidebar}>
            <BurgerIcon />
           </div>
           <div>
                  <Image alt="image" src={phoneWhite} className="phone"></Image>
                </div>
          </div>
          {
            !isAccountTopHeader ? (
              <div className="d-flex justify-content-between v-center">
       
                <Image className="logo-kib-mobile" onClick={() => router.push('/')} alt="image" src={logo} ></Image>
              </div>
            ) : (
              <div className="d-flex v-center">
                <div onClick={() => router.back()}>
                  <Image src={leftArrow} alt="back-arrow"></Image>
                </div>
                <div className="white-title mx-4">
                  {router.pathname.split('/')[1].toUpperCase()}
                </div>
              </div>
            )
          }
        </div>
        {showSidebar && <div className="sidebar-bg-closer" onClick={() => setShowSidebar(false)}></div>}
        <div className={`sidebar ${showSidebar ? "show-sidebar" : ""}`} ref={sidebarRef}>
          <div className="mobile-top-header">
            <h4 onClick={()=>handleLogState(currentUser && currentUser.id)}>{currentUser && currentUser.id ? 'Logout' : 'Login'}</h4>
            <UserIcon />
            {currentUser && currentUser.id ?
            <div>
              <div>{currentUser.info.general.name}</div>
              <div>{currentUser.info.general.email}</div>

              </div>
            :
            <div onClick={()=>router.push('/auth/login')}>SIGNIN / SIGNUP</div>
          }
          </div>
          {collapseItems()}
        </div>
      </div>
    </nav>
  );
}

export default MobileNavBar;
