import React from "react";
import { Dialog, DialogOverlay, DialogContent } from "@reach/dialog";
import "@reach/dialog/styles.css";

const Modal = ({ isOpen, onClose, onConfirm }:any) => {
  return (
    <DialogOverlay isOpen={isOpen} onDismiss={onClose}>
      <DialogContent>
        <p>Some Content</p>
        <button onClick={onConfirm}>Confirm</button>
        <button onClick={onClose}>Cancel</button>
      </DialogContent>
    </DialogOverlay>
  );
};

export default Modal;
