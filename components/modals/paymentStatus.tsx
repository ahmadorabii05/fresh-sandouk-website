import React from 'react';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';

function PaymentStatusModal({ isModalOpen, close }: any) {
  return (
    <Modal show={isModalOpen} onHide={() => close(null)}>
      <Modal.Header closeButton>
        <Modal.Title className="text-danger">Payment Failed</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <p className="text-muted">
          Unfortunately, your payment was unsuccessful. Please try again with a different payment method or card.
        </p>
      </Modal.Body>
      <Modal.Footer>
        {/* <Button variant="secondary" onClick={() => close(null)}>
          Close
        </Button> */}
        <Button variant="success" onClick={() => close('try-again')}>
          Try Again
        </Button>
      </Modal.Footer>
    </Modal>
  );
}

export default PaymentStatusModal;
