import { useState, useEffect } from "react";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import addListImage from "../../public/images/addtolist.jpg";
import Image from "next/image";
import {
  createNewList,
  getListsPerUser,
  toggleProductsInList,
} from "@/network/favorites";
import { toast } from "react-toastify";
import AddSvg from '../../public/icons/add.svg'
import checkSVG from '../../public/icons/check.svg'
import prepareErrorResponse from "@/errorHandlerLayer/errorHandler";
function ShoppingListModal({ isModalOpen, close, product = { id: "" } }: any) {
  const [lists, setLists]: any = useState([]);
  const [isCreateListFormOpen, setIsCreateListFormOpen]: any = useState(false);
  const [newListName, setNewListName]: any = useState("");

  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    // Fetch lists and products data based on selectedList
    // Update the state accordingly
    setIsLoading(true);
    getListsPerUser().then((res) => {
      setLists(res.data.data.docs);
      setIsLoading(false);
    }).catch(err=>{
      setIsLoading(false);
      toast.error(prepareErrorResponse(err))
    });
  }, []);

  const isInList = (list: any) => {
    const productsIds = list.productIds;
    return productsIds.find((pr:any)=>product.id == pr.id);
  };

  const create = () => {
    createNewList({ name: newListName })
      .then((res) => {
        const newList = res.data.data;
        setLists((prevLists: any) => [...prevLists, newList]);
        setIsLoading(false);
        setIsCreateListFormOpen(false);
        setNewListName("");
        toast.success("List created successfully");
      })
      .catch((err) => {
        toast.error(prepareErrorResponse(err));
        setIsLoading(false);
      });
  };

  const toggleInList = (list: any) => {
    const productsIds = list.productIds;
    const productId = product.id;

    if (productsIds.find((pr:any)=>product.id == pr.id)) {
      // If product already exists in the list, remove it
      toggleProductsInList({ productId, listId: list.id })
        .then((res) => {
          const updatedProductIds = productsIds.filter(
            (pr: any) => pr.id !== productId
          );
          list.productIds = updatedProductIds;
          // Update the lists state with the modified list
          const updatedLists = lists.map((l: any) =>
            l.id === list.id ? list : l
          );
          setLists(updatedLists);
          toast.success("Item removed to list successfully.");
        })
        .catch((err) => {
          toast.error(prepareErrorResponse(err));
        });
    } else {
      // If product doesn't exist in the list, add it
      toggleProductsInList({ productId, listId: list.id })
        .then((res) => {
          productsIds.push(product);

          // Update the lists state with the modified list
          const updatedLists = lists.map((l: any) =>
            l.id === list.id ? list : l
          );
          setLists(updatedLists);
          toast.success("Item added from list.");
        })
        .catch((err) => {
          toast.error(prepareErrorResponse(err));

        });
    }
  };

  return (
    <>
      <Modal show={isModalOpen} onHide={() => close(null)}>
        <div className="wrapper p-4">
          <div className="add-to-list">ADD TO LIST</div>
          <div className="flex flex-row items-center d-flex v-center my-3 py-2">
            <div>
              <Image
                className="add-to-list-image"
                src={addListImage}
                alt="add"
              />
            </div>
            <div className="fs-14 mx-2 text-light-grey fs-14">You are adding Tango Mandarines</div>
          </div>

          <div
            className="list-name-box my-2 text-light-grey fs-14 text-center"
            onClick={(e) => setIsCreateListFormOpen(!isCreateListFormOpen)}
          >
            Create new list
          </div>
          {isCreateListFormOpen && (
            <div className="form">
              <input
                className="form-control"
                placeholder="Type list name"
                onChange={(e) => setNewListName(e.target.value)}
              />
              <button className="primary my-3" onClick={create}>
                Save
              </button>
            </div>
          )}
          <p className="p text-light-grey fs-14">Select an existing list, or create a new one:</p>
          <div>
            {lists &&
              lists.map((list: any,index:number) => {
                return (
                  <div className="list-name-box my-2 text-light-grey fs-14" key={index}>
                    <div>{list.name}</div>
                    <div onClick={(e) => toggleInList(list)}>
                      <Image src={isInList(list) ? checkSVG : AddSvg} alt="check" />
                    </div>
                  </div>
                );
              })}
          </div>
        </div>
      </Modal>
    </>
  );
}

export default ShoppingListModal;
