import React from "react";
import { Carousel } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import { Autoplay, Pagination, Navigation } from "swiper";
import { Swiper, SwiperSlide } from "swiper/react";
import Image from 'next/image'
// Import Swiper styles
import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/navigation";

const MyCarousel = ({ bannerDetails, marginBottom }: any) => {
  return (
    <>
      <Swiper
        spaceBetween={30}
        centeredSlides={true}
        autoplay={{
          delay: 2500,
          disableOnInteraction: false,
        }}
        pagination={{
          clickable: true,
        }}
        navigation={true}
        modules={[Autoplay, Pagination, Navigation]}
        className="mySwiper"
        style={{ marginBottom: marginBottom ?? "10px" }}
      >
        {
          bannerDetails?.map((data: any, index: any) => {
            return <SwiperSlide key={index}>
              <img onClick={() => {
                if (data?.actionUrl) {
                  window.open(data.actionUrl, "_self")
                }
              }} src={data.imageUrl} alt="swiper image" />
            </SwiperSlide>
          })
        }
      </Swiper>
    </>
  );
};

export default MyCarousel;
