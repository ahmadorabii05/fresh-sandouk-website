import { useRouter } from 'next/router';
import { useSelector } from 'react-redux';

const PrivateRoute = ({ children }:any) => {
  const router = useRouter();
  const { user } = useSelector((state:any) => state.user);

  if (!(user && user.id)) {
    // redirect the user to the login page
    router.push('/auth/login');
    return null;
  }

  // allow access to the protected route
  return children;
};

export default PrivateRoute;
