import React, { useState, useTransition } from "react";
import HeartIcon from "./icons/heartIcon";
import { useRouter } from "next/router";
import { useDispatch, useSelector } from "react-redux";
import {
  addToCart,
  decreaseCartItemQuantity,
  removeFromCart,
} from "@/store/reducers/cartReducer";
import {
  toggleFavoriteItem,
  toggleShoppingList,
} from "@/store/reducers/favoritesReducer";
import Image from "next/image";
import { toast } from "react-toastify";
import { Modal } from "react-bootstrap";
import ShoppingListModal from "./modals/shoppingListModal";
import AddToFavoritesIcon from "./icons/AddToFavoritesIcon";
import AddToListIcon from "./icons/AddToListIcon";

import trashImage from "../public/images/trash-blue.png";
import addToCartImage from "../public/images/add-to-cart-blue.png";
import openListImage from "../public/images/open-list-blue.png";
import prepareErrorResponse from "@/errorHandlerLayer/errorHandler";
import Cookies from "js-cookie";
import { isProductDiscounted } from "@/helpers/productHelpers";
import useTranslation from "next-translate/useTranslation";

const ProductItem = ({ product, noImage, flex, itemType = '', removeFromList }: any) => {
  const router = useRouter();
  const {t} = useTranslation('common');
  const dispatch: any = useDispatch();
  const cartItems = useSelector((state: any) => state.cart.cartItems);
  const cartItem = cartItems.find((item: any) => item.id === product.id);
  const favorites = useSelector((state: any) => state.favorites.favorites);
  const favoriteItem = favorites.find((item: any) => item.id === product.id);
  const shoppingList = useSelector(
    (state: any) => state.favorites.shoppingList
  );
  const shoppingItem = shoppingList.find((item: any) => item.id === product.id);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [isAddModalOpen, setIsAddModalOpen] = useState(false)
  const handleAddToCart = () => {
    dispatch(addToCart(product));
    // toast.success("Item added to your cart.");
  };


  const handleRemoveFromCart = () => {
    dispatch(removeFromCart(product));
    // toast.success("Item Removed to your cart.");
  };


  const handleDecreaseItemsFromCart = () => {
    dispatch(decreaseCartItemQuantity(product));
    // toast.info("Item removed from your cart.");
  };
  const handleToggleFavorite = () => {
    dispatch(toggleFavoriteItem(product));
    toast.success("Your favorite list updated");
  };
  const handleToggleShoppingItem = () => {

    dispatch(toggleShoppingList(product));
  };

  const openModal = () => {
    if (Cookies.get('token')) {
      setIsModalOpen(true);
    } else {
      router.push('/auth/login')
    }
  };

  const closeModal = (data: any) => {
    if (data) {
      setIsModalOpen(false);
    } else {
      setIsModalOpen(false);
    }
  };

  const handleHover = (bl: boolean) => {
    setIsAddModalOpen(bl);
  };

  const handleTrashClick = () => {
    switch (itemType) {
      case 'favorite':
        handleToggleFavorite()
        removeFromList()
        // remove from fav
        break;
      case 'list':
        removeFromList(product)
        // remove from list
        // handleToggleShoppingItem()
        break;
      case 'cart':
        // remove from cart
        handleRemoveFromCart()
        break;
      default:
        handleRemoveFromCart()

      // remove from cart
    }
  }

  const getSourceName = () => {
    if (product && product.brand) {

      if (product.brand && product.brand.source == "Freshsandouk") {
        return product.country.country
      } else {
        return product.brand.source
      }
    } else {
      return '-'
    }
  }

  const handleSourceClick = () => {
    if (product.brand && product.brand.source == "Freshsandouk") {
      router.push('/category/shopping/country/query/' + product.country.country)

    } else {
      router.push('/category/shopping/brand/query/' + product.brand.source)

    }
  }

  const getTotalItemAmount = () => {
    return 'AED ' + (parseFloat(cartItem.finalPriceAfterDiscount) * cartItem.quantityInCart).toFixed(2)
  }


  if (!(product && product.name)) {
    return <div>{t('Loading...')}</div>;
  }
  return (
    <div className={"product-item" + (flex ? " flex" : (noImage ? ' no-img ' : '') + (itemType == 'cart' ? ' cart-item' : ''))}>
      {isModalOpen && <ShoppingListModal
        isModalOpen={isModalOpen}
        close={closeModal}
        product={product}
      />}
  
      {
        flex && <div className="blue-options">
          <div className="link curser-pointer" onClick={handleTrashClick}>
            <Image className="blue" alt={t('trash')} src={trashImage} />
          </div>
          <div className="link curser-pointer" onClick={handleAddToCart}>
            <Image className="blue" alt={t('add-to-cart')} src={addToCartImage} />
          </div>
          {itemType != 'cart' && <div className="link curser-pointer" onClick={() => setIsModalOpen(true)}>
            <Image className="blue" alt={t('open-list')} src={openListImage} />
          </div>}
  
  
        </div>
      }
      <div className={flex ? "d-flex w-100" : "d-block w-100"}>
        <div onClick={() => router.push("/product/details/" + product.id)}>
          {noImage ? (
            <div></div>
          ) : (
            <img
              alt={t('product-image')}
              className="product-item-image c-pointer"
              src={
                product.imageUrls &&
                product.imageUrls.length &&
                product.imageUrls[0].imageUrl
              }
            />
          )}
        </div>
        <div className={noImage ? 'no-img-product' : ''}>
          <div className="product-item-name my-1">{product.name}</div>
          <div className={"product-item-country mb-1 text-fixed clickable link curser-pointer " + (itemType == 'cart' && 'text-right') }onClick={handleSourceClick}>{t(getSourceName())}</div>
          <div className={"mb-1 "  + (itemType == 'cart' && 'text-right') }>
            {isProductDiscounted(product) ? (
              <span>
                <span className="product-item-price-final ">
                  {product.finalPriceAfterDiscount}
                </span>
                <span className="product-item-oldPrice">
                  {product.price.toFixed(2)}
                </span>
              </span>
            ) : (
              <span className="item-price">
                {product.finalPriceAfterDiscount}
              </span>
            )}
            <span className="item-price"> / {product.priceModel.unit}</span>
          </div>
          <div className={"product-item-unitDescription mb-1 text-fixed " + (itemType == 'cart' && 'text-right')}>
            {t(product.description || "1KG")}
          </div>
          <div className="tags">
            {!noImage && product.tags &&
              product.tags.map((tag: any, index: number) => {
                return <img className="tag-image" src={tag.imageUrl} key={index} />;
              })}
          </div>
        </div>
        <div>
          {product && !(product.stockQuantity != 'undefined' && product.stockQuantity == 0) ? <div className="add-to-cart-actions-container">
            <div className="text-right">
              {itemType == 'cart' && <span className="cart-price-text">{getTotalItemAmount()}</span>}
            </div>
            <div className="add-to-cart-actions">
              <div className="sub" onClick={handleDecreaseItemsFromCart}>
                -
              </div>
              <div className="add-card-input-container">
                <div className="quantity-number">
                  {cartItem && cartItem.quantityInCart
                    ? (cartItem.quantityInCart).toFixed(2)
                    : '0'}
                </div>
              </div>
              <div className="plus" onClick={handleAddToCart}>
                +
              </div>
            </div>
          </div> : <div>{t('Out Of Stock')}</div>}
  
          {!flex ? (
            <div className="add-to-fav-btn">
              <div className="add-to-fav ptr">
                {t('Add to Favourites')}
              </div>
              <div className="heart ptr" onMouseEnter={() => handleHover(true)} >
                <HeartIcon active={favoriteItem ? "true" : "false"} />
              </div>
              {isAddModalOpen && <div className="add-modal" onMouseLeave={() => handleHover(false)}>
                <div className="add-to-list-block" >
                  <div onClick={openModal} >
                    <AddToListIcon active={false} />
                  </div>
                  <div>{t('Add To List')}</div>
                </div>
                <div className="add-to-list-block"  >
                  <div onClick={handleToggleFavorite}>
  
                    <AddToFavoritesIcon active={favoriteItem ? true : false} />
                  </div>
                  <div>{t('Add To Favourites')}</div>
  
                </div>
              </div>}
            </div>
          ) : (
            ""
          )}
        </div>
      </div>
  
    </div>
  );
  
};

export default ProductItem;
