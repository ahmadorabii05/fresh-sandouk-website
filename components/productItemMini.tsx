import React from 'react'
import HeartIcon from './icons/heartIcon'
import { useRouter } from 'next/router';
import { useDispatch, useSelector } from 'react-redux';
import { addToCart, removeFromCart } from '@/store/reducers/cartReducer';
import { toggleFavoriteItem,toggleShoppingList } from '@/store/reducers/favoritesReducer';
import Image from 'next/image'
import { toast } from 'react-toastify';
import useTranslation from 'next-translate/useTranslation';
import { isProductDiscounted } from '@/helpers/productHelpers';
const ProductItemMini = ({product, noImage,flex}:any) =>{
    const {t} = useTranslation('common')
    const router = useRouter();
    const dispatch:any = useDispatch();
    const cartItems = useSelector((state: any) => state.cart.cartItems);
    const cartItem = cartItems.find((item: any) => item.id === product.id);
    const favorites = useSelector((state: any) => state.favorites.favorites);
    const favoriteItem = favorites.find((item: any) => item.id === product.id);
    const shoppingList = useSelector((state: any) => state.favorites.shoppingList);
    const shoppingItem = shoppingList.find((item: any) => item.id === product.id);

    const handleAddToCart = () => {
        dispatch(addToCart(product));
        toast.success('Item added to your cart.')


    };


    if(!(product && product.id)){
        return <div>Loading...</div>
    }

    return (
        <div className={'product-item mini ' + (flex  ? ' flex' : '')}>
            <div className={flex ? 'd-flex' : 'd-block'}>
                <div className='d-flex v-center justify-content-center'>
                {
                    noImage ? <div></div> : <img alt='product-image' className='product-item-image c-pointer' src={product.imageUrls && product.imageUrls.length && product.imageUrls[0].imageUrl} />
                }
                </div>
                <div style={{minHeight:'80px'}}>
                <div className='product-item-name'>{product.name}</div>
                <div className='fixed-height'>
                {isProductDiscounted(product) ? (
              <span>
                <span className="product-item-price-final ">
                  {product.finalPriceAfterDiscount}
                </span>
                <span className="product-item-oldPrice">
                  {product.price.toFixed(2)}
                </span>
              </span>
            ) : (
              <span className="item-price">
                {product.finalPriceAfterDiscount}
              </span>
            )}
            <span className="item-price"> / {product.priceModel.unit}</span>

                </div>
                </div>
                <div>
              <div className='add-to-fav-btn'  >
                    <div className='add-to-cart-mini' onClick={handleAddToCart}>{t('Add to cart')}</div>
                </div> 
                </div>
            </div>
        </div>
    );
}

export default ProductItemMini;
