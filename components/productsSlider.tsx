import React, { useEffect, useRef, useState } from "react";
// Import Swiper React components
import { Swiper, SwiperSlide } from "swiper/react";

// Import Swiper styles
import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/navigation";


// import required modules
import { Pagination, Navigation } from "swiper";
import ProductItem from "./productItem";
import ProductItemMini from "./productItemMini";

const ProductsSlider = ({title = '',products,mini}:any) =>{
    const [setSwiperRef]:any = useState(null);
    const normalBreakPoint = {
      340: {
        slidesPerView: 2.3,
        spaceBetween: 20,
      },
      500: {
        slidesPerView: 3.3,
        spaceBetween: 20,
      },
      900: {
        slidesPerView: 4.3,
        spaceBetween: 40,
      },
      1024: {
        slidesPerView: 5.3,
        spaceBetween: 50,
      },
      1424: {
        slidesPerView: 6.3,
        spaceBetween: 60,
      },
      1700: {
        slidesPerView: 8,
        spaceBetween: 60,
    },
    }
    const miniBreakPoint = {
      340: {
        slidesPerView: 3,
        spaceBetween: 20,
      },
      500: {
        slidesPerView: 4,
        spaceBetween: 20,
      },
      900: {
        slidesPerView: 6,
        spaceBetween: 40,
      },
      1024: {
        slidesPerView: 8,
        spaceBetween: 50,
      },
      1424: {
        slidesPerView: 10,
        spaceBetween: 60,
      },
      1700: {
        slidesPerView: 12,
        spaceBetween: 60,
    },
    }


  return (
    <div className="products-slider">
      <h1>{title}</h1>
     {products && products.length > 0 && <Swiper
           spaceBetween={50}
           slidesPerView={3}
        onSwiper={setSwiperRef}
        pagination={{
          type: "fraction",
        }}
        
        breakpoints={mini ? miniBreakPoint : normalBreakPoint}
        navigation={true}
        modules={[Pagination, Navigation]}
        className="mySwiper"
      >
        {
           products && products.length && products.map((product:any,index:number)=>{
                return <SwiperSlide key={index} >
                  {mini ? <ProductItemMini key={index} product={product}/> :<ProductItem product={product} key={index} /> }
                </SwiperSlide>
            })
        }
      </Swiper>}
    </div>
  );
}
export default ProductsSlider
