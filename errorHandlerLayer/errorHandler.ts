/**
 *
 * @param {errorResponseReceived} error
 * @returns {Cleaned up ErrorResponseModel}
 */
export default function prepareErrorResponse(error:any) {
  let message = error.response?.data?.message || "Something Went Wrong";
  return message;
}
