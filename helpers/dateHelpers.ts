import moment from "moment";

export function prepareDateString(date: any) {
    return moment(date).format('DD/MM/yyyy hh:mm A');
}
export function formatPrice(price: any) {
    if (price) {
        // Convert the input to a number
        const priceAsNumber = parseFloat(price);

        // Check if it's a valid number
        if (!isNaN(priceAsNumber)) {
            // Apply the toFixed function and convert the result back to a string
            return priceAsNumber.toFixed(2) + ' AED';
        }
    }

    return '';
}

export function formatDate(dateString: any) {
    const formattedDate = new Date(dateString).toLocaleString();
    return formattedDate;
}

export function toWeekDay(dateString: string) {
    var date = moment(dateString);
    if (date.isValid()) {
        return date.format("ddd")
    } else {
        return ""
    }
}

export function toWeekDayFullName(dateString: string) {
    var date = moment(dateString);
    if (date.isValid()) {
        return date.format("dddd")
    } else {
        return ""
    }
}

export function toMonthFormat(dateString: string) {
    var date = moment(dateString);
    if (date.isValid()) {
        return date.format("MMM DD")
    } else {
        return ""
    }
}

export function toAPIFormat(dateString: string) {
    var date = moment(dateString);
    if (date.isValid()) {
        return date.format("dddd DD/MM/yyyy")
    } else {
        return ""
    }
}


export function extractQueryFromUrl(url) {
    const urlObj = new URL(url);
    const pathParts = urlObj.pathname.split('/');
    const queryIndex = pathParts.indexOf('query');
    if (queryIndex !== -1 && queryIndex + 1 < pathParts.length) {
        return pathParts[queryIndex + 1];
    }
    return null;
}
