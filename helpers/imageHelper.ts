export function prepareSafeUrlUsingProxy(imageUrl:string) {
  const key = "dbpvq5efo"; // unique per project
  if (imageUrl) {
    return `https://res.cloudinary.com/${key}/image/fetch/${imageUrl}`;
  } else {
    return null;
  }
}
