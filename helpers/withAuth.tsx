import { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import Cookies from 'js-cookie';
import AppLoader from '@/components/loader';

const withAuth = (WrappedComponent:any) => {
  const WithAuth = (props:any) => {
    const router = useRouter();
    const [isLoading, setIsLoading] = useState(true);

    useEffect(() => {
      const checkAuthentication = async () => {
        // Check if the user is authenticated
        const isAuthenticated = await Cookies.get('token'); // Implement your own authentication logic here

        if (!isAuthenticated) {
          router.push('/auth/login'); // Redirect to login page or any other appropriate action
        }

        setIsLoading(false);
      };

      checkAuthentication();
    }, []);

    if (isLoading) {
      // Show loading indicator or any other desired UI while checking authentication
      return <AppLoader isLoading={true}></AppLoader>
    }

    return <WrappedComponent {...props} />;
  };

  return WithAuth;
};

export default withAuth;
