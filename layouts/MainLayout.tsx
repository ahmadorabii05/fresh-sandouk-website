import Footer from "@/components/footer";
import NavBar from "@/components/NavBar";
import { useDispatch } from "react-redux"; // import useDispatch hook
import React, { useEffect } from "react";
import { setCartItems } from "@/store/reducers/cartReducer";
import { setFavoritesItems, setShoppingList } from "@/store/reducers/favoritesReducer";
import ContainerRow from "@/components/containerRow";
import BottomMobileMenu from "@/components/bottomMobileMenu";
import { addToCartPostRequest, getMyCartGetRequest } from "@/network/cart";
import Cookies from "js-cookie";
import { getListsPerUser, getMyFavorites } from "@/network/favorites";

function MainLayout({ children }: any) {
  const dispatch:any = useDispatch(); // initialize dispatch

  const addToCartFromLocal = async (localItems: any[]) => {
    const productIds = localItems.map((item) => ({
      productId: item.id,
      quantity: item.quantityInCart,
    }));
    addToCartPostRequest({ productIds })
    .then((res) => {
      getMyCartGetRequest().then(res=>{
        const products = res.data.data.products
        if(products && products.length > 0){
          dispatch(setCartItems(products)); // use dispatch to initialize myFavorites in store
          localStorage.setItem("cartItems", "");
        }
      })
    })
    .catch((err) => { });
  }
  useEffect(() => {
      if(Cookies.get('token')){
        const cartItemsLocally = localStorage.getItem("cartItems");
        if (cartItemsLocally) {
          getMyCartGetRequest().then(res=>{
            const products = res.data.data.products
            if(products && products.length > 0){
              dispatch(setCartItems(products));
              localStorage.setItem("cartItems", "");
            } else {
              addToCartFromLocal(JSON.parse(cartItemsLocally));
            }
          }).catch(err=>{
            console.log(err)
          })
        } else {
          // fetch
          getMyCartGetRequest().then(res=>{
            const products = res.data.data.products
            if(products && products.length){
              dispatch(setCartItems(products)); // use dispatch to initialize myFavorites in store
            }
          }).catch(err=>{
            console.log(err)
          })
        }

        getMyFavorites().then(res=>{
          const products = res.data.data
          if(products && products.length){
            dispatch(setFavoritesItems(products));
          }
        }).catch(err=>{
          console.log(err)
        })

        getListsPerUser().then(res=>{
          const products = res.data.data
          if(products && products.length){
            dispatch(setShoppingList(products));
          }
        }).catch(err=>{
          console.log(err)
        })
      }

    
  }, [dispatch]); // add dispatch to dependency array to avoid eslint warning

  return (
    <div className="main-layout-wrapper">
        {/* <div className="only-desktop">
      <ContainerRow />
        </div> */}
      <div className="container">
        <NavBar />
      </div>
      <div className="page-content container">
        
        <div>{children}</div></div>
      <Footer />
      <div className="only-mobile">
        <BottomMobileMenu />
      </div>
    </div>
  );
}

export default MainLayout;
