import Cookies from "js-cookie";
import { networkCallGet, networkCallPatch, networkCallPost } from "./common/networkCalls";

/**
 * @param {RequestModel}
 * @returns {Promise<AxiosResponse<ResponseModel>>}
 */
export function login(requestModel:any) {
  return networkCallPost("auth/login", JSON.stringify(requestModel));
}

export function signup(requestModel:any) {
  return networkCallPost("auth/sign-up", JSON.stringify(requestModel));
}
export function sendResetLink(requestModel:any) {
  return networkCallPost("/auth/forgot-password", JSON.stringify(requestModel));
}

export function updateUser(requestModel:any) {
  return networkCallPatch("/users/mobile", JSON.stringify(requestModel));
}

export function changePassword(requestModel:any) {
  return networkCallPost("/auth/change-password", JSON.stringify(requestModel));
}
export function resetPassword(requestModel:any) {
  return networkCallPost("/auth/reset-password", JSON.stringify(requestModel));
}

export function createAddress(requestModel:any) {
  return networkCallPost("/address", JSON.stringify(requestModel));
}


export function getAddresses() {
  return networkCallGet("/address", null);
}

export function deleteAddresses(requestModel:any) {
  return networkCallPatch("/address", JSON.stringify(requestModel));
}

export function updateAddress(requestModel:any,addressId:string) {
  return networkCallPatch("/address/update-address/"+addressId, JSON.stringify(requestModel));
}
export function getCurrentUser() {
  if(Cookies.get('token')){
    return networkCallGet("/users/get-current-user-info",null);
  }
  return new Promise((resolve,reject)=>resolve(false))
}


