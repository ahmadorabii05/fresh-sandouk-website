import { networkCallGet } from "./common/networkCalls";

export function getAllBanners() {
    return networkCallGet("banner?limit=200", null);
  }