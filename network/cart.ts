import { networkCallGet, networkCallPatch, networkCallPost } from "./common/networkCalls";

/**
 * @param {RequestModel}
 * @returns {Promise<AxiosResponse<ResponseModel>>}
 */
export function addToCartPostRequest(requestModel: any) {
  return networkCallPost("cart", JSON.stringify(requestModel));
}

export function decreaseQuantityFromCartPatchRequest(requestModel: any) {
  return networkCallPatch("cart", JSON.stringify(requestModel));
}

export function removeItemsFromCartPatchRequest(requestModel: any) {
  return networkCallPatch("cart/delete-item-from-cart", JSON.stringify(requestModel));
}

export function getMyCartGetRequest() {
  return networkCallGet("cart", null);
}

export function createOrderRequest(requestModel: any) {
  return networkCallPost("order", requestModel);
}


export function getStripeKeys() {
  return networkCallPost("stripe", { a: 1 });
}

export function getStripeUrl(requestModel: any) {
  return networkCallPost("stripe/create-checkout-url", requestModel);
}

export function getOrderByID(orderId: any) {
  return networkCallGet("/order/" + orderId, null);
}
export function getOrders(userId: any) {
  return networkCallGet(`users/get-user-with-order/${userId}`, null);
}


