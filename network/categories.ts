import {networkCallGet, networkCallPost} from "./common/networkCalls";
import {extractQueryFromUrl} from "@/helpers/dateHelpers";

/**
 * @param {RequestModel}
 * @returns {Promise<AxiosResponse<ResponseModel>>}
 */
export function getAllCategories() {
    return networkCallGet("category?limit=200", null, false);
}

export function getAllsubCategory() {
    return networkCallGet("subCategory", null, false);
}

export function getAllcountry() {
    return networkCallGet("country", null, false);
}

export function getAllbrand() {
    return networkCallGet("brand", null, false);
}

export function getCategoryById(id: any) {
    return networkCallGet("category/" + id, null, false);
}

export function getAllProducts(params: any = null) {
    const current = window.location.href;
    const search = extractQueryFromUrl(current)
    if (search){
        params.search = search
    }
    let url = "products";
    if (params) {
        const keys = Object.keys(params);
        if (keys.length > 0) {
            url += "?";
            var temp = ""
            keys && keys.length && keys.forEach((key, index) => {
                if (`${params[key]}`.includes("&")) {
                    temp = `${params[key]}`.replace("&", "%26");
                } else {
                    temp = params[key];
                }
                url += `${key}=${temp}`;
                if (index !== keys.length - 1) {
                    url += "&";
                }
                temp = ""
            });
        }
    }
    console.log(url)
    return networkCallGet(url, null, false);
}

export function getProductById(id: any) {
    return networkCallGet("products/" + id, null);
}

export function getAllTags() {
    return networkCallGet("tags?limit=20", null);
}

export function getTagById(id: any) {
    return networkCallGet("tags/" + id, null);
}


export function updateMyFavorites(productIds: Array<string>) {
    return networkCallPost("favorite", {productIds});
}

export function getSubcategoryById(id: string) {
    return networkCallGet(`/subCategory/${id}`, null)
}

export function searchForProductsRequest(query: string) {
    return networkCallGet(`/products/get-by-search?searchQuery=` + query, null)
}




