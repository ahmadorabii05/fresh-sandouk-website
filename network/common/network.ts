import axios from "axios";
import Cookies from "js-cookie";
const testToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImlkIjoiNjQ1YmIyMGViYWM3MjM1ZjNhMzZhMGNmIn0sImlhdCI6MTY4NjY2MDU3M30.wSeQDC9HvjOdELUj0x3ZQCebQsgUf1-8fUNRZ-acQGU'
function isStatusValid(status: any) {
  return status >= 200 && status <= 300;
}

export function network(requireAuth = true) {
  const isLive = true
  return Promise.resolve(
    axios.create({
      // baseURL:'https://c14a-94-187-2-72.ngrok-free.app',
      baseURL: isLive ? `https://backend.freshsandouk.com` : `http://localhost:8080`,

      validateStatus: (status) => {
        return isStatusValid(status);
      },
      headers: prepareHeaders(requireAuth),
      timeout: 20000,
    })
  );
}

function prepareHeaders(requireAuth = true) {
  const token = Cookies.get("token") || '';
  const language = Cookies.get("language") || '';
  let headers: any = {
    "Content-Type": "application/json",
    'Accept-Language': language || 'en',
    "isAdminRequest": true
  };
  if (token && true) {
    headers = {
      ...headers,
      Authorization: `Bearer ${token}`,
    };
  }
  return headers;
}
