import { network } from "./network";
import { prepareFormData } from "./networkHelpers";

/**
 * Name: GET
 * @param params ==> {potential query params}
 * @returns {Promise<AxiosResponse<any>>}
 */
export function networkCallGet(path:string, params:any,requireAuth = true) {
  return network(requireAuth).then((call) => {
    return call.get(path, params);
  });
}

/**
 * Name: Post
 * @param path
 * @param params
 * @returns {Promise<AxiosResponse<any>>}
 */
export function networkCallPost(path:string, params:any) {
  return network().then((call) => {
    return call.post(path, params);
  });
}

/**
 * Name: Patch
 * @param path
 * @param params
 * @returns {Promise<AxiosResponse<any>>}
 */
export function networkCallPatch(path:string, params:any) {
  return network().then((call) => {
    return call.patch(path, params);
  });
}

/**
 * Name: Put
 * @param path
 * @param params
 * @returns {Promise<AxiosResponse<any>>}
 */
export function networkCallPut(path:string, params:any) {
  return network().then((call) => {
    return call.put(path, params);
  });
}

/**
 * Name: Delete
 * @param path
 * @param params
 * @returns {Promise<AxiosResponse<any>>}
 */
export function networkCallDelete(path:string, params:any) {
  return network().then((call) => {
    return call.delete(path, params);
  });
}

/**
 * Name: Form Data
 * @param path
 * @param params
 * @returns {Promise<AxiosResponse<any>>}
 */
// export function networkCallFormDataPost(path:string, params:any, fileParams:any) {
//   return network("multipart/form-data").then((call) => {
//     const data = prepareFormData(params);
//     if (fileParams) {
//       data.append("file", fileParams);
//     }
//     return call.post(path, data);
//   });
// }

/**
 * Name: Form Data
 * @param path
 * @param params
 * @returns {Promise<AxiosResponse<any>>}
 */
export function networkCallFormDataPatch(path:string, params:any, fileParams:any) {
  return network().then((call) => {
    return call.patch(path, params);
  });
}
