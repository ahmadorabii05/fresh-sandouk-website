export const preparePagination = (url:string, page:number, limit:number) => {
  var finalUrl = url;
  var pageFinal = page ?? 1;
  var limitFinal = limit ?? 1000;
  finalUrl += `&page=${pageFinal}`;
  finalUrl += `&limit=${limitFinal}`;
  return finalUrl.replace("?&", "?");
};


export const prepareFormData = (object:any) => {
  const formData = new FormData();
  Object.keys(object) && Object.keys(object).length && Object.keys(object).forEach((key) => {
    if (Array.isArray(object[key])) {
      object[key] && object[key].length && object[key].forEach((item:any) => {
        formData.append(`${key}[]`, item);
      });
    } else {
      formData.append(key, object[key]);
    }
  });
  return formData;
};
