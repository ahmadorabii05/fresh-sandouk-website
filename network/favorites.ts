import { networkCallFormDataPatch, networkCallGet, networkCallPost } from "./common/networkCalls";

/**
 * @param {RequestModel}
 * @returns {Promise<AxiosResponse<ResponseModel>>}
 */
export function toggleFavoritePostRequest(requestModel:any) {
  return networkCallPost("favorite", JSON.stringify(requestModel));
}

export function getMyFavorites() {
  return networkCallGet("products/my-favorite-products",null);
}


export function createNewList(requestModel:any) {
  return networkCallPost("list", JSON.stringify(requestModel));
}

export function updateList(requestModel:any) {
  return networkCallFormDataPatch("/list/edit-list", JSON.stringify(requestModel),null);
}

export function toggleProductsInList(requestModel:any) {
  return networkCallPost("/list/add-product-to-list", JSON.stringify(requestModel));
}
export function deleteList(requestModel:any) {
  return networkCallFormDataPatch("list", JSON.stringify(requestModel),null);
}

export function getListsPerUser() {
  return networkCallGet("/list/get-lists",null);
}








