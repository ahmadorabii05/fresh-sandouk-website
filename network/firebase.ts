import firebase from 'firebase/compat/app';
import 'firebase/compat/auth';
import { RecaptchaVerifier, getAuth, signInWithPhoneNumber } from 'firebase/auth';

const firebaseConfig = {
  apiKey: "AIzaSyBrual4AwWnLb2V0C9xssTWVQ_JeA8YLqo",
  authDomain: "kibsons-6f12c.firebaseapp.com",
  projectId: "kibsons-6f12c",
  storageBucket: "kibsons-6f12c.appspot.com",
  messagingSenderId: "640696600230",
  appId: "1:640696600230:web:f18a3d01a87038deb31667",
  measurementId: "G-0QJ2QGNSP3"
};

firebase.initializeApp(firebaseConfig);
; // Access the auth module
const auth = getAuth()
const requestVerificationCodeFirebase = (phoneNumberWithCountryCode: string) => {
    return new Promise((resolve,reject)=>{
      const recaptchaVerifier =  new RecaptchaVerifier(auth,'recaptcha-container', {
        size: 'invisible'
      });
      signInWithPhoneNumber(auth,phoneNumberWithCountryCode, recaptchaVerifier)
        .then((confirmationResult: any) => {
         resolve(confirmationResult)
        })
        .catch((error: any) => {
          // Handle error
          reject(error)
        });
    })

};

const handleVerifyCodeFirebase = (verificationCode: string, confirmationResult: any) => {
  return new Promise((resolve,reject)=>{
    const verificationCodeEntered = verificationCode.trim();

    if (confirmationResult) {
      confirmationResult.confirm(verificationCodeEntered)
        .then((result: any) => {
          // Phone number successfully verified
          const user = result.user;
          resolve(user)
        })
        .catch((error: any) => {
          // Handle error
          reject(error)
          console.log(error);
        });
    }
  })

};

export { requestVerificationCodeFirebase, handleVerifyCodeFirebase };
