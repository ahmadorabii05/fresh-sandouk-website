import { networkCallGet } from "./common/networkCalls";

export function getTimeSlots(currentTimeStamp: number, city: string) {
    return networkCallGet(`/order/time-slots?timeStamp=${currentTimeStamp}&city=${city}&numberOfOptions=5`, null, false);
}