// src/pages/_app.tsx

import React, { useState, useEffect } from 'react';

import { AppProps } from 'next/app';
import '../styles/globals.scss';
import { Provider } from 'react-redux';
import store from '../store/index';
import AppHead from '@/components/Head';
import MainLayout from '@/layouts/MainLayout';
import 'react-toastify/dist/ReactToastify.css';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css'
import {useRouter} from 'next/router'
function MyApp({ Component, pageProps }: AppProps) {
  const router = useRouter()
  return (
    <div className={router.locale == 'ar' ? 'arabic-locale' : ''}>
    <AppHead />
    <Provider store={store}>
      <MainLayout>
        <Component {...pageProps} key={router.asPath} />
        <ToastContainer />
      </MainLayout>
    </Provider>
    </div>
  );
}

export default MyApp;
