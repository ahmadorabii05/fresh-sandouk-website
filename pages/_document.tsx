import Document, { Head, Html, Main, NextScript } from 'next/document'

class MyDocument extends Document {
  render() {
    return (
      <Html lang="en">
        <Head>
          <link
            rel="preload"
            href="/fonts/gillSans.ttf"
            as="font"
            crossOrigin=""
          />
          <style>
            {`
              @font-face {
                font-family: 'GillSansmt';
                src: url('/fonts/gillSans.ttf');
                font-weight: normal;
                font-style: normal;
                font-display: swap;
              }
            `}
          </style>
          <link
            rel="preload"
            href="/fonts/kibsons.ttf"
            as="font"
            crossOrigin=""
          />
          <style>
            {`
              @font-face {
                font-family: 'kibfonts';
                src: url('/fonts/kibsons.ttf');
                font-weight: normal;
                font-style: normal;
                font-display: swap;
              }
            `}
          </style>
          <link
            rel="preload"
            href="/fonts/signPainter.ttf"
            as="font"
            crossOrigin=""
          />
          <style>
            {`
              @font-face {
                font-family: 'SignPainter';
                src: url('/fonts/signPainter.ttf');
                font-weight: normal;
                font-style: normal;
                font-display: swap;
              }
            `}
          </style>
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    )
  }
}

export default MyDocument
