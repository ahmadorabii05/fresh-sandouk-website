import React, { useState, useEffect } from "react";
import Image from "next/image";
import addressImage from "../../../public/images/address.png";
import Accountcomponent from "@/components/accountComponent";

import { createAddress, getAddresses, updateAddress } from "@/network/auth";
import { toast } from "react-toastify";
import { useRouter } from "next/router";
import prepareErrorResponse from "@/errorHandlerLayer/errorHandler";
import MapContainer from "@/components/googlemap";
import mainCitiesList from "../../../constants/maincities.json"; 
import Select from 'react-select'
import useTranslation from "next-translate/useTranslation";

function AddressDetails() {
  const {t} = useTranslation('common')

  const router = useRouter()
  const { addressId } = router.query;
  const [isMapOpen, setIsMapOpen] = useState(false)
  const [errorMessage, setErrorMessage] = useState("");
  const [formData, setFormData]: any = useState({
    addressTitle: "",
    address: "",
    street: "",
    nearestLand: "",
    contactNumber: "",
    // emirate: "",
    isDefaultAddress: false,
    location: {}
  });

  useEffect(() => {
    if (addressId !== 'new') {
      getAddresses().then(res => {
        const address = res.data.data.docs.find((address: any) => address.id == addressId)
        setFormData(address)
      })
    }
  }, [])
  const handleFormSubmit = (event: any) => {
    event.preventDefault();
    if (!validateForm()) {
      return;
    }
    // Add your logic here to handle form submission
    // Save the form data or perform any desired actions
    // Reset the form
    const newAddress = {
      "addressTitle": formData.addressTitle,
      "address": formData.address,
      "contactNumber": formData.contactNumber,
      "street": formData.street,
      "nearestLand": formData.nearestLand,
      "isDefaultAddress": formData.isDefaultAddress,
      "location": formData.location

    }
    if (formData.id) {
      updateAddress(newAddress, formData.id).then(res => {
        toast.success('Address updated successfully')
        router.push('/account/myaddresses')
      }).catch(err => {
        toast.error(prepareErrorResponse(err))
      })
    } else {
      createAddress(newAddress).then(res => {
        toast.success('Address created successfully')
        router.push('/account/myaddresses')
      }).catch(err => {

        toast.error(prepareErrorResponse(err))
      })
    }

  };

  const openMapModal = () => {
    setIsMapOpen(true)

  }
  const callBack = (location: any) => {
    setFormData((prevData: any) => ({
      ...prevData,
      location: location || {},
    }));
    setIsMapOpen(false)
  }

  const handleChange = (event: any) => {
    const { name, value, type, checked } = event.target;
    const fieldValue = type === "checkbox" ? checked : value;

    setFormData((prevData: any) => ({
      ...prevData,
      [name]: fieldValue,
    }));
  };


  const getCityList = () => {
    var dubaiListFinal = mainCitiesList.Dubai.areas.map((val) => { return { label: val, value: val }});
    var abudhabiListFinal = mainCitiesList.Abu_Dhabi.areas.map((val) => { return { label: val, value: val }});
    return [...dubaiListFinal, ...abudhabiListFinal]
  }


  const validateForm = () => {
    if (!formData.addressTitle || !formData.address || !formData.contactNumber) {
      // Display an error message to inform the user that the required fields are missing
      // You can use a state variable to show the error message
      toast.error('Please fill in all required fields');
      return false;
    }

    if (formData.address.trim() === "") {
      setErrorMessage("Please enter an address");
      return false;
    }
    if (formData.emirate === "") {
      setErrorMessage("Please select an emirate");
      return false;
    }
    // Add more validations if needed
    setErrorMessage("");
    return true;
  };


  return (
    <div className="mx-2 p-3 relative">
      {isMapOpen && <div className="map-container">
        <MapContainer callBack={callBack} />
      </div>}
      <div className="d-flex p-relative justify-content-between items-center">
        <div className="d-flex p-relative">

          <Image alt="myaddress" className="lg-image" src={addressImage} />
          <h1 className="mx-3">{t("My Addresses")}</h1>
        </div>
        <div className="map-pin-btn">
          <button className="primary update-pin" onClick={openMapModal}>Update pin</button>
        </div>
      </div>
      <h4 className="text-edit my-4">
        {formData.id ? 'EDIT' : 'ADD'} ADDRESS
      </h4>

      <br />
      <form className="address-form" onSubmit={handleFormSubmit}>
        <div className="address-address">
          <div className="address-title">Select Address Title :</div>

          <div
            className={`addressTitle ${formData.addressTitle === "Home" ? "selected" : ""}`}
            onClick={() =>
              setFormData((prevData: any) => ({
                ...prevData,
                addressTitle: "Home",
              }))
            }
          >
            Home
          </div>
          <div
            className={`addressTitle ${formData.addressTitle === "Office" ? "selected" : ""
              }`}
            onClick={() =>
              setFormData((prevData: any) => ({
                ...prevData,
                addressTitle: "Office",
              }))
            }
          >
            Office
          </div>
          <div
            className={`addressTitle ${formData.addressTitle === "Hotel" ? "selected" : ""}`}
            onClick={() =>
              setFormData((prevData: any) => ({
                ...prevData,
                addressTitle: "Hotel",
              }))
            }
          >
            Hotel
          </div>
          <div
            className={`addressTitle ${formData.addressTitle === "Other" ? "selected" : ""}`}
            onClick={() =>
              setFormData((prevData: any) => ({
                ...prevData,
                addressTitle: "Other",
              }))
            }
          >
            Other
          </div>
        </div>
        <Select
          className="input-city-address p-0"
          options={getCityList()}
          placeholder={formData.address || 'Select Area'}
          defaultValue={formData.address}
          onChange={(selectedOption) => setFormData({ ...formData, address: selectedOption?.value })}
        />
        {/* <div className="address-field my-2">
          <input
            className={`text-box ${errorMessage && "is-invalid"}`}
            type="text"
            name="city"
            placeholder="City *"
            value={formData.address}
            onChange={handleChange}
            required
          />
        </div> */}
        {errorMessage && (
          <div className="invalid-feedback">{errorMessage}</div>
        )}
        {/* <div className="address-field my-2">
          <input
            className={`text-box ${errorMessage && "is-invalid"}`}
            type="text"
            name="address"
            placeholder="Address *"
            value={formData.address}
            onChange={handleChange}
            required
          />
        </div>
        {errorMessage && (
          <div className="invalid-feedback">{errorMessage}</div>
        )} */}
        {/* <div className="address-field my-2">
          <input
            className="address-input text-box"
            type="text"
            name="apartment"
            placeholder="Apartment"
            value={formData.apartment}
            onChange={handleChange}
          />
        </div> */}
        <div className="address-field my-2">
          <input
            className="address-input text-box"
            type="text"
            name="street"
            placeholder="address, Street, Apt No, etc *"
            value={formData.street}
            onChange={handleChange}
          />
        </div>
        <div className="address-field my-2">
          <input
            className="address-input text-box"
            type="text"
            name="nearestLand"
            placeholder="Nearest Land"
            value={formData.nearestLand}
            onChange={handleChange}
          />
        </div>
        <div className="address-field my-2">
          <input
            className="address-input text-box"
            type="tel"
            name="contactNumber"
            placeholder="Contact number * (+97150000000)"
            value={formData.contactNumber}
            onChange={handleChange}
          />
        </div>
        {/* <div className="address-field my-2">
          <select
          className="text-box"
            name="emirate"
            value={formData.emirate}
            onChange={handleChange}
            required
          >
            <option value="">Select emirate</option>
            <option value="Dubai">Dubai</option>
            <option value="Abu Dhabi">Abu Dhabi</option>
            <option value="Sharjah">Sharjah</option>
            <option value="Ajman">Ajman</option>
            <option value="Umm Al Quwain">Umm Al Quwain</option>
            <option value="Ras Al Khaimah">Ras Al Khaimah</option>
            <option value="Fujairah">Fujairah</option>
          </select>
        </div> */}
        <div className="address-field my-2">
          <label htmlFor="default-address">Make default address</label>
          <input
            id="default-address"
            type="checkbox"
            name="isDefaultAddress"
            checked={formData.isDefaultAddress}
            onChange={handleChange}
          />
        </div>
        <div className="address-field my-2">
          <button type="submit" className="primary save-address w-50" >Save Address</button>
        </div>
      </form>
    </div>
  );
}

export default Accountcomponent({
  Component: AddressDetails,
  addressTitle: ("Address Details"),
});
