import { useSelector, useDispatch } from 'react-redux';
import { useState } from 'react';

import { changePassword } from '@/network/auth';
import { toast } from 'react-toastify';
import Image from 'next/image'
import lockImage from "../../public/icons/Change Password.png";
import prepareErrorResponse from '@/errorHandlerLayer/errorHandler';
import useTranslation from 'next-translate/useTranslation';

const MyProfile = (props: any) => {
  const {t} = useTranslation('common')

  const user = useSelector((state:any) => state.user.user);
  const [passwordData, setPasswordData] = useState({ oldPassword: '', newPassword: '', confirmPassword: '' });

  const handleUpdatePassword = () => {
    if (passwordData.newPassword !== passwordData.confirmPassword) {
      toast.error('Passwords do not match');
      return;
    }

    if (passwordData.newPassword.length < 8) {
      toast.error('Password should be at least 8 characters long');
      return;
    }
    const updateData = {
      email:user.info.general.email,
      password:passwordData.newPassword
    }
    changePassword(updateData).then(res=>{
      toast.success('Password updated successfully')
    }).catch(err=>{
    toast.error(prepareErrorResponse(err))
    })
  };

  return (
    <div className="mx-2 p-3">
      <div className="col-md-12">
        <div className="d-flex v-center my-3">
          <Image alt="lock" className="lg-image" src={lockImage} />
          <h1 className="mx-3">{t('Change Password')}</h1>
        </div>
      </div>
      <br />
      <div className="row">
        <div className="col-md-6">
          <div className="form">
            <div className="input-label-text">{t('Old Password')}</div>
            <input type='password'
              className="text-box"
              placeholder={t('Type old password')}
              value={passwordData.oldPassword}
              onChange={e => setPasswordData({ ...passwordData, oldPassword: e.target.value })}
            />
            <br />
            <br />
            <div className="input-label-text">{t('New Password')}</div>
            <input type='password'
              className="text-box"
              placeholder={t('Type new password')}
              value={passwordData.newPassword}
              onChange={e => setPasswordData({ ...passwordData, newPassword: e.target.value })}
            />
            <br />
            <br />
            <div className="input-label-text">{t('Confirm New Password')}</div>
            <input type='password'
              className="text-box"
              placeholder={t('Type new password again')}
              value={passwordData.confirmPassword}
              onChange={e => setPasswordData({ ...passwordData, confirmPassword: e.target.value })}
            />
            <br />
            <br />
            <button className="btn-1 blue" onClick={handleUpdatePassword}>{t('Reset Password')}</button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default MyProfile;
