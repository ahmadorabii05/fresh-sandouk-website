import Accountcomponent from "@/components/accountComponent";
import React from "react";
import Image from "next/image";
import deleteImage from "../../public/icons/Delete Account.png";

import useTranslation from "next-translate/useTranslation";

const MyProfile = (props: any) => {
  const {t} = useTranslation('common')

  return (
    <div className="mx-2 p-3">
      <div className="col-md-12">
        <div className="d-flex v-center my-3">
          <Image alt="deleteImage" className="lg-image" src={deleteImage} />
          <h1 className="mx-3">{t('Delete Account')}</h1>
        </div>
      </div>
      <br />
      <div className="row">
        <div className="col-md-6">
          <div className="form">
            <div className="input-label-text">{t('Email')}</div>
            <input className="text-box" placeholder={t('Type your email')} />
            <br />
            <br />
            <div className="input-label-text">{t('Password')}</div>
            <input className="text-box" placeholder={t('Type your password')} />
            <br />
            <br />
            <button className="btn-1 blue">{t('Delete Account')}</button>
          </div>
        </div>

      </div>
    </div>
  );
};

export default Accountcomponent({
  Component: MyProfile,
  title: "Delete Account",
});
