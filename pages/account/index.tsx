// src/pages/index.tsx

import Accountcomponent from "@/components/accountComponent";
import AccountMenu from "@/components/accountMenu";
import React from "react";
import userSVG from "../../public/user.svg";
import Image from 'next/image'
import { useSelector } from "react-redux";

function Competition() {
  const user = useSelector((state:any)=>state.user.user)
  if(!(user && user.info)){
    return <div></div>
  }
  return (
    <div className="wrapper mb-3">
      <div className="mobile-profile-header text-center">
        <div>
          <Image alt="user-svg" src={userSVG} />
        </div>
        <div>
        <h1 className="text-white">{user.info?.general?.name}</h1>
        </div>
        <div>{user.info?.general?.email}</div>
      </div>
      <AccountMenu />
    </div>
  );
}

export default  Competition;
