import Accountcomponent from "@/components/accountComponent";
import React from "react";
import Image from "next/image";
import accountPointsImage from "../../public/images/accountpoints.png";

import useTranslation from "next-translate/useTranslation";

const MyAccountPoints = (props: any) => {
  const {t} = useTranslation('common')

  return (
    <div className="mx-2 p-3 row">
      <div className="col-md-6">
        <div className="d-flex v-center my-3">
          <Image alt="account" className="lg-image" src={accountPointsImage} />
          <h1 className="mx-3">{t('Account Points')}</h1>
        </div>
        <div>
          <h2 className="account-sub-heading">{t('MY STATUS')}</h2>

          <div className="account-type-box">
            <div className="d-flex">
              <div className="green field">{t('ACCOUNT TYPE')}</div>
              <div className="navy field">{t('STANDARD')}</div>
            </div>
            <div className="navy field bdr">0 {t('ACCOUNT POINTS')}</div>
            <div className="white field">
              {t('You need 5000 more points to reach SILVER')}
            </div>
          </div>
          <p>
           {t('Your Accounts points are equal to your total purchases at Kibsons during the past 6 months.')}
          </p>
        </div>
        <br />
        <h2 className="account-sub-heading bb-g">{t('BENEFITS')}</h2>
        <p>
          {t('Earn ACCOUNT POINTS and qualify for up to 15% discount off your groceries everyday by shopping with Kibsons.')}
        </p>
      <div>
        <table className="account-table table">
            <thead>

        <tr>
                    <td className="bg-green">{t('ACCOUNT BENEFITS')}</td>
                    <td className="silver">{t('SILVER')}</td>
                    <td className="gold">{t('GOLD')}</td>
                    <td className="platinum">{t('PLATINUM')}</td>
                </tr>
            </thead>
            <tbody>

                <tr>
                    <td className="thin-data">
                    {t('POINTS TO QUALIFY')}
                    </td>
                    <td className="bold-data">
                    5 000	
                    </td>       <td className="bold-data">
                    10 000	
                    </td>       <td className="bold-data">
                    15 000

                    </td>
                </tr>



                <tr>
                    <td className="thin-data">
                    {t('DISCOUNT ON FRUITS, VEGETABLES AND MEAT')}
                    </td>
                    <td className="bold-data">
                    -		
                    </td>       <td className="bold-data">
                    10% {t('discount')}		
                    </td>       <td className="bold-data">
                    15% {t('discount')}


                    </td>
                </tr>
                <tr>
                    <td className="thin-data">
                    {t('FREE EXPRESS DELIVERY')}
                    </td>
                    <td className="bold-data">
                    -	
                    </td>       <td className="bold-data">
                    -	
                    </td>       <td className="bold-data">
                    &#x2713;

                    </td>
                </tr>

                <tr>
                    <td className="thin-data">
                    {t('PRIORITY DELIVERY SERVICE')}	
                    </td>
                    <td className="bold-data">
                    -	
                    </td>       <td className="bold-data">
                    -	
                    </td>       <td className="bold-data">
                    &#x2713;

                    </td>
                </tr>

                <tr>
                    <td className="thin-data">
                    {t('DEDICATED CUSTOMER SERVICE	')}		
                    </td>
                    <td className="bold-data">
                    -	
                    </td>       <td className="bold-data">
                    &#x2713;
                    </td>       <td className="bold-data">
                    &#x2713;

                    </td>
                </tr>
                <tr>
                    <td className="thin-data">
                    {t('FREE SAMPLES')}			
		
                    </td>
                    <td className="bold-data">
                    &#x2713;	
                    </td>       <td className="bold-data">
                    &#x2713;
                    </td>       <td className="bold-data">
                    &#x2713;

                    </td>
                </tr>
            </tbody>
        </table>
      </div>
      </div>
      <div className="col-md-6">
        {" "}
        <div className="mini-statement">
          <h1>{t('Mini statement')}</h1>
          <table className="table">
            <thead>
              <tr>
                <td className="text-muted text-center">{t('MONTH')}</td>
                <td className="text-muted text-center">{t('SAVINGS')}</td>
                <td className="text-muted text-center">{t('TOTAL')}</td>
              </tr>
            </thead>
          </table>
        </div>
      </div>
    </div>
  );
};

export default Accountcomponent({
  Component: MyAccountPoints,
  title: "My points",
});
