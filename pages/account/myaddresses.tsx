import Accountcomponent from "@/components/accountComponent";
import ProductItem from "@/components/productItem";
import React ,{useState,useEffect} from "react";
import { useSelector } from "react-redux";
import Image from "next/image";
import Link from "next/link";
import addressImage from "../../public/icons/My address.png";
import editIcon from "../../public/icons/edit.png";
import deleteIcon from "../../public/icons/delete.png";

import { deleteAddresses, getAddresses, updateAddress } from "@/network/auth";
import { useRouter } from "next/router";
import AddressId from "./address/[addressId]";
import useTranslation from "next-translate/useTranslation";

const MyAddresses = (props: any) => {
  const {t} = useTranslation('common')

  const [addresses,setAddresses] = useState([

  ]);
  const router = useRouter()
  useEffect(()=>{
    getAddresses().then(res=>{
      setAddresses(res.data.data.docs)
    })
  },[])

  const handleDefaultAddressCheck = (e:any, index:any) => {
    const updatedAddresses:any = addresses.map((address:any, idx:any) => {
      
      if (idx === index) {
        address = { ...address, isDefaultAddress: true };
      } else {
        address =  { ...address, isDefaultAddress: false };
      }
      updateAddress(address,address.id)
      return address
    });
    setAddresses(updatedAddresses);
    
  };

  const handleDeleteAddress = (addressId:string) =>{
    deleteAddresses({addressIds:[addressId]})
    setAddresses(addresses.filter((add:any)=>add.id !== addressId))
  }
  return (
    <div className="mx-2 p-3">
      <div className=" d-flex">
        <Image alt="address" className="lg-image" src={addressImage} />
        <h1 className="mx-3">{t('My Addresses')}</h1>
      </div>

        <br />
      <div className="row">
        <div className="col-md-8">
        <div className="add-box">
            <div className="w-100 p-3">{t('Add new address')}</div>
            <Link href={'/account/address/new'} className="btn-link-1">+</Link>

        </div>
          {addresses && addresses.map((address: any, index: number) => {
            return (
              <div className="my-2 bordered-navy address-box" key={index}>
                <div className="d-flex w-100 justify-content-between">
                  <div className="">{address.title}</div>
                  <div>
                    <span onClick={()=>router.push('/account/address/'+address.id)}>
                      <Image alt="edit" className="icon--image" src={editIcon} />
                    </span>
                    <span onClick={()=>handleDeleteAddress(address.id)}>
                      {" "}
                      <Image alt="delete" className="icon--image" src={deleteIcon} />
                    </span>
                  </div>
                </div>
                <div className="mx-3">
                <input type="checkbox" checked={address.isDefaultAddress} onChange={(e)=>handleDefaultAddressCheck(e,index)} />
                  </div>
                <div className="p1">{address.addressTitle}</div>
                <div className="p1">{address.address}</div>
                <div className="p1">{address.street}</div>
                <div className="p1">{address.nearestLand}</div>
                <div className="p1">{address.contactNumber}</div>
                <div className="p1">{address.isDefaultAddress}</div>
              </div>
            );
          })}{" "}
        </div>
      </div>
    </div>
  );
};

export default Accountcomponent({
  Component: MyAddresses,
  title: "My Addresses",
});
