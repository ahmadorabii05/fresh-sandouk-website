import Accountcomponent from "@/components/accountComponent";
import ProductItem from "@/components/productItem";
import React,{useState,useEffect} from "react";
import { useSelector } from "react-redux";
import Image from "next/image";
import heartImage from "../../public/icons/My Favorites.png";
import shoppingList from "../../public/icons/My Shopping List.png";


import { getMyFavorites } from "@/network/favorites";
import { toast } from "react-toastify";
import prepareErrorResponse from "@/errorHandlerLayer/errorHandler";
import { useRouter } from "next/router";
import AppLoader from "@/components/loader";
import Cookies from "js-cookie";
import { setFavoritesItems } from "@/store/reducers/favoritesReducer";
import useTranslation from "next-translate/useTranslation";

const MyFavorites = (props: any) => {
  const {t} = useTranslation('common')

  const [favorites,setFavorites] = useState([])
  const [isLoading,setIsLoading] = useState(false)
  const router = useRouter()
  useEffect(()=>{
    if(Cookies.get('token')){
      setIsLoading(true)
      getMyFavorites().then(res=>{
        const favorites = res.data.data
        setFavorites(favorites)
        setIsLoading(false)
    }).catch(err=>{
      setIsLoading(false)
      toast.error(prepareErrorResponse(err))
    })
  }
  },[])

  const removeFromList = (fav:any)=>{
    setFavorites(favorites.filter((item:any)=>fav.id != item.id))
  }
  return (
    <div className="mx-2 p-3">
      {isLoading && <AppLoader isLoading={isLoading} />}
      <div className="d-flex justify-content-between">

      <div className=" d-flex">
      <Image alt="heart" className="lg-image" src={heartImage} />
      <h1 className="mx-3">{t('My Favorites')}</h1>
      </div>
      <div>
      <div className=" d-flex sm-link" onClick={()=>router.push('/account/myshoppinglist')}>
      <div className="mx-2 d-flex v-center">{t('View Shopping Lists')}</div>
      <Image alt="heart" className="lg-image" src={shoppingList} />
      </div>
      </div>
      </div>

      <div className="my-3">
        <div className="d-grid-50">
          {favorites && favorites.map((item: any, index: number) => {
            return (
              <div className="border-y mx-3" key={index}>
                <ProductItem product={item} flex={true} key={index} itemType='favorite' removeFromList={()=>removeFromList(item)} />
              </div>
            );
          })}{" "}
        </div>
      </div>
    </div>
  );
};

export default Accountcomponent({
  Component:  MyFavorites,
  title: "My Favorites",
});
