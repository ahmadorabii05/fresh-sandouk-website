import Accountcomponent from "@/components/accountComponent";
import React, { useEffect, useState } from "react";
import Image from "next/image";
import ordersImage from "../../public/icons/My Orders.png";

import { getOrderByID, getOrders } from "@/network/cart";
import { useSelector } from "react-redux";
import { formatDate, formatPrice } from "@/helpers/dateHelpers";
import AppLoader from "@/components/loader";
import { toast } from "react-toastify";
import useTranslation from "next-translate/useTranslation";

const MyStatement = (props: any) => {
  const {t} = useTranslation('common')

  const userRef = useSelector((state: any) => state.user.user);
  const [orders, setOrders] = useState([]);
  const [isLoading, setIsloading]: any = useState(false);
  useEffect(() => {
    setIsloading(true);
    getOrders(userRef.id)
      .then((ordersRes) => {
        const orders = ordersRes.data.data.orders;
        setOrders(orders);
        setIsloading(false);
      })
      .catch((er) => {
        setIsloading(false);
        toast.error(er);
      });
  }, []);
  return (
    <div className="mx-2 p-3 row">
      {isLoading && <AppLoader />}
      <div className="col-md-12">
        <div className="d-flex v-center my-3">
          <Image alt="orders" className="lg-image" src={ordersImage} />
          <h1 className="mx-3">{t("My Orders")}</h1>
        </div>
        {orders &&
          orders.map((order: any,index: number) => {
            return (
              <div key={index} className="card order p-3 shadow my-2">
                <div>
                  <div className="fs-12 text-muted">
                    Delivered On : {formatDate(order.createdAt)}
                  </div>
                  <hr />
                  {order.products &&
                    order.products.map((product: any, index: number) => {
                      return (
                        <div key={index} className="my-4">
                        <div className="d-flex justify-content-between">
                          <div className="text fs-16 d-flex">
                              <img className="product-order-image" src={product.imageUrls[0].imageUrl} />
                            <div className="mx-2">
                              <div>{product.name}</div>
                              <div className="text-green">x{product.quantity}</div>
                            </div>
                          </div>
                          <div>
                            <div className="text-muted">
                              {formatPrice(product.price)}
                            </div>
                          </div>
                        </div>
                        </div>

                      );
                    })}

                  <div className="total text-black fs-14 bold text-bold mt-1 text-right">
                    Total : {formatPrice(order.totalAmount)}
                  </div>
                </div>
              </div>
            );
          })}
      </div>
    </div>
  );
};

export default Accountcomponent({
  Component: MyStatement,
  title: "My Favorites",
});
