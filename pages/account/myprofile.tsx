import Accountcomponent from "@/components/accountComponent";
import React, { useEffect, useState } from "react";
import Image from "next/image";
import userImage from "../../public/icons/Update My Profile.png";

import { useSelector } from "react-redux";
import { useDispatch } from "react-redux";
import { updateUser } from "@/network/auth";
import { toast } from 'react-toastify';
import prepareErrorResponse from "@/errorHandlerLayer/errorHandler";
import useTranslation from "next-translate/useTranslation";

const MyProfile = (props: any) => {
  const {t} = useTranslation('common')

  const user = useSelector((state:any) => state.user.user);
const [userData, setUserData] = useState({ name: '', email: '', phoneNumber: '' });


const handleUpdateUser = () => {
  updateUser(userData).then(res=>{
    toast.success('User info update successfully')
  }).catch(err=>{
    toast.error(prepareErrorResponse(err))
  })
};
useEffect(() => {
  if (user) {
    let userData = user.info.general
    setUserData({
      name: userData.name || '',
      email: userData.email || '',
      phoneNumber: userData.phoneNumber || ''
    });
  }
}, [user]);
  return (
    <div className="mx-2 p-3">
      <div className="col-md-12">
        <div className="d-flex v-center my-3">
          <Image alt="user image" className="lg-image" src={userImage} />
          <h1 className="mx-3">{t('My Profile')}</h1>
        </div>
      </div>
      <br />
      <div className="row">
        <div className="col-md-6">
          <div className="label-text">{t('Personal Information')}</div>
          <div className="form">
            <div className="input-label-text">{t('Name')}</div>
            <input className="text-box" placeholder={t('Type your name')}    value={userData.name}
  onChange={e => setUserData({ ...userData, name: e.target.value })}
 />
            <br />
            <br />
            <div className="input-label-text">{t('Email ID')}</div>
            <input className="text-box" placeholder={t("Email ID")}   value={userData.email}
  onChange={e => setUserData({ ...userData, email: e.target.value })} />
            <br />
            <br />
            <div className="input-label-text">{t('Phone Number')}</div>
            <input className="text-box" placeholder={t("Phone Number")}   value={userData.phoneNumber}
  onChange={e => setUserData({ ...userData, phoneNumber: e.target.value })} />
            <br />
            <br />
            <div className="d-flex justify-content-end">
                <button className="btn-1 blue" onClick={handleUpdateUser}>{t('UPDATE INFO')}</button>
            </div>
          </div>
        </div>
        {/* <div className="col-md-6">
        
        <div className="label-text">{t('PACKING INFORMATION')}</div>
            <div>{t('Packing')}</div>
            <div className="blue-box">
                <input type="radio" name="one" />
                <span>{t('Eco-friendly recyclable paper bag')}</span>
            </div>
            <div className="blue-box no-top">
                <input type="radio" name="one" />
                <span>{t('Packed without bags (if possible)')}</span>
            </div>
            <div className="blue-box no-top">
                <input type="radio" name="one" />
                <span>{t('Gift wrapped (if applicable)')}</span>
            </div>
            <br />
            <div>{t('Go Green')}</div>
            <div className="blue-box no-bot">
                <input type="radio" name="two" />
                <span>{t('Email invoice copy')}</span>
            </div>
            <div className="blue-box">
                <input type="radio" name="two" />
                <span>{t('Print invoice copy')}
</span>
            </div>

            <br />
            <div>{t('Alternative Products')}</div>
            <div className="blue-box no-bot">
                <input type="radio" name="three" />
                <span>{t('Please send alternative')}
</span>
            </div>
            <div className="blue-box">
                <input type="radio" name="three" />
                <span>{t('Do not send alternative')}

</span>
            </div>
            <br />
  
        </div> */}
      </div>
    </div>
  );
};

export default Accountcomponent({
  Component: MyProfile,
  title: "My Profile",
});
