import Accountcomponent from "@/components/accountComponent";
import React from "react";
import Image from "next/image";
import regularsImage from "../../public/images/regulars.png";

import useTranslation from "next-translate/useTranslation";

const MyRegulars = (props: any) => {
  const {t} = useTranslation('common')

  return (
    <div className="mx-2 p-3 row">
      <div className="col-md-12">
        <div className="d-flex v-center my-3">
          <Image alt="regulars" className="lg-image" src={regularsImage} />
          <h1 className="mx-3">{t('My Regulars')}</h1>
        </div>
        <div className="w-100">

        <table className="table">
          <thead>
            <tr>
              <td className="text-muted text-center">{t('ITEM NAME')}</td>
              <td className="text-muted text-center">{t('ITEM QTY')}</td>
              <td className="text-muted text-center">{t('NEXT DELIVERY DATE')}</td>
              <td className="text-muted text-center">{t('DELIVERY TIME SLOT')}</td>
              <td className="text-muted text-center">{t('PAYMENT STATUS')}</td>
              <td className="text-muted text-center">{t('MORE')}</td>
            </tr>
          </thead>
        </table>
        </div>
      </div>
    </div>
  );
};

export default Accountcomponent({
  Component: MyRegulars,
  title: "My Regulars",
});
