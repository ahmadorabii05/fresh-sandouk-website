import Accountcomponent from "@/components/accountComponent";
import ProductItem from "@/components/productItem";
import React from "react";
import { useSelector } from "react-redux";
import Image from "next/image";
import rewardPointsImage from "../../public/images/rewardpoints.png";
import cardsIcon from "../../public/icons/cards2.svg";
import Link from "next/link";
import { useRouter } from "next/router";

import useTranslation from "next-translate/useTranslation";

const MyRewardPoints = (props: any) => {
  const {t} = useTranslation('common')

  const router = useRouter();
  return (
    <div className="mx-2 p-3 row">
      <div className="col-md-4">
        <div className="d-flex v-center my-3">
          <Image alt="rewards" className="lg-image" src={rewardPointsImage} />
          <h1 className="mx-3">{t('Reward Points')}</h1>
        </div>
        <h2 className="account-sub-heading">
          {t('NO LOYALTY CARDS! NO MEMBERSHIP NUMBER! NO FUSS!')}
        </h2>
        <p>
          {t('Collect Points Automatically when you Shop Online at Kibsons and earn vouchers.')}
        </p>

        <h2 className="account-sub-heading">{t('HOW DO YOU COLLECT POINTS?')}</h2>
        <p>
           {t('Easy! Earn 10 points for every 100 dirhams you spend on fruits, vegetables and meat items at Kibsons. When you have 500 points Redeem a AED 50 voucher.')}
        </p>

        <h2 className="account-sub-heading">
          {t('HAVE YOU HEARD ABOUT OUR REFERRAL PROGRAM?')}
        </h2>
        <p>
          {t('It’s twice as good! You can share your Referral code and get friends to join Kibsons. For every purchase your friend makes at Kibsons, you collect 20 points! You can have unlimited friends with unlimited points! Forever!')}
        </p>

        <h2 className="account-sub-heading">{t('ABOUT REFERRAL REWARD POINTS')}</h2>
        <p>
          {t('Earn referral points when your friends shop online - every time they shop!')}
        </p>
        <p>
          {t('This is the time to share your love with the person who introduced you to Kibsons so that they can earn credits when you shop!')}
        </p>

        {/* <div className="link">
          <Link href={"/account/refertofriends"}>{t('Find out more')}</Link>
        </div> */}
      </div>
      <div className="col-md-8">
        <div className="bg-g">
          <div className="row">
            <div className="col-md-4">
              <h1>{t('My Point Balance')}</h1>
            </div>
          </div>

          <div className="wallet-box">
            <div className="wallet-box-header">
              <div className="box-h-left w-100">{t('POINTS')}</div>
              <div className="box-h-right w-100 bg-green text-white">{t('VALUE (AED)')}</div>
            </div>
            <div>
              <div className="d-flex">
                <div className="wallet-box-body border-right-1-g w-100">
                  <div>0</div>
                </div>
                <div className="wallet-box-body w-100">0 {t('(AED)')}</div>
              </div>
            </div>
          </div>
        </div>
        <br />
        <div className="mini-statement">
          <h1>{t('Mini statement')}</h1>
          <table className="table">
            <thead>
              <tr>
                <td className="text-muted text-center">{t('MONTH')}</td>
                <td className="text-muted text-center">{t('SAVINGS')}</td>
                <td className="text-muted text-center">{t('TOTAL')}</td>
              </tr>
            </thead>
          </table>
        </div>
      </div>
    </div>
  );
};

export default Accountcomponent({
  Component: MyRewardPoints,
  title: "My Reward Points",
});
