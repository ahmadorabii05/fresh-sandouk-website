import Accountcomponent from "@/components/accountComponent";
import ProductItem from "@/components/productItem";
import React, { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import Image from "next/image";
import shoppingListImage from "../../public/icons/My Shopping List.png";
import heartImage from "../../public/icons/My Favorites.png";
import addToCartImage from "../../public/images/add-top-cart-blue.png";
import trashImage from "../../public/images/trash-blue.png";
import editImage from "../../public/images/edit-blue.png";
import openList from "../../public/images/open-list-blue.png";


import Select from 'react-select'

import {
  createNewList,
  deleteList,
  getListsPerUser,
  toggleProductsInList,
  updateList,
} from "@/network/favorites";
import { toast } from "react-toastify";
import { removeFromCart, setCartItems } from "@/store/reducers/cartReducer";
import AppLoader from "@/components/loader";
import { getAllProducts } from "@/network/categories";
import prepareErrorResponse from "@/errorHandlerLayer/errorHandler";
import { useDispatch } from "react-redux";
import { useRouter } from "next/router";
import { toggleShoppingList } from "@/store/reducers/favoritesReducer";
import useTranslation from "next-translate/useTranslation";

const MyShoppingList = () => {
  const {t} = useTranslation('common')

  const [lists, setLists]: any = useState([]);
  const [products, setProducts] = useState([]);
  const [selectedList, setSelectedList]: any = useState({ name: "" });
  const [newListName, setNewListName] = useState("");
  const [editedListName, setEditedListName] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const [isCreateField, setIsCreateField] = useState(false);
  const [isUpdateList , setIsUpdateList] = useState(false);

  const dispatch: any = useDispatch();
  const router = useRouter()
  useEffect(() => {
    // Fetch lists and products data based on selectedList
    // Update the state accordingly
    setIsLoading(true);
    getListsPerUser().then((res) => {
      const lists = res.data.data.docs
      setLists(lists);
      setIsLoading(false);
      if (lists && lists.length) {
        const option = lists[0]
        setSelectedList(option);
        setEditedListName(option.name);
      }
    }).catch(err=>{
      setIsLoading(false)
    });
  }, []);

  const handleCreateList = () => {
    // Logic to create a new list using the newListName value
    // Update the state accordingly
    setIsLoading(true);

    createNewList({ name: newListName })
      .then((res) => {
        let newLists = lists;
        newLists.push(res.data.data);
        setLists(newLists);
        setNewListName('')
        setIsCreateField(false)
        setIsLoading(false);

        toast.success("List created successfully");
      })
      .catch((err) => {
        toast.error(prepareErrorResponse(err));

        setIsLoading(false);
      });
  };
  const updateSelectedList = (option: any) => {
    setSelectedList(option);
    setEditedListName(option.name);
  };

  interface CustomStyles {
    [key: string]: any;
    control: (provided: any) => any;
    option: (provided: any, state: any) => any;
    dropdownIndicator: any
    indicatorContainer: (provided: any) => any;
  }

  const customStyles: CustomStyles = {
    control: provided => ({
      ...provided,
      border: 'none',
      boxShadow: 'none',
      height: '40px',
      position: 'relative',
    }),
    option: (provided, state) => ({
      ...provided,
      backgroundColor: state.isFocused ? '#a30b33' : 'white',
      color: state.isFocused ? 'white' : 'black',
    }),
    dropdownIndicator: (provided:any, state:any) => ({
      ...provided,
      backgroundColor: state.isFocused ? '#a30b33' : 'brown',
      color: 'white',
      padding: '8px',
      borderRadius: '0',
      alignSelf: 'center',
      position: 'absolute',
      right: '0',
      width:'45px',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',      top:'0px',
      height:'44px'
    }),
    indicatorContainer: provided => ({
      ...provided,
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      height: '100%',
      margin: '0',
      paddingRight: '4px',
    }),
  };
  
  const handleEditListName = () => {
    // Logic to edit the selected list name using the editedListName value
    // Update the state accordingly
    setIsLoading(true);
    updateList({ name: editedListName, listId: selectedList.id })
      .then((res) => {
        let updatedLists = lists;
        updatedLists = updatedLists.map((list: any) => {
          if (list.id == selectedList.id) {
            list.name = editedListName;
          }
          return list;
        });
        toast.success("list name updated");
        setIsLoading(false);
      })
      .catch((err) => {
        setIsLoading(false);

        toast.error(prepareErrorResponse(err));
      });
  };

  const handleDeleteList = () => {
    // Logic to delete the selected list
    // Update the state accordingly
    setIsLoading(true)
    deleteList({ listId: selectedList.id }).then((res) => {
      let updatedLists = lists;
      updatedLists = updatedLists.filter(
        (list: any) => list.id != selectedList.id
      );
      setLists(updatedLists)
      updateSelectedList(updatedLists[0])
      setIsLoading(false)
      toast.success("List deleted");
    });
  };


  const removeFromList = (product:any) =>{
       // If product already exists in the list, remove it
       toggleProductsInList({ productId:product.id, listId: selectedList.id })
       .then((res) => {
         const updatedProductIds = selectedList.productsIds.filter(
           (pr: any) => pr.id !== product.id
         );
         selectedList.productIds = updatedProductIds;
         // Update the lists state with the modified list
         const updatedLists = lists.map((l: any) =>
           l.id === selectedList.id ? selectedList : l
         );
         setLists(updatedLists);
         toast.success("Item removed to list successfully.");
       })
       .catch((err) => {
         toast.error(prepareErrorResponse(err));
       });
  }

  
  const handleAddItemsToCart = () => {
    const products = selectedList.productIds
      if(products && products.length){
        dispatch(setCartItems(products))
        toast.success('Products added to cart')
      }else {
        toast.error('Your shopping cart is empty')
      }
      
  };

    const formattedLists = lists.map((option:any) => ({
      label: option.name,
      value: option,
    }));


  return (
    <div className="mx-2 p-3">
      <AppLoader isLoading={isLoading}></AppLoader>
      <div className="d-flex align-items-center v-center justify-content-between">

      <div className="d-flex align-items-center mb-3">
        <Image
          alt="shopping list"
          className="lg-image"
          src={shoppingListImage}
          />
        <h1 className="mx-3">{t("My Shopping List")}</h1>
      </div>
      <div className=" d-flex sm-link" onClick={()=>router.push('/account/myfavorites')}>
      <div className="mx-2 d-flex v-center">{t('View Shopping Lists')}</div>
      <Image alt="heart" className="lg-image" src={heartImage} />
      </div>
          </div>
    <div className="row">
    <div className="col-md-7">

      <div className="mb-3">
        <div className="d-flex justify-content-between">
          <input
          className="input-shopping-list"
          disabled
            placeholder="Create new list"
     
          />
          <button  className="primary-brown s-md text-white" onClick={()=>setIsCreateField(!isCreateField)}>
            +
          </button>
        </div>
      </div>
      {isCreateField && <div className="mb-3">
        <div className="justify-content-between">
          <input
          className="input-shopping-list"
            placeholder="Please enter here"
            value={newListName}
            onChange={(e) => setNewListName(e.target.value)}
          />
          <div className="text-right d-flex justify-content-end">

          <button  className="primary-brown s-md text-white my-2" onClick={handleCreateList}>
            Create
          </button>
          </div>
        </div>
      </div> }

      <div className="">
      <Select     styles={customStyles}
 className="input-shopping-list p-0" defaultValue={formattedLists[0]}  options={formattedLists}  onChange={(e:any)=>updateSelectedList(e.value)}
 />
      </div>

      {isUpdateList && selectedList.name && (
        <div className="mb-3 mt-3">
          <div>
            <input
              placeholder="Edit List Name"
              className="input-shopping-list"
              value={editedListName}
              onChange={(e) => setEditedListName(e.target.value)}
            />
                      <div className="text-right d-flex justify-content-end">

            <button  className="primary-brown s-md text-white my-2" onClick={handleEditListName}>
              Update
            </button>
            </div>
          </div>

        </div>
      )}
            {selectedList.name && (
        <div className="row w-100 m-auto">
             <div className={"col-4 list-option " + ((isUpdateList && selectedList.name) ? '' :  'border-collapse')}  onClick={handleAddItemsToCart}>
              <Image alt="addlist" src={addToCartImage} />
            list to cart
          </div>
          <div  className={"col-4 list-option " + ((isUpdateList && selectedList.name) ? '' :  'border-collapse')}  onClick={()=>setIsUpdateList(!isUpdateList)}>
          <Image alt="addlist" src={editImage} />

            edit list
          </div>
       
          <div  className={"col-4 list-option " + ((isUpdateList && selectedList.name) ? '' :  'border-collapse')} onClick={handleDeleteList}>
          <Image alt="addlist" src={trashImage} />
            delete list 
          </div>
        </div>
      )}


    </div>
    </div>

      {/* List of products for the selected list */}
      <div className="d-grid-50 mt-4">
        {selectedList && selectedList.productIds && selectedList.productIds.map((product:any, index:number) => (
    
                <div className="border-y mx-3" key={index}>
                <ProductItem product={product} flex={true} key={index} itemType="list" list={selectedList} removeFromList={removeFromList}/>
              </div>
        ))}
      </div>
    </div>
  );
};

export default Accountcomponent({
  Component: MyShoppingList,
  title: ("My Shopping List"),
});
