import Accountcomponent from "@/components/accountComponent";
import React from "react";
import Image from "next/image";
import statementImage from "../../public/images/statement.png";

import useTranslation from "next-translate/useTranslation";

const MyStatement = (props: any) => {
  const {t} = useTranslation('common')

  return (
    <div className="mx-2 p-3 row">
      <div className="col-md-12">
        <div className="d-flex v-center my-3">
          <Image alt="my-statement" className="lg-image" src={statementImage} />
          <h1 className="mx-3">{t('My Statement')}</h1>
        </div>
        <p>{t('How much you have spent in the last 6 months:')}</p>
        <div className="w-100">
          <table className="table">
            <thead>
              <tr>
                <td className="text-muted text-center">{t('MONTH')}</td>
                <td className="text-muted text-center">{t('SAVINGS')}</td>
                <td className="text-muted text-center">{t('TOTAL')}</td>
              </tr>
            </thead>
          </table>
        </div>
        <div>
          <button className="deposit-balance-btn">
            {t('Your deposit balance')}: AED 0 / Bottles 0
          </button>
        </div>
      </div>
    </div>
  );
};

export default Accountcomponent({
  Component: MyStatement,
  title: ('My Statement'),
});
