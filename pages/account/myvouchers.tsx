import Accountcomponent from "@/components/accountComponent";
import React from "react";
import Image from "next/image";
import vouchersImage from "../../public/images/vouchers.png";

import useTranslation from "next-translate/useTranslation";

const MyVouchers = (props: any) => {
  const {t} = useTranslation('common')

  return (
    <div className="mx-2 p-3 row">
      <div className="col-md-12">
        <div className="d-flex v-center my-3">
          <Image alt="voucher" className="lg-image" src={vouchersImage} />
          <h1 className="mx-3">{t('My Vouchers')}</h1>
        </div>
        <div className="w-100">
          <table className="table">
            <thead>
              <tr>
                <td className="text-muted text-center">{t('VOUCHER TYPE')}</td>
                <td className="text-muted text-center">{t('VOUCHER NUMBER')}</td>
                <td className="text-muted text-center">{t('BALANCE AMOUNT')}</td>
              </tr>
            </thead>
          </table>
        </div>
      </div>
    </div>
  );
};

export default Accountcomponent({
  Component: MyVouchers,
  title: ('My Vouchers'),
});
