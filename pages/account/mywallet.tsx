import Accountcomponent from "@/components/accountComponent";
import ProductItem from "@/components/productItem";
import React from "react";
import { useSelector } from "react-redux";
import Image from "next/image";
import walletImage from "../../public/images/wallet.png";
import cardsIcon from "../../public/icons/cards2.svg";

import useTranslation from "next-translate/useTranslation";

const MyWallet = (props: any) => {
  const {t} = useTranslation('common')

  return (
    <div className="mx-2 p-3 row">
      <div className="col-md-6">
        <div className="d-flex v-center my-3">
          <Image
            className="lg-image"
            src={walletImage}
            alt={t("wallet image")}
          />
          <h1 className="mx-3">{t("My Wallet")}</h1>
        </div>
        <h2 className="account-sub-heading">{t("BULK BUY BONUS")}</h2>
        <p>
          {t(
            "Earn FREE BONUS CREDIT when you buy our below packages to top up your wallet:"
          )}
        </p>
        <div>
          <table className="wallet-table">
            <thead>
              <tr className="table-h">
                <td>{t("ADD TO WALLET")}</td>
                <td className="BOLD">{t("BONUS CREDIT")}</td>
                <td>{t("TOTAL IN WALLET")}</td>
              </tr>
            </thead>
            <tbody>
              <tr className="table-b">
                <td>
                  500 <span>AED</span>
                </td>
                <td className="acc-td">
                  + 25 <span>AED</span>
                </td>
                <td>
                  1798 <span>AED</span>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
        <br />
        <h2 className="account-sub-heading">
          {t("ABOUT KIBSONS WALLET")}
        </h2>
        <p>
          {t(
            "Using the money in your KIBSONS wallet, you can pay for orders without using cash/card. Select KIBSONS Wallet on time of checkout and money will be automatically deducted from your wallet. You dont need to give cash to driver or pay through online after invoice generation. The money in the wallet will not expires so no worry, whenever you are purchasing, it will be available for next purchase."
          )}
        </p>
        <p>
          {t(
            "To add money to your Kibsons Wallet, just enter amount in text box in the previous page and click «Add Money» button. it will redirect you to the payment gateway. Select your debit/credit card type and enter details. It will automatically transfer the amount from your card to your KIBSONS Wallet real time. PS: Please dont close your browser/click back button until you are redirected back from payment page to KIBSONS payment response page. The system needs some time to generate the receipt voucher."
          )}
        </p>
        <p>
          {t(
            "any queries, please call our support desk at 800 KIBSONS"
          )}
        </p>
      </div>
      <div className="col-md-6">
        <div className="bg-g">
          <div className="row">
            <div className="col-md-4">
              <h1>{t("My Wallet")}</h1>
              <div className="wallet-box">
                <div className="wallet-box-header">
                  <div className="box-h-left">{t("My Balance")}</div>
                </div>
                <div className="wallet-box-body">
                  <input
                    type="number"
                    className="wallet-box-input"
                    placeholder={t("AED 0.00")}
                  />
                </div>
              </div>
            </div>
            <div className="col-md-8">
              <h1>{t("Add money to wallet")}</h1>
              <div className="wallet-box">
                <div className="wallet-box-header">
                  <div className="box-h-left">{t("Amount")}</div>
                  <div className="box-h-right">
                    <input
                      type="number"
                      className="wallet-box-input"
                      placeholder={t("AED 0.00")}
                    />
                  </div>
                </div>
                <div className="wallet-box-body">
                  <div>{t("Pay with:")}</div>
                  <Image
                    className="card-images2"
                    src={cardsIcon}
                    alt={t("card image")}
                  />
                </div>
              </div>
            </div>
          </div>
          <br />
          <div className="wallet-box">
            <div className="wallet-box-header">
              <div className="box-h-left">
                {t("+ MONEY TO FRIEND'S WALLET :")}
              </div>
              <div className="box-h-right">
                <input
                  type="number"
                  className="wallet-box-input"
                  placeholder={t("AED 0.00")}
                />
              </div>
            </div>
            <div>
              <div className="d-flex">
                <div className="wallet-box-body border-right-1-g">
                  <div>
                    {" "}
                    <input
                      type="number"
                      className="wallet-box-input"
                      placeholder={t("AED 0.00")}
                    />
                  </div>
                </div>
                <div className="wallet-box-body w-100">
                  <div>{t("Pay with:")}</div>
                  <Image
                    className="card-images2"
                    src={cardsIcon}
                    alt={t("card image")}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
        <br />
        <div className="total-voucher">
          {t("Total Voucher Value is : AED 0")}
        </div>
        <br />
        <div className="mini-statement">
          <h1>{t("Mini statement")}</h1>
          <table className="table">
            <thead>
              <tr>
                <td className="text-muted text-center">{t("MONTH")}</td>
                <td className="text-muted text-center">
                  {t("SAVINGS")}
                </td>
                <td className="text-muted text-center">{t("TOTAL")}</td>
              </tr>
            </thead>
          </table>
        </div>
      </div>
    </div>
  );
};

export default Accountcomponent({
  Component: MyWallet,
  title: ("My Wallet"),
});
