import Accountcomponent from "@/components/accountComponent";
import React from "react";
import Image from "next/image";
import referFriendsImage from "../../public/images/referFriends.png";
import foodTableImage from "../../public/images/food-table.jpg";

import useTranslation from "next-translate/useTranslation";

const ReferToFriends = (props: any) => {
  const {t} = useTranslation('common')

  return (
    <div className="mx-2 p-3 row">
      <div className="col-md-4">
        <div className="d-flex v-center my-3">
          <Image alt="refer" className="lg-image" src={referFriendsImage} />
          <h1 className="mx-3">{t('Refer To Friends')}</h1>
        </div>
        <div className="bordered-box">
          {t('More friends = More rewards')}
        </div>
        <p>{t('Earn reward points when your friends shop online - every time they shop!')}</p>
        <h2 className="account-sub-heading">{t('TELL YOUR FRIENDS NOW:')}</h2>
        <div>
          <button className="btn-1 red">
            {t('EMAIL')}
          </button>
          <button className="btn-1 blue">
            {t('FACEBOOK')}
          </button>
          <button className="btn-1 green">
            {t('COPY LINK')}
          </button>
        </div>
        <br />
        <h2 className="account-sub-heading">{t('DID YOUR FRIENDS FORGET TO TAG YOU AS THEIR REFEREE?')}</h2>
        <p>{t('Not to worry… Just give them the below customer code and ask them to update your customer code in the "My Referee Information" in their account.')}</p>
        <br />
        <button className="btn-1 red">
          {t('Your customer code')}: 221343
        </button>
      </div>
      <div className="col-md-8">
        <div className="row bordered-green">
          <div className="col-md-5">
            <h1 className="mx-3">{t('My Refere')}</h1>
            <div className="wallet-box-body bordered-green  h-38">
              <input
                type="number"
                className="wallet-box-input"
                placeholder={t('Enter referee code')}
              />
            </div>
            <button className="btn-1 green m-0">{t('UPDATE MY REFEREE')}</button>
            <br />
            <p className="my-1">{t('This is the time to share your love with the person who introduced you to Kibsons so that they can earn credits when you shop.')}</p>
          </div>
          <div className="col-md-7">
            <h1 className="mx-3 text-right">{t('My Friends')}</h1>
            <div>
              <table className="table">
                <thead>
                  <tr>
                    <td className="text-muted text-center">{t('NAME')}</td>
                    <td className="text-muted text-center">{t('#ORDERS')}</td>
                  </tr>
                </thead>
              </table>
            </div>
          </div>
        </div>
        <div className="refer-image mt-2">
          <Image alt="food-table" src={foodTableImage} />
        </div>
      </div>
    </div>
  );
};

export default Accountcomponent({
  Component: ReferToFriends,
  title: 'Refer to friends',
});
