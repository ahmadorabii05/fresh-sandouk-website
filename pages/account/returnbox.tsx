import Accountcomponent from "@/components/accountComponent";
import React, { useState } from "react";
import Image from "next/image";
import returnImage from "../../public/images/return.png";
import gArrow from "../../public/g-arrow.svg";

import useTranslation from "next-translate/useTranslation";
const ReturnBox = (props: any) => {
  const {t} = useTranslation('common')

  const [isTimeSelectOpen, setIsTimeSelectOpen] = useState(false);
  const upcomingDays = [
    {
      day: 19,
      name: "Tom",
    },
    {
      day: 20,
      name: "Tue",
    },
    {
      day: 21,
      name: "Wed",
    },
    {
      day: 22,
      name: "Thu",
    },
  ];

  return (
    <div className="mx-2 p-3 row">
      <div className="col-md-7">
        <div className="d-flex v-center my-3">
          <Image className="lg-image" src={returnImage} alt="return image" />
          <h1 className="mx-3">{t('Return & Recycle Service')}</h1>
        </div>
        <div className="checkout-information">
          <div className="information-box">
            <div className="label-text">{t('DELIVERY ADDRESS')}</div>
            <select className="text-box">
              <option>{t('Dubai')}</option>
              <option>{t('Abu Dhabi')}</option>
            </select>
          </div>

          <div className="information-box">
            <div className="label-text">{t('SCHEDULE COLLECTION')}</div>
            <div className="d-flex">
              <input
                className="text-box"
                placeholder={t('Day')}
                value={"Thursday"}
                disabled
              />
              <input
                className="text-box"
                placeholder={t('Time')}
                value={"4-5 pm"}
                disabled
              />
              <div
                className="g-arrow"
                onClick={() => setIsTimeSelectOpen(!isTimeSelectOpen)}
              >
                <Image src={gArrow} alt="garrow" />
              </div>
            </div>
            {isTimeSelectOpen ? (
              <div className="time-select-modal">
                <div className="label-text">{t('Choose a date')}</div>
                <div className="days d-flex">
                  {upcomingDays.map((day: any) => {
                    return (
                      <div className="wrapper" key={day.name}>
                        <div className="time-box">{day.day}</div>
                        <div className="">{day.name}</div>
                      </div>
                    );
                  })}
                </div>
                <div className="label-text">{t('Choose a time')}</div>
                <div className="times d-flex">
                  <div className="time-box long">8 AM - 2 PM</div>
                  <div className="time-box long">3 PM - 6 PM</div>
                  <div className="time-box long">6 PM - 9 PM</div>
                </div>
                <div className="my-3">
                  <button className="btn-1 green">{t('SELECT')}</button>
                </div>
              </div>
            ) : (
              ""
            )}
          </div>
          <br />
          <div className="information-box">
            <select className="text-box">
              <option>{t('EMPTY WATER')}</option>
              <option>{t('EMPTY BOX')}</option>
            </select>
          </div>
          <br />
          <div className="information-box">
            <textarea
              className="text-box"
              placeholder={t('Special instructions, if any..')}
              ></textarea>
              </div>
              <button className="btn-1 green">{t('SUBMIT')}</button>
              </div>
              </div>
              </div>
              );
              };
              
              export default Accountcomponent({
              Component: ReturnBox,
              title: "My Return Box",
              });
