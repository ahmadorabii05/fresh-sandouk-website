import FacebookIcon2 from "@/components/icons/facebookIcon2";
import GoogleIcon from "@/components/icons/googleIcon";
import React, { useState } from "react";
import Image from 'next/image'
import Link from 'next/link'
import IsLoggedIn from "@/components/isLoggedIn";
import { useDispatch } from "react-redux";
import { saveUser } from "@/store/reducers/userReducer";
import { login } from "@/network/auth";
import { useRouter } from "next/router";
import prepareErrorResponse from "@/errorHandlerLayer/errorHandler";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css'

import englishIcon from "../../public/images/english kibsons logo.png";
import useTranslation from "next-translate/useTranslation";

const Login = () => {
  const {t} = useTranslation('common')

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [errorMessage, setErrorMessage] = useState("");
  const router = useRouter()
  const dispatch = useDispatch()
  const requestModel = {
    email,
    password
  }
  const handleSubmit = (e:any) => {

    e.preventDefault();
    // perform form validation
    if (!email || !password) {
      setErrorMessage(t("Please enter your email and password."));
      return;
    }

    // make API call to login with email and password
    // TODO: replace with actual API call
    if (email  && password ) {
      // handle success response
      login(requestModel).then(res=>{
        dispatch(saveUser(res.data.data))
        toast.success(('Welcome back !'))
        window.location.href = window.location.origin
      }).catch(err=>{
        toast.error(prepareErrorResponse(err))

      })
    } else {
      // handle failure response
      toast.error(("Invalid email or password."))
    }
  };

  return (
    <IsLoggedIn>
      <ToastContainer />

      <div className="my-3">
        <div className="row">
          <div className="col-md-6 login-page">
            <div className="heading only-desktop">{t('SIGN IN')}</div>
            <div className="only-mobile w-100 text-center">
              <Image alt="white" src={englishIcon} style={{width:'100%','height':'100px',objectFit:'contain'}} />
              <h2 className="text-white only-mobile text-center">{t('SIGN IN')}</h2>
            </div>
            <div className="devider-small only-desktop"></div>
            <div>
              <p className="text-welcome my-2 only-desktop">{t('Hello, welcome to your account')}</p>
              {/* <div className="d-flex justify-content-between">
                <button className="primary m-custom-login-bts">
                  <FacebookIcon2 />
                  <span className="mx-2">{t('Login with Facebook')}</span>
                </button>
                <button className="primary m-custom-login-bts">
                  <GoogleIcon />
                  <span className="mx-2">{t('Login with Google')}</span>
                </button>
              </div> */}
            </div>
            <br />

            <form className="form" onSubmit={handleSubmit}>
              <div className="input-label-text only-desktop">{t('EMAIL*')}</div>
              <input
                className={`text-box ${errorMessage && 'is-invalid'}`}
                type="email"
                placeholder={t('Email')}
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
              <br />
              <br />

              <div className="input-label-text only-desktop">{t('PASSWORD*')}</div>
              <input
                className={`text-box ${errorMessage && 'is-invalid'}`}
                type="password"
                placeholder={t('Password')}
                value={password}
                onChange={(e) => setPassword(e.target.value)}
              />
              <br />
              <br />
              {errorMessage && <div className="invalid-feedback text-danger">{errorMessage}</div>}

                            <div className="d-flex justify-content-between">
                <button className="danger only-mobile" type="button" onClick={()=>router.push('/')}>{t('SKIP')}</button>
                <div className="only-desktop">
                <button className="primary mxw-fit" type="submit">{t('LOGIN')}</button>
                <br />
                <div className="text-right only-desktop">
                  <Link href={'/auth/request-password-reset'}>
                  {t('Forgot Password ?')}
                  </Link>
                   </div>

                </div>
                <div className="only-mobile w-50">

                <button className="secondary w-100 " type="submit">{t('LOGIN')}</button>
                </div>

              </div>
    
              {/* <div className="text-right m-2 text-white only-mobile">{t('Forgot your password ?')}</div> */}
              <br />
              <br />
              <button className="transparent only-mobile">
                <Link className="text-white" href={'/auth/register'}>{t('CREATE AN ACCOUNT')}</Link>
              </button>
    
            </form>
          </div>
          <div className="col-md-6">
            <div className="heading">{t('CREATE NEW ACCOUNT')}</div>
            <div className="devider-small"></div>
            <div className="label-check">{t('Speed your way through checkout')}</div>
            <div className="label-check">{t('Track your orders easily')}</div>
            <div className="label-check">{t('Keep a record of all purchases')}</div>
            <div className="label-check">{t('Create convenient shopping lists')}</div>
            <div className="label-check">{t('Refer friends and earn credits')}</div>
            <button className="primary mxw-fit"><Link className="text-white" href={'/auth/register'}>{t('Signup')}</Link></button>
          </div>
        </div>
      </div>
    </IsLoggedIn>
);

};
export default Login;    