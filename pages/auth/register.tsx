import FacebookIcon2 from "@/components/icons/facebookIcon2";
import GoogleIcon from "@/components/icons/googleIcon";
import { signup } from "@/network/auth";
import Cookies from "js-cookie";
import React, { useState } from "react";
import { useRouter } from "next/router";
import AppLoader from "@/components/loader";
import { useDispatch } from "react-redux";
import { saveUser } from "@/store/reducers/userReducer";
import IsLoggedIn from "@/components/isLoggedIn";

import { toast } from "react-toastify";
import {
  handleVerifyCodeFirebase,
  requestVerificationCodeFirebase,
} from "@/network/firebase";
import { TRUE } from "sass";
import useTranslation from "next-translate/useTranslation";

const Register = () => {
  const {t} = useTranslation('common')

  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [phoneNumber, setPhoneNumber] = useState("");
  const [otp, setOtp] = useState("");
  const [password, setPassword] = useState("");
  const [retypePassword, setRetypePassword] = useState("");
  
  const [nameError, setNameError] = useState("");
  const [emailError, setEmailError] = useState("");
  const [phoneNumberError, setPhoneNumberError] = useState("");
  const [otpError, setOtpError] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const [formError, setFormError] = useState("");
  const dispatch = useDispatch();
  const [passwordError, setPasswordError] = useState("");
  const [retypePasswordError, setRetypePasswordError] = useState("");
  
  const [verificationCodeError, setVerificationCodeError] = useState(false);
  const [confirmationResult, setConfirmationResult] = useState<any>(null);
  const [isVerified, setIsVerified] = useState(false);

  const router = useRouter();
  const handleSubmit = (e: any) => {
    e.preventDefault();
    const userData = {
      name,
      email,
      password,
      phoneNumber,
      otp,
    };

    if (validate()) {
      setIsLoading(true);
      signup(userData)
        .then((res) => {
          dispatch(saveUser(res.data.data));
          toast.success("Authentication successfull");
          router.push("/");
          setIsLoading(false);
        })
        .catch((err) => {
          setFormError(err.response.data.message);
          setIsLoading(false);
        });
    }
  };

  const requestVerificationCode = () => {
    const phoneNumberRegex = /^\+?\d{1,3}[-.\s]?\(?\d{1,3}\)?[-.\s]?\d{1,4}[-.\s]?\d{1,4}[-.\s]?\d{1,9}$/;
    const isPhoneNumberValid = phoneNumberRegex.test(phoneNumber.trim());
    if(isPhoneNumberValid){
setPhoneNumberError('')
      setIsLoading(true);
      requestVerificationCodeFirebase(phoneNumber)
      .then((res) => {
        setIsLoading(false);
        
        setConfirmationResult(res);
        toast.success("Verification code was sent to you");
      })
      .catch((err) => {
        setIsLoading(false);
        console.log(err.response)
        console.log(err.message)
        toast.error(err.message);
        if(err.message == 'reCAPTCHA has already been rendered in this element'){
          window.location.reload()
        }
      });
    }else {
      setPhoneNumberError('Phone number not valid')

    }
  };

  const handleVerifyCode = () => {
    if (otp && otp.length > 0 && phoneNumber && phoneNumber.length > 5 && !isVerified) {
      setIsLoading(true);
      handleVerifyCodeFirebase(otp, confirmationResult)
        .then((res) => {
          if (res) {
            setIsLoading(false);
            toast.success("Phone number verified !");
            setVerificationCodeError(false);

            setIsVerified(true);
          } else {
            setIsLoading(false);
            toast.error("Verification error");
            setVerificationCodeError(true);
          }
        })
        .catch((err) => {
          setIsLoading(false);
          toast.error("Verification error");
          setVerificationCodeError(true);
        });
    }
  };

  const validate = () => {
    let isValid = true;

    if (!name) {
      setNameError(t("Name is required"));
      isValid = false;
    } else {
      setNameError("");
    }

    if (!email) {
      setEmailError(t("Email is required"));
      isValid = false;
    } else if (!/\S+@\S+\.\S+/.test(email)) {
      setEmailError(t("Email is invalid"));
      isValid = false;
    } else {
      setEmailError("");
    }

    if (!phoneNumber) {
      setPhoneNumberError(t("Mobile number is required"));
      isValid = false;
    } else {
      setPhoneNumberError("");
    }

    if (!otp) {
      // setOtpError(t("OTP is required"));
      isValid = false;
    } else if (!/^\d{6}$/.test(otp)) {
      // setOtpError(t("OTP is invalid"));
      isValid = false;
    } else {
      setOtpError("");
    }

    if (!password) {
      setPasswordError(t("Password is required"));
      isValid = false;
    } else if (password.length < 6) {
      setPasswordError(
        t("Password must be at least 8 characters long")
      );
      isValid = false;
    } else {
      setPasswordError("");
    }
    if (password !== retypePassword) {
      setRetypePasswordError(t("Password are not match"));
      isValid = false;
    } else if (retypePassword.length < 6) {
      setRetypePasswordError(
        t("Password must be at least 8 characters long")
      );
      isValid = false;
    } else {
      setRetypePasswordError("");
    }
    return isValid;
  };
  return (
<IsLoggedIn>
  <div id="recaptcha-container"></div>
  <div className="my-3 modal-size">
    <h2 className="text-center bold">
      {t("Create your Kibsons account")}
    </h2>
    <p className="text-center text-danger">{t(formError)}</p>
    <form onSubmit={handleSubmit}>
      <div className="input-label-text-small">{t("Name")}</div>
      <input
        className="text-box"
        placeholder={t("Name")}
        value={name}
        onChange={(e) => setName(e.target.value)}
      />
      <div className="text-danger">{t(nameError)}</div>
      <br />
      <div className="input-label-text-small">{t("Email ID")}</div>
      <input
        className="text-box"
        placeholder={t("Email ID")}
        value={email}
        onChange={(e) => setEmail(e.target.value)}
      />
      <div className="text-danger">{t(emailError)}</div>
      <br />
      <div className="input-label-text-small">
        {t("Mobile Number")}
      </div>
      <div className="verification-wrapper">
        <div className="w-100">
          <input
            className="text-box"
            placeholder={t("+971 55 260 3982")}
            value={phoneNumber}
            onChange={(e) => setPhoneNumber(e.target.value)}
          />
        </div>
        <div className="otp-btn-wrapper">
          <button
            className="btn-1 green otp-btn"
            type="button"
            disabled={confirmationResult && confirmationResult.verificationId || isVerified}
            onClick={requestVerificationCode}
          >
            {t("Send OTP")}
          </button>
        </div>
      </div>
      {confirmationResult && confirmationResult.verificationId && !isVerified && (
        <div className="text-success">
          {t("Check your phone for verification code")}
        </div>
      )}
      <div className="text-danger">{t(phoneNumberError)}</div>
      <br />
      <div className="input-label-text-small">
        {t("Verification OTP")}
      </div>
      <input
        className="text-box"
        placeholder={t("Verification OTP")}
        value={otp}
        autoComplete="off"
        onChange={(e) => setOtp(e.target.value)}
        onBlur={handleVerifyCode}
        disabled={isVerified}
      />
      <div className="text-danger">{t(otpError)}</div>
      {isVerified && (
        <div className="text-success">{t("Verified successfully")}</div>
      )}
      {!isVerified && verificationCodeError && (
        <div className="text-error">{t("Verification Error")}</div>
      )}
      <br />
      <div className="input-label-text-small">{t("Password")}</div>
      <input
        className="text-box"
        placeholder={t("Password")}
        type="password"
        autoComplete="new-password"
        value={password}
        onChange={(e) => setPassword(e.target.value)}
      />
      <div className="text-danger">{t(passwordError)}</div>
      <br />
      <div className="input-label-text-small">{t("Retype Password")}</div>
      <input
        className="text-box"
        placeholder={t("Retype Password")}
        type="password"
        autoComplete="new-password"
        value={retypePassword}
        onChange={(e) => setRetypePassword(e.target.value)}
      />
      <div className="text-danger">{t(retypePasswordError)}</div>
      <br />
      <button className="btn-1 green" type="submit" disabled={!isVerified}>
        {t("REGISTER USER")}
      </button>
    </form>
    <AppLoader isLoading={isLoading}></AppLoader>
  </div>
</IsLoggedIn>

  );
};
export default Register;
