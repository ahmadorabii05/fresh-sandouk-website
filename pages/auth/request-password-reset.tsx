import FacebookIcon2 from "@/components/icons/facebookIcon2";
import GoogleIcon from "@/components/icons/googleIcon";
import { sendResetLink, signup } from "@/network/auth";
import Cookies from "js-cookie";
import React, { useState } from "react";
import { useRouter } from "next/router";
import AppLoader from "@/components/loader";
import { useDispatch } from "react-redux";
import { saveUser } from "@/store/reducers/userReducer";
import IsLoggedIn from "@/components/isLoggedIn";

import useTranslation from "next-translate/useTranslation";

const ResetPassword = () => {
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [verificationCode, setverificationCode] = useState("");
  const [otp, setOtp] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassword, setconfirmPassword] = useState("");
  const [confirmPasswordError, setConfirmPasswordError] = useState("");
  const {t} = useTranslation('common')

  const [emailError, setEmailError] = useState("");
  const [verificationCodeError, setverificationCodeError] = useState("");
  const [otpError, setOtpError] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const [formError, setFormError] = useState("");
  const dispatch = useDispatch();
  const [successMessage, setSuccessMessage] = useState("");
  const router = useRouter();
  const handleSubmit = (e: any) => {
    e.preventDefault();
    const userData = {
      email,
    };

    if (validate()) {
      setIsLoading(true);
      sendResetLink(userData)
        .then((res) => {
          setSuccessMessage('We have sent you reset password link, Please check you email')
          setEmail('')
          setIsLoading(false);
        })
        .catch((err) => {
          setFormError(err.response.data.message);
          setIsLoading(false);
        });
    }
  };

  const requestVerificationCode = () => {};

  const validate = () => {
    let isValid = true;
    if (!email) {
      setEmailError(t("Email is required"));
      isValid = false;
    } else if (!/\S+@\S+\.\S+/.test(email)) {
      setEmailError(t("Email is invalid"));
      isValid = false;
    } else {
      setEmailError("");
    }

    return isValid;
  };
  return (
  <div className="my-3 modal-size">
    <h2 className="my-2">{t('Reset password')}</h2>
    <p className="text-center text-danger">{t(formError)}</p>
    {successMessage && <p className="text-center text-success">{successMessage}</p>}
    
    <form onSubmit={handleSubmit}>
      <div className="input-label-text mt-2">{t("Email ID")}</div>
      <input
        className="text-box"
        placeholder={t("Email ID")}
        value={email}
        onChange={(e) => setEmail(e.target.value)}
      />
      <div className="text-danger">{t(emailError)}</div>
      <button className="btn-1 green" type="submit">
        {t("SEND RESET LINK")}
      </button>
    </form>
    <AppLoader isLoading={isLoading}></AppLoader>
  </div>

  );
};
export default ResetPassword;
