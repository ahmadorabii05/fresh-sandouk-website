import Categories from "@/components/categories";
import React, { useEffect, useState } from "react";
import { useRouter } from "next/router";
import ProductItem from "@/components/productItem";
import { useSelector } from "react-redux";
import Image from "next/image";
import ProductsSlider from "@/components/productsSlider";
import {
  getAllCategories,
  getAllProducts,
  getAllTags,
  getAllbrand,
  getAllcountry,
  getAllsubCategory,
  getCategoryById,
  getSubcategoryById,
  getTagById,
  searchForProductsRequest,
} from "@/network/categories";
import SearchIcon from "../../../../../public/icons/search png.png";
import { getAllJSDocTags } from "typescript";
import AppLoader from "@/components/loader";
import { toast } from "react-toastify";
import prepareErrorResponse from "@/errorHandlerLayer/errorHandler";
import Link from "next/link";
import leftArrow from "../../../../../public/leftArrow.svg";
import sortwhite from "../../../../../public/sort-white.svg";
import filterwhite from "../../../../../public/filter-white.svg";
import ModalSlide from "@/components/slideup-modal";
export async function getStaticPaths() {
  return { paths: [], fallback: true };
}

export async function getStaticProps({ params }: any) {
  return { props: { params } };
}

export default function ItemCollection({ params }: any) {
  const [isLoading, setIsLoading] = useState(false);
  const [selectedFilter, setSelectedfilter] = useState("");
  const [products, setProducts] = useState([]);
  const [allProducts, setAllProducts] = useState([]);
  const [activeTag, setActiveTag] = useState({ id: "", name: "" });

  // const [activeCategory, setActiveCategory] = useState({id:'',name:''});
  // const [categories, setCategories] = useState({id:'',name:''});

  // const [activeSubCategory, setActiveSubCategory] = useState({id:'',name:''});
  // const [subCategories, setSubCategories] = useState({id:'',name:''});

  // const [activeBrand, setActiveBrand] = useState({id:'',name:''});
  // const [brands, setBrands] = useState({id:'',name:''});

  // const [activecountry, setActivecountry] = useState({id:'',name:''});
  // const [countries, setCountries] = useState({id:'',name:''});

  const [tags, setTags] = useState([]);

  const router = useRouter();

  const [category, setCategory]: any = useState({});
  const [query, setquery]: any = useState({});

  const [filters, setFilters]: any = useState([]);
  const [allFilters, setAllFilters]: any = useState([]);
  const [alert, setAlert] = useState("");
  const [displayModal, setDisplayModal] = useState(false);
  const [activeSlideContent, setActiveSlideContent] = useState("");

  useEffect(() => {
    Promise.all([
      getAllCategories(),
      getAllsubCategory(),
      getAllcountry()
      // getAllbrand(),
      // Add other function calls here
    ]).then(
      ([
        categoriesRes,
        subCategoriesRes,
        // brandRes /*, ...otherResults */,
        countryRes,
      ]) => {
        const newFilters: any = [];

        newFilters.push({
          key: "categoryId",
          name: "Category",
          options: categoriesRes.data.data.docs,
        });

        newFilters.push({
          key: "subcategoryId",
          name: "Sub Category",
          options: subCategoriesRes.data.data.docs,
        });
        // newFilters.push({
        //   key: "brandName",
        //   name: "Brand",
        //   options: brandRes.data.data,
        // });
        newFilters.push({
          key: "countryName",
          name: "Country of origin",
          options: countryRes.data.data,
        });

        // Push options from other API calls to newFilters array

        setFilters(newFilters); // Update the filters state
        setAllFilters(newFilters); // Update the filters state
        // handleInputChange('categoryId','')
        // handleInputChange('subcategoryId','')
        // handleInputChange('countryName','')
      }
    );
  }, []);

  useEffect(() => {
    if (params && params["type"]) {
      if (params.type == "tag") {
        const tag = params.id
        const categoryId = params.categoryId
        getProducts({ tag, categoryId });
        return;
      }
      if (params.type == "country") {
        const searchQuery = params.id;
        getProducts({ countryName: searchQuery });
        return;
      }
      if (params.type == "brand") {
        const searchQuery = params.id;
        getProducts({ brandName: searchQuery });
        return;
      }
      if (params.type == "search") {
        const searchQuery = params.id;
        getProductsBySearch(searchQuery);
        return;
      }
      const categoryId = params.categoryId;
      const queryId = params.id;

      getCategoryById(categoryId).then((res) => {
        setCategory(res.data.data);
      });

      if (params.type == "subcategory") {
        getSubcategoryById(queryId).then((res) => {
          setquery(res.data.data);
        });
      } else {
        getTagById(queryId).then((res) => {
          setquery(res.data.data);
          setActiveTag(res.data.data);
        });
      }

      let vars = {};

      if (params.type == "subcategory") {
        vars = {
          subcategoryId: queryId,
        };
      } else {
        vars = { tag: queryId };
      }
      getProducts(vars);
    }
  }, [params]);

  useEffect(() => {
    const clearMessage = setTimeout(() => {
      setAlert("");
    }, 5000);
    return () => clearTimeout(clearMessage);
  }, [alert]);

  const handleInputChange = (filterKey: any, inputValue: any) => {
    let filteredFilters = [...allFilters];
    const newFilteredOptions = filteredFilters.map((filter: any) => {
      if (filter.key === filterKey) {
        const options = inputValue
          ? filter.options.filter((option: any) =>
            option[filterKey == "countryName" ? "country" : "name"]
              .toLowerCase()
              .includes(inputValue.toLowerCase())
          )
          : filter.options;
        return { ...filter, options };
      }
      return filter;
    });
    setFilters(newFilteredOptions);
  };

  const getProductsBySearch = (searchQuery: string) => {
    setIsLoading(true);
    searchForProductsRequest(searchQuery)
      .then((res) => {
        const docs = res.data.data.docs;
        setProducts(docs);

        setIsLoading(false);

        let tags: any = [];
        let tagIds = new Set();
        products.forEach((pr: any) => {
          const productTags = pr.tags;
          productTags.forEach((tag: any) => {
            if (tag.id && !tagIds.has(tag.id)) {
              tagIds.add(tag.id);
              tags.push(tag);
            }
          });
        });
        // setTags(tags)
      })
      .catch((err) => {
        toast.error(prepareErrorResponse(err));
      });
  };

  const getProducts = (query: any) => {
    // setIsLoading(true)
    setIsLoading(true);
    getAllProducts(query)
      .then((res) => {
        const products = res.data.data.docs;
        setProducts(products);
        setAllProducts(products);
        setIsLoading(false);

        if (params.type == "subcategory") {
          let tags: any = [];
          let tagIds = new Set();
          products.forEach((pr: any) => {
            const productTags = pr.tags;
            productTags.forEach((tag: any) => {
              if (tag.id && !tagIds.has(tag.id)) {
                tagIds.add(tag.id);
                tags.push(tag);
              }
            });
          });
          setTags(tags);
        } else {
          getAllTags().then((res) => {
            const tags = res.data.data.docs;
            setTags(tags);
          });
        }
      })
      .catch((err) => {
        setIsLoading(false);

        toast.error(prepareErrorResponse(err));
        setIsLoading(false);
      });
  };

  const getAllProductsSorted = (sortname: string, direction: string) => {
    setSelectedfilter(sortname + "-" + direction);
    const query = {
      [sortname]: direction,
    };
    let object: any = {
      ...query,
    }
    if (activeTag.name) {
      object.tag = activeTag.name
    }
    if (category.id) {
      object.categoryId = category.id
    }
    getProducts(object);
  };

  if (!(params && params.id)) {
    return <AppLoader isLoading={isLoading}></AppLoader>;
  }

  const handleTagClick = (clickedTag: any) => {
    if (params.type == "subcategory") {
      const matchingProducts = products.filter((pr: any) =>
        pr.tags.some((tag: any) => tag.id === clickedTag.id)
      );
      if (activeTag.id == clickedTag.id) {
        setActiveTag({ id: "", name: "" });
        setProducts(allProducts);
      } else {
        setActiveTag(clickedTag);
        setProducts(matchingProducts);
      }
    } else {
      router.push(`/category/shopping/tag/${category.id}/${clickedTag.name}`);
      getAllProducts({
        tag: clickedTag.name,
        categoryId: category.id,
      });
      // if(activeTag.id == clickedTag.id){
      //   setActiveTag({id:''})
      //   setProducts(allProducts)
      //   setIsLoading(true)
      //   getAllProducts().then(res=>{
      //     setProducts(res.data.data.docs)
      //   setIsLoading(false)

      //   }).catch(err=>{
      //     setIsLoading(false)
      //   })
      // }else {
      //   setActiveTag(clickedTag)
      //   setIsLoading(true)
      //   getAllProducts({
      //     tag:clickedTag.name,
      //     categoryId:category.id
      //   }).then(res=>{
      //     setProducts(res.data.data.docs)
      //   setIsLoading(false)

      //   }).catch(err=>{
      //     setIsLoading(false)
      //   })

      // }
    }
  };

  const handleClick = (filter: any, option: any) => {
    if (filter.key == "categoryId") {
      router.push(`/category/shopping/tag/${option.id}/${params.id}`);
    }
    if (filter.key == "subcategoryId") {
      router.push(`/category/shopping/subcategory/${category.id}/${option.id}`);
    }
    if (filter.key == "countryName") {
      setIsLoading(true);
      getAllProducts({
        countryName: option.country,
      }).then((res) => {
        setIsLoading(false);

        setProducts(res.data.data.docs);
      });
    }
  };

  const mobileFilters = () => {
    return (
      <div className="mobile-filters">
        <div onClick={() => router.back()}>
          <Image src={leftArrow} alt="back-arrow"></Image>
        </div>
        <div className="text-white">{query.name}</div>
        <div className="d-flex flex-row">
          <div>
            <Image
              src={filterwhite}
              alt="back-arrow"
              onClick={() => {
                setDisplayModal(!displayModal);
                setActiveSlideContent("filter");
              }}
            ></Image>
          </div>
          <div>
            <Image
              src={sortwhite}
              alt="back-arrow"
              onClick={() => {
                setDisplayModal(!displayModal);
                setActiveSlideContent("sort");
              }}
            ></Image>
          </div>
        </div>
      </div>
    );
  };
  return (
    <div className="wrapper">
      <div className="only-mobile">
        {mobileFilters()}
        <div className="modal-slide-up">
          <div className={`Modal ${displayModal ? "Show" : ""}`}>
            <button
              className="Close"
              onClick={() => setDisplayModal(!displayModal)}
            >
              X
            </button>
            {activeSlideContent == "filter" ? (
              <div className=" hy-custom">
                {filters &&
                  filters.length &&
                  filters.map((fltr: any, index: any) => {
                    return (
                      <div
                        className="bb-1 p-relative section p-3"
                        key={index}
                      >
                        <div>{fltr.name}</div>
                        <div className="b1input">
                          <Image
                            src={SearchIcon}
                            className="search-icon"
                            alt="search"
                          />
                          <input
                            onChange={(e) =>
                              handleInputChange(fltr.key, e.target.value)
                            }
                            type="text"
                            placeholder={"Search By " + fltr.name}
                          />
                        </div>
                        <div className=" scroll-y">

                          {fltr.options &&
                            fltr.options.length &&
                            fltr.options.map((option: any, idx: any) => {
                              return (
                                <div className="radio section p-3" key={idx}>
                                  {/* <input id="r1" type="checkbox" checked={activeSubCategory.id == option.id} onChange={(e)=>setActiveSubCategory({})} /> */}
                                  <label htmlFor="r1">
                                    <div
                                      onClick={() => handleClick(fltr, option)}
                                      className="filter-item-tag"
                                    >
                                      {option.name || option.country}
                                    </div>
                                  </label>
                                </div>
                              );
                            })}
                        </div>
                        {/* <div className="clear link curser-pointer" onClick={()=>handleInputChange(filter.key, '')}>Clear</div> */}
                      </div>
                    );
                  })}
              </div>
            ) : (
              <div className="">
                <br />
                <div
                  className={
                    "section px-4 " +
                    (selectedFilter == "price-desc" ? "active" : "")
                  }
                  onClick={() => getAllProductsSorted("price", "desc")}
                >
                  Price: High to Low
                </div>
                <hr />
                <div
                  className={
                    "section px-4 " +
                    (selectedFilter == "price-asc" ? "active" : "")
                  }
                  onClick={() => getAllProductsSorted("price", "asc")}
                >
                  Price: Low to High
                </div>
                <hr />


                <div
                  className={
                    "section px-4 " +
                    (selectedFilter == "sort_by_name-asc" ? "active" : "")
                  }
                  onClick={() => getAllProductsSorted("sort_by_name", "asc")}
                >
                  Z - A
                </div>
                <hr />
                <div
                  className={
                    "section px-4 " +
                    (selectedFilter == "sort_by_name-desc" ? "active" : "")
                  }
                  onClick={() => getAllProductsSorted("sort_by_name", "desc")}
                >
                  A - Z
                </div>
              </div>
            )}
          </div>
          <div
            className={`Overlay ${displayModal ? "Show" : ""}`}
            onClick={() => setDisplayModal(!displayModal)}
          />
          <p className="Alert">{alert}</p>
        </div>
      </div>
      <AppLoader isLoading={isLoading}></AppLoader>
      <div className="only-desktop">
        <Categories onlyLabels={true} />
      </div>
      <div className="filter-page-path my-2 d-flex">
        <Link href={'/'}> Home </Link> {category.name && <span className="mx-1">&rarr;</span>} <Link href={'/category/' + category.id}>{category.name}</Link> {query.name && <span className="mx-1">&rarr;</span>} <div className="bold text-dark text-black">{query.name}</div>
      </div>
      <div className="row">
        <div className="col-md-3 only-desktop">
          {/* <b>{categoryId + ">" + queryId}</b> */}
          <div className="filtering-table">
            <div className="tbl">
              <div className="part-1 sort">
                <div className="bb-1 filter-header section p-3 p-3">
                  <img
                    alt="sort-logo"
                    src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGAAAABgCAYAAADimHc4AAAACXBIWXMAAAx/AAAMfwF2DF+ZAAAC6klEQVR4nO2cMW4TQRhGvwAdSM4RIkEfiwsQynTcIk0KcgJygw3SNlvmBnS0kWgp4gMgcYQgkTIyGvSPtDLLru2dfz7v7PekFPF6NfZ7sr2eGflovV5D8Hgm91wUgIwCkFEAMgpARgHIKAAZBSCjAGQUgIwCkFEAMgpARgHIKAAZBSCjAGQUgIwCkFEAMgpARgHIKAAZBSDzgv0Ari4vTgB8BHAM4Kaqm3vn8ZY2XuC6qpufnuMNQd0ZZzK+A3huN4UH866qm29O44XIQfjCblpVdbP0GGtbaG9BJv+uJT9wBOCrHfNg2ZIfOM37rP+FEqAlf9Fx+GU45hjhoMgeYEB+ZDGXCFkDbCk/MosI2QLsKD9SfIQsAXrk/+q4+4+N/4uO4B5gQP5ZxynhGn21cVuxEVwDDMn/z5eu3xZmFhHcAuwp/y9V3TzMJYJLgDHyI3OJkDxACvmROURIGiCl/EjpEZIF8JAfKTlCkgCe8iOlRhgdwObzXeVHSoyQ4hVwkkN+ZCDCh9TjeeNxGeomP9ITYXKkDuAuP1JKhNFrwlXd3NlKVnYsgi5DhQJMFgUgowBkFIAMfWfcrlxdXlwD+GSnreyy9yHT2JtTLu/tKnBvpv4KOLUpiGPvgfbcVDBICW9B7hG85GOiAb507KZwizAw0zt6Y+/kAtg0x1mOCFtMs88vADJFyLHGgSl/BnhGyCUfU/8Q9oiQUz5KuApKGSG3fJTyTThFBIZ8lDQVMSYCSz5KmwsaigDg1eY5TPkocTJuIMJNxyk0+Sh1NrQnwuuOu9Pko+Tp6J4IfWSVj9LXA3aMkF0+5rAgs2UEinzMZUWsFeGx4zBNPg4kQHtjVZDhIsIEn9vPIUSemPJxIAHCfs5b+3NdXrTfoHhrY30G8IYpH+wf6xDaFUFHAcgoABkFIKMAZBSAjAKQUQAyCkBGAcgoABkFIKMAZBSAjAKQUQAyCkBGAcgoABkFIKMAZBSAjAKQUQAmAP4Atmt8NOFog9cAAAAASUVORK5CYII="
                  />
                  SORT
                </div>
                <div className="bb-1">
                  <div
                    className={
                      "section px-4 " +
                      (selectedFilter == "price-desc" ? "active" : "")
                    }
                    onClick={() => getAllProductsSorted("price", "desc")}
                  >
                    Price: High to Low
                  </div>
                  <div
                    className={
                      "section px-4 " +
                      (selectedFilter == "price-asc" ? "active" : "")
                    }
                    onClick={() => getAllProductsSorted("price", "asc")}
                  >
                    Price: Low to High
                  </div>
                  <div
                    className={
                      "section px-4 " +
                      (selectedFilter != "sort_by_name-desc" ? "active" : "")
                    }
                    onClick={() => getAllProductsSorted("sort_by_name", "asc")}
                  >
                    A - Z
                  </div>
                  <div
                    className={
                      "section px-4 " +
                      (selectedFilter != "sort_by_name-asc" ? "active" : "")
                    }
                    onClick={() => getAllProductsSorted("sort_by_name", "desc")}
                  >
                    Z - A
                  </div>
                </div>
                <div className="bb-1 filter-header section p-3 p-3 ">
                  <img
                    alt="filter-by-logo"
                    src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGAAAABgCAYAAADimHc4AAAACXBIWXMAAAx/AAAMfwF2DF+ZAAADmElEQVR4nO2dwW3bQBBFJ0HuSQnuQE4HSgV2B5ZPvBCw2YFTAWOAF97sDuQKolQQqwOlA7sCB0RGgLAYUqai2T8U/rtZNMTlPHI5O7ukPry9vQnB8ZGxx0IBYCgADAWAoQAwFACGAsBQABgKAEMBYCgADAWAoQAwFACGAsBQABgKAEMBYCgADAWAoQAwnzx3X5XFQkTOphCIATZ10z54fbnbspSqLJYicuHy5fl5qpv20mOvnl3QqQRfPI+F9wAwrveAhLWIvASPx5YvIjLLsaOcAm7rpl1l3N/BVGUxF5GfOfbFLggMBYDJKeA8ZARssrXVU8Br8netfWtotI31nmM5Gp4CrNHjsiqLsFeCtm1pbJreSFj+HdCzkc516ei8btpQKWlVFl3qubLaWzet20njfQ+Ya8B3memBRsMMvh6DG64C9CxfGH3orCoLt8t6LNqWNPhdmxfeV6p7FlQ3bdcNWYWsq6osbr33vw9tw5Xxb5fadleypKE6Ar62NmnJGoLuO814Oq5zjdqzPiVZlcUPEblJPn7Vm7L72Za05Vz7/c/Jpvu6abNdmdkfU+2ZJ+gknOXKjDTj2RjBd6v794EoRSyMzKgLxEoD48pOupkGf61ty0p2AXqWz63MyHPAs0NfxgMZm0CKcQMSLvQ+4YJ+t9X9wQaGsGqo3nStm92NR2ak35kmAKLzFFkTgF3g74rQPNxKBb8dKxUcmGCp6qZ1u+LeQ4iXdehINB0MHSU9HUg3H+umhY1BtoR5W4pH4Q5VYBtDpBkxj8IdpMA2hjACjl24QxbYxhBqTvhYhTt0gW0M4Sbl9xTu9pYJ9H+gBbYxhFwVoYth741ND0NTmrrN6q7uPRfY/g+h3xk3pnAXqcA2hujrgvoKd1b+vohSYBtDaAEDNSOrapp+Bq3xvJfwK+M0gIdkLs/Rgy9cmoiHAsBQABgKAEMBYCgADAWAoQAwFACGAsBQABgKAEMBYCgADAWAoQAwpyTA/dkCD3K+LcUFnYy/61n5HJ5JC9hZhmK92yfkMpSUyXZBuvrtd0/w11HXAaVM9Qq4NZagbHnsefAjJFMVYAV/u/DWetlGWE4lC/rVveNnasGXU8iCROR73bR3AdpxEFO5AjbGZ39E5OuUgy8TEnCXLE981C4n1Fr/Q5jML2przt8Nul5OIfBb+JPmYFiMA0MBYCgADAWAoQAwFACGAsBQABgKAEMBYCgADAUgEZG/MwM6RoI2gkgAAAAASUVORK5CYII="
                  />
                  Filter By
                </div>
                {filters &&
                  filters.length &&
                  filters.map((fltr: any, index: any) => {
                    return (
                      <div
                        className="bb-1 p-relative section p-3"
                        key={index}
                      >
                        <div>{fltr.name}</div>
                        <div className="b1input">
                          <Image
                            src={SearchIcon}
                            className="search-icon"
                            alt="search"
                          />
                          <input
                            onChange={(e) =>
                              handleInputChange(fltr.key, e.target.value)
                            }
                            type="text"
                            placeholder={"Search By " + fltr.name}
                          />
                        </div>
                        <div className=" scroll-y">
                          {fltr.options &&
                            fltr.options.length &&
                            fltr.options.map((option: any, idx: any) => {
                              return (
                                <div className="radio section p-3" key={idx}>
                                  {/* <input id="r1" type="checkbox" checked={activeSubCategory.id == option.id} onChange={(e)=>setActiveSubCategory({})} /> */}
                                  <label htmlFor="r1">
                                    <div
                                      onClick={() => handleClick(fltr, option)}
                                      className="filter-item-tag"
                                    >
                                      {option.name || option.country}
                                    </div>
                                  </label>
                                </div>
                              );
                            })}
                        </div>

                        {/* <div className="clear link curser-pointer" onClick={()=>handleInputChange(filter.key, '')}>Clear</div> */}
                      </div>
                    );
                  })}
              </div>
            </div>
          </div>
        </div>
        <div className="col-xl-9">
          <h1>{category.name}</h1>
          {
            params.type != "tag" &&
            <>
              <div className="category-items-grid">
                {tags.map((tag: any, ix) => {
                  return (
                    <div
                      className={
                        "c-item text-center curser-pointer " +
                        (activeTag.id == tag.id ? "active-tag" : "")
                      }
                      key={ix}
                      onClick={() => handleTagClick(tag)}
                    >
                      <img alt="c-item" src={tag.imageUrl} />
                      <div>{tag.name}</div>
                    </div>
                  );
                })}
              </div>
              <hr />
            </>
          }

          {products && products.length ? (
            <div className="grid-regular md">
              {products &&
                products.length &&
                [...products].map((product, index) => {
                  return <ProductItem product={product} key={index} />;
                })}
            </div>
          ) : (
            <div className="text-center m-5">No matching results</div>
          )}
        </div>
      </div>
    </div>
  );
}
