// src/pages/index.tsx

import Categories from "@/components/categories";
import ProductItem from "@/components/productItem";
import ProductItemMini from "@/components/productItemMini";
import React, { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import gArrow from "../public/icons/arrow-down.svg"
import cardsIcon from "../public/icons/cards.svg";
import { useRouter } from "next/router";

import Image from "next/image";
import ProductsSlider from "@/components/productsSlider";

import { getAllProducts } from "@/network/categories";
import { toast } from "react-toastify";
import prepareErrorResponse from "@/errorHandlerLayer/errorHandler";
import { getAddresses, getCurrentUser, updateAddress } from "@/network/auth";
import withAuth from "@/helpers/withAuth";
import Cookies from "js-cookie";
import cartReducer, { setCartItems } from "@/store/reducers/cartReducer";
import { useDispatch } from "react-redux";
import { createOrderRequest, getStripeKeys, getStripeUrl, removeItemsFromCartPatchRequest } from "@/network/cart";
import Select from 'react-select'
import AppLoader from "@/components/loader";
import PaymentStatusModal from "@/components/modals/paymentStatus";
import Link from "next/link";
import { formatPrice, toAPIFormat, toMonthFormat, toWeekDay, toWeekDayFullName } from "@/helpers/dateHelpers";
import { getTimeSlots } from "@/network/timeSlots";
import { time } from "console";
import mainCitiesList from "../constants/maincities.json";
import useTranslation from "next-translate/useTranslation";
// import {useStripe, useElements, PaymentElement,ElementsConsumer} from '@stripe/react-stripe-js';

interface TimeSlot {
  deliveryTime: string;
  timing: string;
}

interface GroupedTimeSlot {
  key: string;
  values: TimeSlot[];
}

function Checkout() {
  const { t } = useTranslation('common')

  interface CustomStyles {
    [key: string]: any;
    control: (provided: any) => any;
    option: (provided: any, state: any) => any;
    dropdownIndicator: any
    indicatorContainer: (provided: any) => any;
  }

  
  const cartItems = useSelector((state: any) => state.cart.cartItems);
  const user = useSelector((state: any) => state.user.user);
  const [isLoading, setIsLoading] = useState(false)

  const [selectedtimeSlot, setSelectedTimeSlot]: any = useState('');
  const [selectedDay, setSelectedDay]: any = useState(null);
  const [deliveryContactAddress, setDeliveryContactAddress] = useState(user.info.general.phoneNumber)
  const [products, setProducts] = useState([]);
  const [addresses, setAddresses] = useState([]);
  const [selectedAddress, setSelectedAddresss]: any = useState({ id: "" });
  const [activeStep, setActiveStep] = useState(1);
  const [isRetry, setIsRetry] = useState(false);
  const [selectedTimeSlotLocation, setSelectedTimeSlotLocation]: any = useState(null);
  const [isTimeSelectOpen, setIsTimeSelectOpen] = useState(false);
  const [deliveryInstructions, setDeliveryInstructions] = useState('')
  // const stripe = useStripe();
  // const elements = useElements();
  const dispatch: any = useDispatch()
  const subTotal = cartItems.reduce((accumulator: any, product: any) => {
    const price = product.finalPriceAfterDiscount
    return accumulator + (price * product.quantityInCart);
  }, 0);
  const router = useRouter();


  const customStyles: CustomStyles = {
    control: provided => ({
      ...provided,
      border: 'none',
      boxShadow: 'none',
      height: '40px',
      position: 'relative',
    }),
    option: (provided, state) => ({
      ...provided,
      backgroundColor: state.isFocused ? '#8cc751' : 'white',
      color: state.isFocused ? 'white' : 'black',
    }),
    dropdownIndicator: (provided: any, state: any) => ({
      ...provided,
      backgroundColor: state.isFocused ? '#8cc751' : '#8cc751',
      color: 'white',
      padding: '8px',
      borderRadius: '0',
      alignSelf: 'center',
      position: 'absolute',
      right: '0',
      width: '45px',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center', top: '0px',
      height: '44px'
    }),
    indicatorContainer: provided => ({
      ...provided,
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      height: '100%',
      margin: '0',
      paddingRight: '4px',
    }),
  };



  useEffect(() => {
    const params = router.query;
    const redirectStatus: any = params.redirect_status;
    if (redirectStatus === 'success') {
      setActiveStep(3);
      // router.replace(router.pathname, undefined, { shallow: true });
    } else if (redirectStatus === 'failed') {
      setActiveStep(1);
      setIsRetry(true);

    }

  }, [router.query]);


  useEffect(() => {

    if (!Cookies.get('token')) {
      return
    }
    getAllProducts({ page: 1, limit: 20 })
      .then((res) => {
        setProducts(res.data.data.docs);

      })
      .catch((err) => {
        toast.error(prepareErrorResponse(err));
      });
    getAddresses()
      .then((res) => {
        const addresses = res.data?.data?.docs ?? [];
        const selectedAddress = addresses.find((item: any) => { return item.isDefaultAddress });
        setAddresses(addresses);
        setSelectedAddresss(selectedAddress);

        updateTimeSlots(selectedAddress, addresses);
      })
      .catch((err) => {
        toast.error(prepareErrorResponse(err));
      });
  }, []);

  const handleSelectAddress = (option: any) => {
    setSelectedAddresss(option.value)
    const updatedAddresses: any = addresses.map((address: any) => {
      if (option.value.id === address.id) {
        address = { ...address, isDefaultAddress: true };
      } else {
        address = { ...address, isDefaultAddress: false };
      }
      updateAddress(address, address.id)
      return address
    });
    setAddresses(updatedAddresses);
    updateTimeSlots(option.value, updateAddress);
  };

  const updateTimeSlots = (selectedAddress: any, addresses: any) => {
    if (selectedAddress && selectedAddress.id) {
      if (addresses && addresses.length) {
        var addressTitle = selectedAddress.address;
        var isDubai = mainCitiesList.Dubai.areas.find((val: any) => { return val.toLowerCase() == addressTitle.toLowerCase() }) != null;
        setSelectedTimeSlotLocation(isDubai ? "Dubai" : "AbuDhabi")
        getTimeSlotsForCity(isDubai ? "Dubai" : "AbuDhabi");
      }
    }
  }

  const nextStep = () => {

    if (activeStep < 3) {
      if (activeStep == 1) {
        if (!isFormValid(1)) {
          return
        } else {
          setActiveStep(activeStep + 1);

        }
      }
      if (activeStep == 2) {

        // setIsLoading(true)
        // getStripeKeys().then(res=>{
        //   const keys = 

        // }).catch(err=>{
        //   console.log({err})
        // }).finally(()=>{
        // })
        return
        setActiveStep(activeStep + 1);
      }
    } else {


      // submit order
    }
  };

  const isFormValid = (formNumber: any) => {
    if (formNumber == 1) {
      if (!(cartItems && cartItems.length)) {
        toast.error('You dont have any items in the cart')
        return false
      }
      if (!(selectedAddress && selectedAddress.id)) {
        toast.error('Please select address')
        return false
      }
      if (!deliveryContactAddress) {
        toast.error('Please enter a valid phone number')
        return false
      }
      if (!selectedDay) {
        toast.error('Please select a delivery day')
        return false
      }
      if (!selectedtimeSlot) {
        toast.error('Please select a delivery time')
        return false
      }
      return true
    }
  }

  const getStep = () => {
    if (activeStep == 1) {
      return step1();
    }
    if (activeStep == 2) {
      return step2();
    }
    if (activeStep == 3) {
      return step3();
    }
  };

  const clearAll = () => {
    const products: any = []
    dispatch(setCartItems(products))
  }


  const options = addresses.map((address: any) => ({
    label: address.addressTitle + '-' + address.street,
    value: address,
  }));

  const [timeSlots, setTimeSlots] = useState<any>([{ key: "", values: [] }]);
  const [timeSlotsLocations, setTimeSlotsLocations] = useState<any>([{ id: "Dubai", name: "Dubai" }, { id: "AbuDhabi", name: "Abu Dhabi" }]);

  const getTimeSlotsForCity = async (city: string) => {
    try {
      const currentTimeStamp = Math.floor(Date.now() / 1000);
      setSelectedTimeSlotLocation(city);
      const response = await getTimeSlots(currentTimeStamp, city);
      const respTimeSlots = response.data?.data ?? [];
      var groupedTimeSlot: GroupedTimeSlot[] = []
      var found = false
      respTimeSlots.forEach((timeSlot: any) => {
        groupedTimeSlot.forEach((groupedTimeSlot) => {
          if (groupedTimeSlot.key == timeSlot.delivery_day) {
            groupedTimeSlot.values.push(timeSlot);
            found = true;
          }
        })

        if (!found) {
          groupedTimeSlot.push({
            key: timeSlot.delivery_day,
            values: [timeSlot]
          })
        }
        found = false
      })

      setTimeSlots(groupedTimeSlot);
    } catch (error) {
      console.error("Error fetching time slots:", error);
      return [];
    }
  };

  // Function to handle time slot selection
  const handleTimeSlotSelect = (selectedtimeSlot: any) => {
    setSelectedTimeSlot(selectedtimeSlot)
  };

  // Function to handle day selection
  const handleDaySelect = (day: any) => {
    setSelectedDay(day);
  };
  const getDayName = (date: any) => {
    return toWeekDayFullName(date)
  };

  const getAmount = () => {
    const totalAmount = cartItems.reduce((accumulator: any, product: any) => {
      const price = product.finalPriceAfterDiscount
      return accumulator + (price * product.quantityInCart);
    }, 0);
    return totalAmount.toFixed(3)
  }
  const getPromotonAmount = () => {
    const promotionAmount = 0
    return promotionAmount.toFixed(3)
  }
  const getNetAmount = () => {
    return getAmount() + getPromotonAmount()
  }
  const getWalletBalance = () => {
    const walletBalance = user.info.general.walletBalance
    return walletBalance.toFixed(3)
  }

  const getNetPayableAmount = () => {
    const netAmount = getNetAmount();
    const walletBalance = getWalletBalance();
    const netPayableAmount = Math.max(parseFloat(walletBalance) - parseFloat(netAmount), 0);
    return netPayableAmount.toFixed(3); // Fixed to 3 decimal places
  };

  const formatDate = (obj: any) => {
    // Extract day, month, and year from the 'name' property
    const [month, day] = obj.name.split(' ');
    const year = new Date().getFullYear(); // Assuming the current year

    // Create a map to convert month names to their corresponding numbers
    const months: any = {
      "Jan": "01", "Feb": "02", "Mar": "03", "Apr": "04", "May": "05", "Jun": "06",
      "Jul": "07", "Aug": "08", "Sep": "09", "Oct": "10", "Nov": "11", "Dec": "12"
    };

    // Get the numeric month value from the map
    const formattedMonth = months[month];

    // Get the full day name based on the abbreviated day provided in the object
    const fullDayNames = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
    const dayIndex = fullDayNames.findIndex(dayName => dayName.startsWith(obj.day));
    const fullDayName = fullDayNames[dayIndex];

    // Create the formatted date string
    const formattedDate = `${fullDayName} ${day}/${formattedMonth}/${year}`;

    return formattedDate;
  }


  const handleSubmit = async (event: any) => {
    // We don't want to let default form submission happen here,
    // which would refresh the page.
    event.preventDefault();
    setIsLoading(true)
    var orderTime: any = {
      "scheduledDate": toAPIFormat(selectedDay),
      "scheduledTime": selectedtimeSlot
    }

    if (deliveryInstructions.trim().length > 0) {
      orderTime.deliveryInstruction = deliveryInstructions
    }

    getStripeUrl(orderTime).then(res => {
      window.location.href = res.data.url
      setIsLoading(false)

    }).catch(err => {
      console.log({ err })
      toast.error(prepareErrorResponse(err))
      setIsLoading(false)

    })

    // if (!stripe || !elements) {
    //   // Stripe.js hasn't yet loaded.
    //   // Make sure to disable form submission until Stripe.js has loaded.
    //   return;
    // }

    // const result = await stripe.confirmPayment({
    //   //`Elements` instance that was used to create the Payment Element
    //   elements,
    //   confirmParams: {
    //     return_url: window.location.href,
    //   },
    // });

    // if (result.error) {
    //   // Show error to your customer (for example, payment details incomplete)
    //   console.log(result.error.message);
    // setIsLoading(false)

    // } else {
    // setIsLoading(false)

    //   // Your customer will be redirected to your `return_url`. For some payment
    //   // methods like iDEAL, your customer will be redirected to an intermediate
    //   // site first to authorize the payment, then redirected to the `return_url`.
    // }
  };

  const handleBackToHome = () => {
    router.replace('/', undefined, { shallow: true });
  }
  const step3 = () => {
    return <div className="p-3 d-flex flex-column align-items-center justify-content-center success my-4 card">
      <div className="col-md-6 mx-auto">
        <div className="alert alert-success text-center w-100" role="alert">
          Order created successfully. Thank you!
        </div>
        <button className="primary w-100" onClick={handleBackToHome}>
          Back to Home
        </button>
      </div>
    </div>
  };

  const step2 = () => {
    return (
      <div className="row">
        <div className="col-md-6 m-auto">
          <div className="information-box mb-2 ">
            <div className="label-text">{t("ORDER SAMMARY")}</div>
            <div className="data-box">
              <div className="">{t("Gross Amount")}</div>
              <div className="">{formatPrice(getAmount())}</div>
            </div>
            {/* <div className="data-box no-bot">
              <div className="">{t("Promotion Amount")}</div>
              <div className="">{getPromotonAmount()}</div>
            </div>
            <div className="data-box no-bot">
              <div className="">{t("Net Amount")}</div>
              <div className="">{getNetAmount()}</div>
            </div>
            <div className="data-box">
              <div className="">{t("Wallet Balance")}</div>
              <div className="">{getWalletBalance()}</div>
            </div>
            <div className="data-box no-top">
              <div className="bold">{t("Net Payable Amount")}</div>
              <div className="">{getNetPayableAmount()}</div>
            </div> */}
          </div>
          <div className="label-text mt-3">{t("Address")}</div>
          <div className="data-box d-block h-auto">
            <div>{selectedAddress.addressTitle}</div>
            <div>{selectedAddress.address}</div>
            <div>{selectedAddress.nearestLand}</div>
            <div>{selectedAddress.street}</div>
            <div>{selectedAddress.contactNumber}</div>
          </div>
          <div className="label-text mt-3">{t("Delivery")}</div>
          <div className="d-flex w-100">
            <div className="data-box w-100 v-center justify-content-center bdr w-100">
              <div className="bdr">My Cart</div>
            </div>
            <div className="data-box w-100 v-center justify-content-center bdr w-100">
              <div className="bdr">{selectedDay}</div>
            </div>
            <div className="data-box w-100 v-center justify-content-center w-100">
              <div>{selectedtimeSlot}</div>
            </div>
          </div>


        </div>
        <div className="col-md-6 d-none">
          <div className="label-text">{t("PAYMENT MODE")}</div>
          <div className="data-box justify-content-start no-bot">
            <div className="icon mx-2">
              <input type="radio" name="paymentMode" />
            </div>
            <div>
              {t("Pay with wallet (your wallet balance is : 0)")}
            </div>
          </div>
          <div className="data-box justify-content-start">
            <div className="icon mx-2">
              <input type="radio" name="paymentMode" />
            </div>
            <div>{t("Pay Online")}</div>
          </div>
          <br />
          <br />
          <div className="data-box h-fit  no-bot justify-content-start">
            <div className="icon mx-2">
              <input type="radio" name="with" />
            </div>
            <div>{t("Pay with new card")}</div>
            <Image className="card-images" src={cardsIcon} alt="icon image" />
          </div>

          <div className="data-box justify-content-start">
            <div className="icon mx-2">
              <input type="radio" name="save" />
            </div>
            <div>{t("Save my card securly")}</div>
          </div>
          <div className="data-box no-top justify-content-start">
            <div className="icon mx-2">
              <input type="radio" name="iagree" />
            </div>
            <div>
              {t("I agree and accept the terms and conditions")}
            </div>
          </div>
          <br />
          {/* <button className="info">Cancel Payment</button> */}

          <div></div>
        </div>
        <br />
        <br />
        <div className="my-3"></div>
      </div>
    );
  };
  const resetRetry = () => {
    router.replace(router.pathname, undefined, { shallow: true });
    setIsRetry(false)
  }
  const step1 = () => {
    return (
      <div>

        {isRetry && <PaymentStatusModal
          isModalOpen={true}
          close={() => resetRetry()}
        />
        }
        <div className="checkout-mini-header text-upper-case px-3 text-l-grey">

          {t("Things you might like")}
        </div>
        <ProductsSlider products={[...products]} mini={true} slides={10} title='' />

        <div className="checkout-mini-header text-upper-case px-3">
          <div className="d-flex v-center">

            <div className="text-l-grey">{cartItems.length} {t("ITEMS")}</div>
            <button className="mx-2 clear-btn" onClick={clearAll}>
              {t("Clear All")}</button>
          </div>
          <div className="text-l-grey">
            SUBTOTAL : {subTotal.toFixed(2)} AED
          </div>
        </div>
        {cartItems && cartItems.length ? <div className="d-grid-50 p-2">
          {cartItems.map((offer: any, index: number) => {
            return (
              <div className="border-t" key={index}>
                <ProductItem product={offer} flex={true} key={index} itemType='cart' />
              </div>
            );
          })}
        </div> : <div className="not-cart-items text-center">
          <div className="empty-text mb-2">Your shopping cart is empty</div>
          <Link href={"/"}>
            <button className="light-green">SHOP NOW</button>
          </Link>
        </div>}
        <hr />
        <div className="d-grid-50  m-auto checkout-p-1">
          <div className="checkout-information">
            <div className="information-box mb-2 ">
              <div className="label-text">{t("Delivery Address")}</div>
              <Select
                styles={customStyles}
                className="input-shopping-cart p-0"
                defaultValue={options[0]} // Make sure options[0] is a valid option from the options array
                options={options}
                onChange={(selectedOption) => handleSelectAddress(selectedOption)}
              />

              {selectedAddress && selectedAddress.id && <div className="selected-addess">
                <div>{selectedAddress.address}</div>
                <div>{selectedAddress.addressTitle}</div>
                <div>{selectedAddress.contactNumber}</div>
                <div>{selectedAddress.isDefaultAddress}</div>
                <div>{selectedAddress.nearestLand}</div>
              </div>}
              <div
                className="text-sm fs-sm link curser-pointer mb-2"
                onClick={() => router.push("/account/myaddresses")}
              >
                {t("create new address ?")}
              </div>
            </div>

            <div className="information-box mb-2  mb-2">
              <div className="label-text">
                {t("Delivery Contact Number")}
              </div>
              <input className="text-box" placeholder="" value={deliveryContactAddress} onChange={(e) => setDeliveryContactAddress(e.target.value)} />
              <div className="text-sm fs-sm link curser-pointer" onClick={() => router.push('/account/myprofile')}>
                {t("What to update ? click here")}
              </div>
            </div>
            <div className="information-box mb-2  mb-2">
              <div className="label-text">{t("Schedule Delivery")}</div>
              <div className="d-flex">
                <input
                  className="text-box"
                  placeholder={t("Day")}
                  value={getDayName(selectedDay)}
                  disabled
                />
                <div
                  className="g-arrow"
                  onClick={() => setIsTimeSelectOpen(!isTimeSelectOpen)}
                >
                  <Image src={gArrow} alt="garrow" />
                </div>
                <input
                  className="text-box"
                  placeholder={t("Time")}
                  value={selectedtimeSlot}
                  disabled
                />
              </div>
              {isTimeSelectOpen && (
                <>
                  <div className="time-select-modal">
                    {/* <div className="label-text">Choose a location</div>
                    <div className="days d-flex mb-2">
                      {timeSlotsLocations.map((data: any, index: number) => (
                        <div
                          key={index}
                          onClick={() => {
                            getTimeSlotsForCity(data.id);
                          }}
                        >
                          <div className={`time-box wrapper ${selectedTimeSlotLocation === data.id ? "selected" : ""}`}>{data.name}</div>
                        </div>
                      ))}
                    </div> */}
                    {
                      selectedTimeSlotLocation &&
                      <>
                        <div className="label-text">Choose a date</div>
                        <div className="days d-flex mb-2">
                          {timeSlots.map((groupedTimeSlot: GroupedTimeSlot, index: number) => (
                            <div
                              key={index}
                              onClick={() => {
                                handleDaySelect(groupedTimeSlot.key)
                              }}
                            >
                              <div className={`time-box wrapper ${selectedDay === groupedTimeSlot.key ? "selected" : ""}`}>{toMonthFormat(groupedTimeSlot.key)}</div>
                              <div className="text-center">{toWeekDay(groupedTimeSlot.key)}</div>
                            </div>
                          ))}
                        </div>
                        {
                          selectedDay &&
                          <>
                            <div className="label-text">Choose a time
                            </div>
                            <div className="times d-flex">
                              {timeSlots.length > 0 && timeSlots.find((item: GroupedTimeSlot) => { return item.key == selectedDay })?.values.map((timeSlot: TimeSlot, index: number) => (
                                <div
                                  key={index}
                                >
                                  <div
                                    className={"time-box long " + (selectedtimeSlot == timeSlot.timing ? "selected" : "")}
                                    onClick={() => handleTimeSlotSelect(timeSlot.timing)}
                                  >
                                    {timeSlot.timing}
                                  </div>
                                </div>
                              ))}
                            </div>
                          </>
                        }
                      </>
                    }

                    <div className="my-3">
                      <button className="btn-1 green" onClick={() => setIsTimeSelectOpen(!isTimeSelectOpen)}>
                        SELECT
                      </button>
                    </div>
                  </div>
                </>
              )}
            </div>
            <div className="information-box mb-2  mb-2">
              <div className="label-text">{t("Delivery Instructions")}</div>
              <textarea className="text-box" placeholder="Type your delivery instructions" value={deliveryInstructions} onChange={(e)=>setDeliveryInstructions(e.target.value)}></textarea>
              </div>
          </div>
          <div className="checkout-extras d-none">

            <div className="information-box mb-2 ">
            </div>
          </div>
          <div className="my-3">
            <button className="btn-1 green" onClick={() => nextStep()}>
              {t("Proceed to checkout")}
            </button>
          </div>
        </div>
      </div>
    );
  };

  return (
    <div className="wrapper">

      <div className="only-desktop">
        <Categories />
      </div>
      <div className="checkout-mini-header">
        <div className="step-wrapper">
          <span className={"step " + (activeStep === 1 ? "active" : "")}>
            1
          </span>
          <span className="step-text ptr" onClick={() => activeStep == 2 ? setActiveStep(1) : null}>
            {t("Checkout")}
          </span>
        </div>
        <div className="step-wrapper">
          <span className={"step " + (activeStep === 2 ? "active" : "")}>
            2
          </span>
          <span className="step-text ptr" onClick={() => nextStep()}>
            {t("Confirm")}
          </span>
        </div>
        <div className="step-wrapper">
          <span className={"step " + (activeStep === 3 ? "active" : "")}>
            3
          </span>
          <span className="step-text ptr" >
            {t("Thank You")}
          </span>
        </div>
      </div>
      {getStep()}
      <div>
        {activeStep == 2 &&
          <div className="row">
            <div className="col-md-6 m-auto">
              {/* <div className="label-text">{t("Payment Information")}</div> */}




              <form onSubmit={handleSubmit}>
                {/* <PaymentElement /> */}
                <br />

                <button className="btn-1 green" type="submit" onClick={handleSubmit}>{t("Pay Now")}</button>
              </form>
            </div>

          </div>
        }

      </div>
      <AppLoader isLoading={isLoading} />
    </div>
  );
}

export default withAuth(Checkout);
