import React from "react";
import Image from "next/image";
import Link from "next/link";
import competitionImage from "../public/images/competition.jpg";
import shoppingCartIcon from "../public/icons/shoppingCart.svg";
import completeIcon from "../public/icons/complete.svg";
import saveImage from "../public/icons/saveImage.svg";

import useTranslation from "next-translate/useTranslation";

function Competition() {
  const {t} = useTranslation('common')

  return (
    <div className="wrapper p-3">
      <div className="row">
        <div className="col-md-6">
          <div className="refer-image">
            <Image
              alt={t("competition image")}
              src={competitionImage}
            />
          </div>
        </div>
        <div className="col-md-6">
          <div className="p-5">{t("WIN A TRIP TO SOUTH AFRICA!!")}</div>
          <div className="d-flex justify-content-between">
            <div className="p-3">
              <div className="text-center rounded-icon-primary">
                <Image alt={t("shopping")} src={shoppingCartIcon} />
              </div>
              <div className="text-center w-100 primary-text">
                {t("PURCHASE")}
              </div>
              <div className="my-3 p1">
                {t(
                  "Purchase Nectarines, Peaches or Plums of South African origin at Kibsons"
                )}
              </div>
            </div>
            <div className="p-3">
              <div className="text-center rounded-icon-primary">
                <Image alt={t("competition-icon")} src={completeIcon} />
              </div>
              <div className="text-center w-100 primary-text">
                {t("COMPLETE")}
              </div>
              <div className="my-3 p1">
                {t(
                  "Complete the Competition Entry Form by telling us about the product quality"
                )}
              </div>
            </div>
            <div className="p-3">
              <div className="text-center rounded-icon-primary">
                <Image alt={t("save-image")} src={saveImage} />
              </div>
              <div className="text-center w-100 primary-text">
                {t("SUBMIT")}
              </div>
              <div className="my-3 p1">
                {t(
                  "Validate and Submit your Competition Entry Form by telling us about your delivery experience"
                )}
              </div>
            </div>
          </div>
          <div>
            <span className="text-danger m">*</span>
            <span className="mx-2">
              {t("One entry form permitted per invoice")}
            </span>
          </div>
          <p className="p2">
            {t(
              "South African Peaches, Nectarines and Plums are brought direct to you from South Africa’s Western Cape region - recognised worldwide as Africa’s number 1 stone-fruit growing area. Taste South Africa fruit assures you of quality because it’s cultivated to strictest international standards of environmental, ethical cultivation and fruit-growing science. Sweet, juicy and really nutritious, our fruit is also packed with essential vitamins, minerals and nutrients. Perfect for fresh summer snacks! Great for salads, pies and desserts or a meat sauce!"
            )}
          </p>
          <p className="text-bordo">
            {t("I have read the terms and conditions below:")}
          </p>
          <p className="">
            {t(
              "This competition is open only to customers of Kibsons participating in the 2023 Taste South Africa Plums, Peaches & Nectarines Promotion."
            )}
          </p>
          <p className="">
            {t(
              "To qualify for entry, you must have purchased Taste South Africa fruit from Kibsons anytime during the promotional period - 20th January 2023 to 30th April 2023."
            )}
          </p>
          <p className="">
            {t(
              "You may enter the competition more than once every time you make a purchase of Taste South Africa fruit during the promotional period."
            )}
          </p>
          <p className="">
            {t(
              "To enter this competition, complete the entry online at www.kibsons.com or via the Kibsons app."
            )}
          </p>
        </div>
      </div>
    </div>
  );
}

export default Competition;
