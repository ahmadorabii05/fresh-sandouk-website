// src/pages/index.tsx

import React from "react";
import Image from "next/image";
import Link from "next/link";
import basketImage from "../public/icons/basket.png";
// import hiw1 from "../public/images/hiw1.png";
// import hiw2 from "../public/images/hiw2.png";
// import hiw3 from "../public/images/hiw3.png";
// import hiw4 from "../public/images/hiw4.png";
import line from "../public/images/line.png";
import moneyIcon from "../public/icons/money.png";
// import walletIcon from "../public/icons/wallet.png";
import deliveryIcon from "../public/icons/delivery1.png";
import rewardIcon from "../public/icons/reward.png";
import meetIcon from "../public/icons/meet.png";

import useTranslation from "next-translate/useTranslation";

function HowItWorks() {
  const {t} = useTranslation('common')

  return (
    <div className="wrapper p-3">
      <hr />
      <div className="row">
        <div className="col-md-7">
          <div className="flex-wrapped d-flex justify-content-between v-center">
            {/* <h1>{t('How it works')}: </h1> */}
            <div>
             {t('Items freshly handpicked and delivered from the market to your door. Online shopping at wholesale prices.')}
            </div>
          </div>
          <br />
          <br />
          {/* a */}
          <div className="d-flex justify-content-between v-center howitworkswrapper">
            <Image className="line-image" src={line} alt="line" />
            <div className="howitworkstep s1">
              <Image src={moneyIcon} alt="hiw1" />
              <div>
                <div className="primary-text">QUICK SHOPPING</div>
                <div>
                  <div>Keep your preferred items secure.</div>
                  <div>Ensuring the best prices.</div>
                </div>
              </div>
            </div>

            <div className="howitworkstep s1">
              <Image src={deliveryIcon} alt="hiw2" />
              <div>
                <div className="primary-text">FREE DELIVERY</div>
                <div>
                  <div>Get your delivery when you want</div>
                  <div>Receive your delivery at your convenience.</div>
                </div>
              </div>
            </div>

            <div className="howitworkstep s1">
              <Image src={rewardIcon} alt="hiw3" />
              <div>
                <div className="primary-text">REFER & EARN</div>
                <div>
                  <div>Accumulate credits as your friends make purchases with us.</div>
                </div>
              </div>
            </div>

            <div className="howitworkstep s1">
              <Image src={meetIcon} alt="hiw4" />
              <div>
                <div className="primary-text">{t('CONTACT US')}</div>
                <div>
                  <div>{t('Fresh Sandouk')}</div>
                  <div>sales@freshsandouk.com</div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="col-md-3"></div>
        <div className="col-md-2">
          <Image className="basketImg" src={basketImage} alt="basketImage" />
        </div>
      </div>
    </div>
  );
}

export default HowItWorks;
