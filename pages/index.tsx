// src/pages/index.tsx

import BrandsCarousel from "@/components/brandsCarousel";
import Categories from "@/components/categories";
import ContainerRow from "@/components/containerRow";
import Footer from "@/components/footer";
import MyCarousel from "@/components/myCarousel";
import ProductItem from "@/components/productItem";
import React, { useState, useEffect, useRef } from "react";
import { useSelector } from "react-redux";
import MainLayout from "../layouts/MainLayout";
import ProductsSlider from "@/components/productsSlider";
import { getAllProducts, getAllTags } from "@/network/categories";
import { useDispatch } from "react-redux";

import AppLoader from "@/components/loader";
import { useRouter } from "next/router";
import Image from 'next/image'
import arrowTop from '../public/icons/arrowTop.svg'
import ArrowTop from "@/components/icons/ArrowTop";
import { getAllBanners } from "@/network/banners";
import useTranslation from "next-translate/useTranslation";
function HomePage() {
  const { t } = useTranslation('common')

  const dispatch = useDispatch()
  const [isLoading, setIsLoading] = useState(false)
  const [showButton, setShowButton] = useState(false);
  const mainSliderImagesDesktop = useSelector(
    (state: any) => state.home.mainSliderImagesDesktop
  );
  const mainSliderImagesMobile = useSelector(
    (state: any) => state.home.mainSliderImagesMobile
  );


  const offerImages = useSelector((state: any) => state.home.offerImages);
  const [products, setProducts]: any = useState([])
  const [bannersSection1, setBannersSection1]: any = useState([])
  const [bannersSection2, setBannersSection2]: any = useState([])
  const [topOffers, setTopOffers]: any = useState([]);
  const [newInSeason, setNewInSeason]: any = useState([]);
  const [page, setPage] = useState(1);
  const [hasMore, setHasMore] = useState(true);
  const [isCalled, setIsCalled] = useState(false);


  function getRandomItemsFromArray(array: any, numItems: any) {
    const shuffled = array.sort(() => 0.5 - Math.random()); // Shuffle the array
    return shuffled.slice(0, numItems); // Get the first 'numItems' items
  }

  // Get random 10 items from the 'products' array
  const categoriedProducts = getRandomItemsFromArray(products, 10);

  useEffect(() => {
    if (!(products && products.length)) {
      loadMoreProducts()
    }
    getTopOfferProducts();
    getNewInSeasonProducts();
    getBanners()
  }, [])

  const getTopOfferProducts = async () => {
    const response = await getAllProducts({ limit: 20, tag: "PROMOTION" });
    setTopOffers(response?.data?.data?.docs ?? [])
  }

  const getNewInSeasonProducts = async () => {
    const response = await getAllProducts({ limit: 20, tag: "NEW ARRIVALS" });
    setNewInSeason(response?.data?.data?.docs ?? [])
  }

  // useEffect(() => {
  //   const handleScroll = () => {
  //     if (
  //       window.innerHeight + document.documentElement.scrollTop ===
  //       document.documentElement.offsetHeight
  //     ) {
  //       console.log('we are in bottom');
  //     }
  //     const almostBottom = (document.documentElement.offsetHeight - (window.innerHeight + document.documentElement.scrollTop)) < 420

  //     if (almostBottom && !isCalled) {
  //       loadMoreProducts()
  //       setIsCalled(true);
  //     }

  //     if (window.scrollY > 300) {
  //       setShowButton(true);
  //     } else {
  //       setShowButton(false);
  //     }
  //   };

  //   window.addEventListener('scroll', handleScroll);

  //   return () => {
  //     // Cleanup: Remove the scroll event listener when the component unmounts
  //     window.removeEventListener('scroll', handleScroll);
  //   };
  // }, [isCalled]); // Include isCalled in the dependency array to ensure the effect updates when isCalled changes


  const getBanners = async () => {
    const response = await getAllBanners();
    const banners = response?.data?.data?.docs ?? []
    const section1 = banners.filter((item: any) => { return item.location == "home_page_section_1" }).sort((item1: any, item2: any) => { return item1.priority - item2.priority }).map((item: any) => { return { imageUrl: item.imageUrl, actionUrl: item.actionUrl } })
    const section2 = banners.filter((item: any) => { return item.location == "home_page_section_2" }).sort((item1: any, item2: any) => { return item1.priority - item2.priority }).map((item: any) => { return { imageUrl: item.imageUrl, actionUrl: item.actionUrl } })
    setBannersSection1(section1)
    setBannersSection2(section2)
  }


  const loadMoreProducts = async () => {
    if (hasMore) {
      try {
        setIsLoading(true);

        const response = await getAllProducts({ limit: 40, page });
        const newProducts = response.data.data.docs;
        const hasNextPage = response.data.data.hasNextPage;

        // Reset isCalled to false here
        setIsCalled(false);
        setHasMore(hasNextPage);
        setProducts((prevProducts: any) => [...prevProducts, ...newProducts]);
        setPage(page + 1);
        setHasMore(newProducts.length > 0);
        setIsLoading(false);
      } catch (err) {
        setIsLoading(false);
        console.error('Error fetching more products:', err);
      }
    }
  };

  const scrollToTop = () => {
    window.scrollTo({ top: 0, behavior: 'smooth' });
  };
  const [tags, setTags] = useState([])
  useEffect(() => {
    getAllTags().then((res) => {
      const tags = res.data.data.docs;
      setTags(tags);
    });
  }, [])

  const router = useRouter()
  const handleTagClick = (tag: any) => {
    router.push(`/tag/${tag.name}`);
  };

  return (
    <div>
      {showButton && (
        <button className="scroll-top-button text-white" onClick={scrollToTop}>
          <ArrowTop />
        </button>
      )}
      <AppLoader isLoading={isLoading}></AppLoader>
      <div>
        <Categories />
      </div>
      <div className="only-desktop">
        <div className="large-slider-images">
          <MyCarousel bannerDetails={bannersSection1} />
        </div>
        <hr />
        <div>
          <h1>{t('Featured Category')}</h1>
        </div>
        <div className="category-items-grid my-3">

          {tags.map((tag: any, ix) => {
            return (
              <div
                className=
                "c-item text-center curser-pointer "
                key={ix}
                onClick={() => handleTagClick(tag)}
              >
                <img alt="c-item" src={tag.imageUrl} />
                <div>{tag.name}</div>
              </div>
            );
          })}

        </div>
        {
          topOffers.length > 0 &&
          <>
            <ProductsSlider products={topOffers} title={t('topOffers')} />
            <hr />
          </>
        }
        <div className="images-carousel-s">
          <MyCarousel bannerDetails={bannersSection2} />
        </div>
        
        <ProductsSlider products={newInSeason} title={t('New In Season')} />

        {/* <hr /> */}
        {/* <ProductsSlider products={categoriedProducts} title={t('Featured Products')} /> */}
        <br />
        <br />
      </div>
      <div className="only-mobile">
        <div className="large-slider-images only-mobile">
          <MyCarousel images={bannersSection1} />
        </div>
        <hr />

        <br />
        <div className="images-carousel-s">
          <MyCarousel images={bannersSection2} />
        </div>
        <hr />
        <ProductsSlider products={topOffers} title={t('Top Offers')} />

        <hr />
        <ProductsSlider products={newInSeason} title={t('New In Season')} />

        <hr />
        <ProductsSlider products={categoriedProducts} title={t('Featured Products')} />
        <br />
        <br />
      </div>
      <hr />
      <h1 className="px-3">{t('More Products')}</h1>
      <div className="grid-regular">
        {products && products.length && products.map((product: any, index: number) => {
          return <ProductItem product={product} key={index} />
        })}
      </div>
      <div className="primary p-3 d-flex justify-content-center">
        <button className="primary w-auto px-3" onClick={()=>loadMoreProducts()}>
          {t('Load more products')}
        </button>
      </div>
      <br />
    </div>
  );
}

export default HomePage;
