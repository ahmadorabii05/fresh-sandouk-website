import React from "react";
import kibsonsCareKid from "../public/images/kibsonsCareKid.jpg";
import sustain from "../public/images/sustain.jpg";

import Image from "next/image";


function FreshSandoukCare() {
  return (
    <div className="wrapper my-1">
      {/* <div className="kibsons-card">
        <Image
          className="w-100 full-image"
          src={kibsonsCareKid}
          alt={t("kibsonsCareKid")}
        />
        <br />
        <h1>{t("Kibsons Cares")}</h1>
        <div className="row">
          <div className="col-md-6">
            <p className="p2">
              {t(
                "Kibsons home delivery was established to make healthy food options conveniently available to UAE families at an affordable price. Our vision is to make ‘healthy eating easy’."
              )}
            </p>
            <p className="p2">
              {t(
                "More importantly, we also believe that our point of difference is ethical, sustainable and responsible sourcing."
              )}
            </p>
          </div>
          <div className="col-md-6">
            {t(
              "We strive to bring healthier, safer, clean-label and more eco-friendly products to the community and aspire to build a brand that prides itself on quality, transparency, sustainability and giving back to the community."
            )}
          </div>
        </div>
        <br />
      </div>
      <br />
      <br />
      <div className="kibsons-card">
        <Image
          className="w-100 full-image"
          src={sustain}
          alt={t("sustain")}
        />
        <br />
        <h1>{t("Sustainable Grocery Shopping")}</h1>
        <div className="row">
          <div className="col-md-6">
            <p className="p2">
              {t(
                "You asked, we heard: we are so thrilled to announce our sustainability initiatives that will proudly make Kibsons your choice for ‘Sustainable Grocery Shopping’."
              )}
            </p>
            <p className="p2">
              {t(
                "Water – Our new water recycling plant is on track to recycle more than 75% of the water we use to keep our demanding cold storage facility up and running."
              )}
            </p>
            <p className="p2">
              {t(
                "Electricity – Our solar panel project (spanning over our 130,000 sq mtr facility) is expected to save electricity consumption by more than 25% on average annually (and by more than 50% in winter months)."
              )}
            </p>
          </div>
          <div className="col-md-6">
            <p className="p2">
              {t(
                "Fuel – Our bio fuel additives for our refrigerated fleet are expected to improve our overall fuel efficiency (km/ltr) by more than 20% which will help to significantly decrease of carbon emissions."
              )}
            </p>
            <p className="p2">
              {t(
                "Plastic – More than 90% of all plastic at our facility is recycled."
              )}
            </p>
          </div>
        </div>
      </div>
      <br />
      <br /> */}
    </div>
  );
}

export default FreshSandoukCare;
