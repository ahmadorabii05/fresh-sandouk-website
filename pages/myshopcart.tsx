import React, { useEffect, useMemo, useState } from "react";
import Checkout from "./checkout";
import { useStripe, useElements, Elements } from "@stripe/react-stripe-js";
import { loadStripe } from "@stripe/stripe-js";
import { getStripeKeys, getStripeUrl } from "@/network/cart";
import AppLoader from "@/components/loader";

const MyShoppingcart = () => {
  // const [pubkey, setPubkey] = useState("");
  // const [clientSecret, setClientSecret] = useState("");
  // useEffect(() => {

  //   getStripeKeys()
  //     .then((res: any) => {
  //       const data = res.data.data
  //       const publishableKey = data.publishableKey
  //       const paymentIntent = data.paymentIntent
  //       setClientSecret(paymentIntent)
  //       setPubkey(publishableKey)
  //     })
  //     .catch((err) => {
  //       console.log({ err });
  //     });
  // },[]);
  // const stripePromise = loadStripe(pubkey);

  // const options = {
  //   // currency:'USD',
  //   // passing the client secret obtained from the server
  //   clientSecret,
  // };
  // if (!pubkey || !clientSecret) {
  //   return <AppLoader isLoading={true} />;
  // }
  return (
    // <Elements stripe={stripePromise} options={options}>
      <Checkout />
    /* </Elements> */
  );
};

export default MyShoppingcart;
