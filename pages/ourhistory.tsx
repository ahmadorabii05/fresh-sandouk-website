// src/pages/index.tsx

import React from "react";


function OurHistory() {
  return (
    <div className="wrapper pt-1">
      <br />
      <br />
      <h1>
        Fresh Sandouk has grown to be the trusted online food shop for over 10,000 customers in the UAE
      </h1>
      <p>
        Fresh Sandouk is the online division of KARIMAN FOODSTUFF TRADING LLC. a wholesaler for fruits and vegetables, that sources locally and globally to ensure the produce that reaches our customer is of highest quality, and in season.
      </p>
      <h1>
        How it All Started
      </h1>
      <p>
        Kariman Foodstuff was founded over a decade ago in 2008, as an importer and wholesaler of fruits and vegetables, always on the lookout for best products and suppliers on a global level. Over the years, we became the trusted partner to hotels, supermarkets, catering companies and online e-commerce platforms, always delivering the highest quality of produce on time.
      </p>
      <h1>
        Spreading the Love for “Food from Home”
      </h1>
      <p>
        Although we were sourcing globally, there was a focus towards Levantine products, which was due to the origin of our CEO and Managing Partner, Ahmad Hakimi. His culinary love for food from the Levantine region and its richness led him to this business in the first place.
        In 2019, he took the reins of the company and decided to broaden the outreach and bring the delicacies directly to the customer. It was also time to expand the variety of assortment and bring the same joy of “food from home” to other nationalities, aiming to cater to the diversity of the citizens of Dubai.
        Hence, early 2020, with the start of the pandemic, came the birth of the Fresh Sandouk online delivery service. Fresh Sandouk saw an exponential growth from its beginnings and within a short period of time expanded the team, the operations and the fleet, to cover demand all over the UAE.
        Early 2021, Fresh Sandouk was approached by major online market platforms such as Instashop deliveroo, talabat, el grocer, now now and Swan, to offer its assortment of fruits and vegetables. These successful partnerships allowed Fresh Sandouk to gain the trust of thousands of customers, which led the management to the decision to further expand and offer the full range of FMCG products.

        The skyrocketing growth of Fresh Sandouk has been a blessing and a challenge at the same time. It is testament to the hard work of the Fresh Sandouk team, a reward for their dedication, and at the same time it is a commitment to excellence towards the customers, as we aim to keep the standards of our product and services high to meet and even exceed the expectations of our valuable customers.
      </p>
      <h1>
        Turning a Passion into Mission
      </h1>
      <p>
        Fresh Sandouk’s mission is to spread joy through food and offer a carefully curated variety of authentic, in-season food, catering to the needs of an extremely diverse population.
        Our customer’s happiness is the beating heart of our success, and we are committed to keeping their trust at the center of our business.

        We thank you for your trust and are always curious to hear your feedback!
        If you would like to reach out to us, please contact us under sales@freshsandouk
      </p>
      <br />
      <br />
    </div>
  );
}

export default OurHistory;
