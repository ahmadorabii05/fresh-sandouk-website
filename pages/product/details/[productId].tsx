// src/pages/index.tsx

import ProductItem from "@/components/productItem";
import ProductsSlider from "@/components/productsSlider";
import { addToCart } from "@/store/reducers/cartReducer";
import React, { useState, useEffect } from "react";
import { useRouter } from "next/router";
import ReactImageMagnify from "react-image-magnify";

import { useSelector } from "react-redux";
import Image from "next/image";
import {
  getAllCategories,
  getAllProducts,
  getProductById,
} from "@/network/categories";
import { toast } from "react-toastify";
import prepareErrorResponse from "@/errorHandlerLayer/errorHandler";
import AppLoader from "@/components/loader";
import Categories from "@/components/categories";

function ProductDetails() {
  const router = useRouter();
  const [product, setProduct]: any = useState({});
  const { productId } = router.query;
  const [isLoading, setIsLoading] = useState(false);
  const [products, setProducts] = useState([]);

  useEffect(() => {
    setIsLoading(true);
    if (productId) {
      getProductById(productId)
        .then((res) => {
          const product = res.data.data;
          setProduct(product);
          getAllProducts({isActive : true , tag : product?.tags[0]?.name})
            .then((resp) => {
              setProducts(resp.data.data.docs);
              setIsLoading(false);
            })
            .catch((err) => {
              setIsLoading(false);
            });
        })
        .catch((err) => {
          setIsLoading(false);
          toast.error(prepareErrorResponse(err));
        });
    }
  }, [productId]);
  return (
    <div className="wrapper p-3">
      <Categories onlyLabels={true} />
      <div className="product-details-item">
        {product && product.name && (
          <div className="product-details-info-image">
            <ReactImageMagnify
              enlargedImageContainerDimensions={{ width: '200%', height: '200%' }}
              style={{ zIndex: 2 }}
              {...{
                smallImage: {
                  alt: "Wristwatch by Ted Baker London",
                  isFluidWidth: true,
                  src: product.imageUrls && product.imageUrls[0].imageUrl,
                },
                largeImage: {
                  src: product.imageUrls && product.imageUrls[0].imageUrl,
                  width: 2000,
                  height: 1500,
                },
              }}
            />
            <div className="tags img-tags">
              {product.tags &&
                product.tags.map((tag: any, index: number) => {
                  return (
                    <img className="tag-image" src={tag.imageUrl} key={index} />
                  );
                })}
            </div>
          </div>
        )}

        <div className="product-details-info-info  px-3">
          <ProductItem product={product} noImage={true} />
        </div>
      </div>
      {product && product.name && (
        <img
          className="img-small"
          src={product.imageUrls && product.imageUrls[0].imageUrl}
          alt="info"
        />
      )}
      <br />
      <br />
      {
        product.description &&
        <>
          <div className="description-area">
            <div className="description-tag">DESCRIPTION</div>
            <div className="my-2">{product.description}</div>
          </div>
          <br />
        </>
      }
      {
        product.ingredient &&
        <>
          <div className="description-area">
            <div className="description-tag">INGREDIENTS</div>
            <div className="my-2">{product.ingredient}</div>
          </div>
          <br />
        </>
      }
      {
        product.nutrition &&
        <>
          <div className="description-area">
            <div className="description-tag">NUTRITION</div>
            <div className="my-2">
              {product.nutrition &&
                product.nutrition.map((nut: any, index: number) => {
                  return (
                    <div className="nutrition-data py-3" key={index}>
                      {
                        nut.description &&
                        <>
                          <div className="description">
                            {nut.description}
                          </div><br />
                        </>
                      }
                      <div className="per-100g-ml">
                        Per 100g/ml: {nut.per_100g_ml}
                      </div>
                      <div className="per-serving">
                        Per Serving: {nut.per_serving}
                      </div>
                      <div className="measure">Measure: {nut.measure}</div>
                      <div className="nrv">NRV: {nut.nrv}</div>
                      {/* <div className="description-ar">
                    Description (Arabic): {nut.descriptionAr}
                  </div>
                  <div className="per-100g-ml-ar">
                    Per 100g/ml (Arabic): {nut.per_100g_mlAr}
                  </div>
                  <div className="per-serving-ar">
                    Per Serving (Arabic): {nut.per_servingAr}
                  </div>
                  <div className="measure-ar">
                    Measure (Arabic): {nut.measureAr}
                  </div>
                  <div className="nrv-ar">NRV (Arabic): {nut.nrvAr}</div> */}
                    </div>
                  );
                })}
            </div>
          </div>
          <br />
        </>
      }
      <section className="title-wrapper">
        <div className="line"></div>
        <div className="title">more in {product.name}</div>
        <div className="line"></div>
      </section>
      <br />
      <ProductsSlider products={products} title='' />
      <AppLoader isLoading={isLoading} />
      <br />
      <br />
      <br />
    </div>
  );
}

export default ProductDetails;
