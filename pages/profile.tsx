// src/pages/index.tsx

import React from 'react';
import MainLayout from '../layouts/MainLayout';

function Profile() {
  return (
    <MainLayout>
      <p>Profile Page</p>
    </MainLayout>
  );
}

export default Profile;
