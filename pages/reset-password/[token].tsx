import AppLoader from '@/components/loader';
import { changePassword, resetPassword } from '@/network/auth';
import React, { useState } from 'react'; // Import React and useState if not already imported
import Link  from 'next/link'; // Import Link from 'react-router-dom' if you are using React Router
import { useRouter } from 'next/router';
import { toast } from 'react-toastify';
import prepareErrorResponse from '@/errorHandlerLayer/errorHandler';

const ResetPasswordForm = () => {
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const [emailError, setEmailError] = useState('');
  const [passwordError, setPasswordError] = useState('');
  const [confirmPasswordError, setConfirmPasswordError] = useState('');
  const [formError, setFormError] = useState('');
  const [successMessage, setSuccessMessage] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const router = useRouter();
  const { token } = router.query;
  const handleSubmit = (e:any) => {
    e.preventDefault();
    setPasswordError('')
    setConfirmPasswordError('')
    setEmailError('')
    const userData = {
      password,
      sentToken: token,
    };
  
    if (validate()) {
      if (password !== confirmPassword) {
        setConfirmPasswordError("Passwords do not match");
      } else {
        setIsLoading(true);
        resetPassword(userData)
          .then((res) => {
            setIsLoading(false);
            toast.success('Your password has been successfully reset')
            router.push('/auth/login')
          })
          .catch((err) => {
            toast.error(prepareErrorResponse(err))
            setFormError(prepareErrorResponse(err));
            setIsLoading(false);
          });
      }
    }
  };
  

  const validate = () => {
    let isValid = true;
    if (!password) {
      setPasswordError("Password is required"); // Remove translate function
      isValid = false;
    }
    if (!confirmPassword) {
      setConfirmPasswordError("Confirm password is required"); // Remove translate function
      isValid = false;
    }
    return isValid;
  };

  return (
    <div className="my-3 modal-size">
      <h2 className="my-2">Reset password</h2> {/* Remove translate function */}
      <p className="text-center text-danger">{formError}</p> {/* Remove translate function */}
      {successMessage && (
        <p className="text-center text-success">{successMessage}</p>
      )}

      <form onSubmit={handleSubmit}>
        {/* <div className="input-label-text mt-2">Email</div>
        <input
          className="text-box"
          placeholder="Type Email"
          value={email}
          type='email'
          onChange={(e) => setEmail(e.target.value)}
        />
        <br />
        <div className="text-danger mt-2">{emailError}</div> */}
        <div className="input-label-text mt-2">New Password</div> {/* Remove translate function */}
        <input
          className="text-box"
          placeholder="Type New Password"
          value={password}
          type='password'

          onChange={(e) => setPassword(e.target.value)}
        />
        <br />
        <div className="text-danger mt-2">{passwordError}</div>
        <div className="input-label-text mt-2">Confirm New Password</div> {/* Remove translate function */}
        <input
          className="text-box"
          placeholder="Type New Password Again"
          value={confirmPassword}
          type='password'
          onChange={(e) => setConfirmPassword(e.target.value)}
        />
        <div className="text-danger">{confirmPasswordError}</div>
        <button className="btn-1 green" type="submit">
          SAVE NEW PASSWORD
        </button>
      </form>
      <hr />
      <div className="text-center">
        <Link href="/auth/login">Login</Link> {/* Use "to" instead of "href" for Link */}
      </div>

      <AppLoader isLoading={isLoading}></AppLoader>
    </div>
  );
};

export default ResetPasswordForm;
