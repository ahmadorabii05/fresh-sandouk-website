import React, { useState, useRef, useEffect } from "react";
import logo from "../public/icons/logo-mobile.png";
import deliveryImage from "../public/images/delivery.jpg";
import phoneWhite from "../public/icons/phoneWhite.png";
import Image from "next/image";
import { Collapse, Button } from "react-bootstrap";
import { useSelector } from "react-redux";
import RightArrow from "@/components/icons/rightArrow";
import { searchForProductsRequest } from "@/network/categories";
import { toast } from "react-toastify";
import prepareErrorResponse from "@/errorHandlerLayer/errorHandler";
import { useRouter } from "next/router";

import searchIcon from "../public/icons/search.svg";
import useTranslation from "next-translate/useTranslation";

function SearchPage() {
  const [foundItems, setFoundItems] = useState([]);
  const [showOverlay, setShowOverlay] = useState(false);
  const [searchQuery, setSearchQuery] = useState('');
  const router = useRouter();
  const {t} = useTranslation('common')

  function handleBlur() {
    setFoundItems([]);
    setShowOverlay(false);
  }

  const searchForItems = () => {
    searchForProductsRequest(searchQuery)
      .then((res) => {
        const docs = res.data.data.docs;
        setFoundItems(docs);
      })
      .catch((err) => {
        // toast.error(prepareErrorResponse(err));
      });
  };
  const handleSearchInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const query = e.target.value;
    setSearchQuery(query);
    searchForItems()
  };
  const handleItemClick = (e:any,item:any) => {
    setSearchQuery('')
      router.push('/product/details/'+ item.id)
  };

  function handleFocus() {
    setShowOverlay(true);
  }

  
  const handleSearchSubmit  = (e:any) =>{
    e.preventDefault()
    setFoundItems([]);
    setShowOverlay(false);
    setSearchQuery('');
    router.push('/category/shopping/search/query/' + searchQuery)

  }

  const highlightMatchingLetters = (itemName: any, searchQuery: any) => {
    const regex = new RegExp(searchQuery, "gi");
    return itemName.replace(
      regex,
      (match: any) => `<span style="color: #81C259">${match}</span>`
    );
  };

  return (
    <div className="wrapper search-page mt-4">
 <form className="input-wrapper mx-3" onSubmit={handleSearchSubmit}>

<input

  className="kibsons-input"
  placeholder={t("findProducts")}
  onFocus={handleFocus}
  onChange={handleSearchInputChange}
  onBlur={handleBlur}
  value={searchQuery}
  />
  
<button className="search-icon" type="submit">
  <Image alt="image" src={searchIcon} />
</button>
<div className="ui-list-searched">
  {foundItems.map((item: any) => {
    return (
      <div
      key={item.id}
      className="found text-muted fs-sm"
      // onClick={e=>handleItemClick(e,item)}
      onMouseDown={(e) => handleItemClick(e, item)}
      
      dangerouslySetInnerHTML={{
        __html: highlightMatchingLetters(item.name, searchQuery),
      }}
      />
    );
  })}
</div>
  </form>
    </div>
  );
}

export default SearchPage;
