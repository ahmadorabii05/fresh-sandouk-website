

import React, { useState, useEffect } from 'react';
import { useRouter } from 'next/router';
import ProductItem from '@/components/productItem';
import { useSelector } from 'react-redux';
import ProductsSlider from '@/components/productsSlider';
import Image from 'next/image'
import { getAllProducts, getTagById } from '@/network/categories';
import { toast, ToastContainer } from 'react-toastify';
import prepareErrorResponse from '@/errorHandlerLayer/errorHandler';
import AppLoader from '@/components/loader';

function Tag() {
  const router = useRouter();
  const [Tag, setTag]: any = useState({})
  const [products, setProducts]: any = useState([])
  const [isLoading, setIsLoading] = useState(true)

  const { tagName } = router.query;

  useEffect(() => {
    if (tagName) {
      setIsLoading(true)
      getAllProducts({ tag: tagName }).then((res: any) => {
        setProducts(res.data.data.docs)
        setIsLoading(false)
      })
    }
  }, [tagName])
  const featureds = [{
    name: 'Promotions',
    image: 'https://kibsons.com/assets/app/featured/round/en/638096443056887950_103.png'
  }, {
    name: 'New Arrivals',
    image: 'https://kibsons.com/assets/app/featured/round/en/637854415932307268_101.png'
  }, {
    name: 'Ramadan',
    image: 'https://kibsons.com/assets/app/featured/round/en/11801.png'
  }, {
    name: 'Vegan',
    image: 'https://kibsons.com/assets/app/featured/round/en/105.png'
  }]
  return (
    <div className='wrapper'>
      <AppLoader isLoading={isLoading}></AppLoader>
      <div className='only-desktop'>
      </div>
      {/* <img className='w-100 cat-image-main' alt="" src={Tag.imageUrl} /> */}
      <div className='pad-mobile'>
        <div className='title-Tag-page mb-2 d-flex v-center justify-content-between'>
          <h1>{Tag.name}</h1>
          {/* <button className='view-all'>View All</button> */}
        </div>
        <div className='Tag-items-grid'>
          {Tag && Tag.subtags && [...Tag.subtags].map((fr: any, index) => {
            return <div className='c-item text-center' onClick={() => router.push('/Tag/shopping/subTag/' + Tag.id + '/' + fr.id)} key={index}>
              <img src={fr.imageUrl} alt='fr image' />
              <div>{fr.name}</div>
            </div>
          })}
        </div>
        <hr />
        {/* <ProductsSlider products={products} title={'Top Offers'} /> */}
        {products && products.length ? <div className="grid-regular">
          {products &&
            products.length &&
            [...products].map((product, index) => {
              return <ProductItem product={product} key={index} />;
            })}
        </div> :
          <div className="text-center m-5">No matching results</div>
        }
        {/*   <hr />
      <h1>New Products</h1>
      <ProductsSlider products={products} />
      <hr />
      <h1>Featured Products</h1>
      <div className='Tag-items-grid'>
        {featureds.map((fr:any)=>{
          return <div className='c-item text-center' key={fr.name}>
          <img src={fr.image} alt='fr.image'  />
          <div>{fr.name}</div>
        </div>
        })}
      </div>
      <hr />
      <h1>Have you seen</h1>

      <ProductsSlider products={products} />

      <hr /> */}
        <br />
      </div>

    </div>
  );
}

export default Tag;
