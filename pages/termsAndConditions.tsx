// src/pages/index.tsx


function Terms() {
    return (
        <div className="p-3">
            <p>
                <strong><u>Terms and conditions of service</u></strong>
                <br />
                Please read this terms of service agreement carefully. By using this website or ordering products from this website you agree to be bound by all of the terms and conditions of this agreement.<br /><br />
                Welcome to www.freshSandouk.com (“<strong>site</strong>”). The owner and operator of the site is Fresh Sandouk owned by Kariman Foodstuff Trading LLC registered in the United Arab Emirates (“UAE”) , with its office located at store number 33, Ras al Khor Industrial 2, PO box 96120 Dubai in the UAE (“<strong>we</strong>”, “<strong>our</strong>” or “<strong>us</strong>”).
            </p>

            <p>
                <strong><u>Overview</u></strong>
                <br />
                Fresh Sandouk provides an online platform for stores selling FMCG goods such as food, beverages and other grocery products (collectively, for simplicity we say groceries). Fresh Sandouk offers this website, including all information, tools and services available from this site to you, the user, conditioned upon your acceptance of all terms, conditions, policies and notices stated here.<br /><br />
                These terms (terms of service) apply when you use the fresh Sandouk mobile applications or website (collectively, services). By using the services, you automatically agree to the terms of service, including those additional terms and conditions and policies referenced herein and/or available by hyperlink. These terms of service apply to all users of the site, including without limitation users who are browsers, vendors, customers, merchants, and/ or contributors of content. If you do not agree to all the terms and conditions of this agreement, then you may not access the website or use any services. If these terms of service are considered an offer, acceptance is expressly limited to these terms of service.
                Any new features or tools which are added to the current website and mobile application shall also be subject to the terms of service.<br /><br /> You can review the most current version of the terms of service at any time on this page. We reserve the right to update, change or replace any part of these terms of service by posting updates and/or changes to our website. It is your responsibility to check this page periodically for changes. Your continued use of or access to the website following the posting of any changes constitutes acceptance of those changes.

            </p>

            <p>
                <strong><u>CLAUSE1 -  ONLINE STORE TERMS</u></strong>
                <br />
                By agreeing to these terms of service www.freshSandouk.com wishes to ensure that its members can form legally binding contracts and further that minors do not purchase unsuitable content. Therefore, membership of the site is not available to persons under the age of 18 years. You represent that you are 18 years of age or over the age of 18 years before you become a member of the site.<br /><br />
                You may not use our products for any illegal or unauthorized purpose nor may you, in the use of the service, violate any laws in your jurisdiction (including but not limited to copyright laws).<br /><br />
                You must not transmit any worms or viruses or any code of a destructive nature.<br /><br />
                A breach or violation of any of the terms will result in an immediate termination of your services.

            </p>

            <p>
                <strong><u>CLAUSE2 -  ELIGIBILITY AND REGISTRATION REQUIREMENTS</u></strong>
                <br />
                2.1 you are eligible to register as a buyer or  and benefit from the services if you meet the following eligibility criteria:<br /><br />
                <strong>A. For buyers:</strong><br /><br />
                I. You are above the legal age for purchasing products in your country of residence; and<br /><br />
                Ii. You are able to provide an address in the UAEfor delivery of products.<br /><br />
                .<br /><br />
                2.2 in order to register to the site, you will need to provide us with certain information. Your registration to the site may not be accepted if you do not provide us with the required information. We reserve the right to decline any registration without further explanation. We reserve the right to undertake such checks as are necessary to verify your identity.<br /><br />
                2.3 You are the sole authorized User of any account (Account) you create using the Services. Your action of registration constitutes your acceptance of the Terms of Service and the Privacy Policy. Registration is free of charge. No entitlement exists for admission to the Service. Fresh Sandouk maintains the www.freshSandouk.com Website. The data required for registration provided by the User must be complete and accurate in order to make use of our services.<br /><br />
                2.4 The User is responsible for updating his/her own data that can be amended at any time from the respective interface of the Service. Fresh Sandouk has no responsibility over the use of the User account and expressly disclaims any liability therefrom.<br /><br />
                2.4 Should you suspect that any unauthorized party might be using your Account, you are obligated for your own security to notify  Fresh Sandouk immediately by emailing us at accounting@freshsandouk.com by providing your mobile phone number to Fresh Sandouk pursuant to the use of the Service, you hereby affirmatively consent to our use of your mobile phone number for contacting you directly in order to perform the Services, including but not limited to occasionally send notifications, text messages with promotional offers, service updates and reach out to you to survey regarding the quality of our services<br /><br />
                2.3 once you have successfully completed registration, your registration shall continue for an indefinite period, subject to suspension or termination in accordance with clause 17 of these terms of use.


            </p>

            <p>
                <strong><u>CLAUSE 3 - GENERAL CONDITIONS</u></strong>
                <br />
                3.1 We reserve the right to refuse service to anyone for any reason at any time.<br /><br />
                3.2 You understand that your content (not including credit card information), may be transferred unencrypted and involve (a) transmissions over various networks; and (b) changes to conform and adapt to technical requirements of connecting networks or devices. Credit card information is always encrypted during transfer over networks.<br /><br />
                3.3 You agree not to reproduce, duplicate, copy, sell, resell or exploit any portion of the Service, use of the Service, or access to the Service or any contact on the website through which the service is provided, without express written permission by us.<br /><br />
                3.4The headings used in this agreement are included for convenience only and will not limit or otherwise affect these Terms.

            </p>
            <p>
                <strong><u>CLAUSE 4 - ACCURACY, COMPLETENESS AND TIMELINESS OF INFORMATION</u></strong>
                <br />
                4.1 We are not responsible if information made available on this site is not accurate, complete or current. The material on this site is provided for general information only and should not be relied upon or used as the sole basis for making decisions without consulting primary, more accurate, more complete or timelier sources of information. Any reliance on the material on this site is at your own risk.<br /><br />
                4.2 This site may contain certain historical information, necessarily, is not current and is provided for your reference only. We reserve the right to modify the contents of this site at any time, but we have no obligation to update any information on our site. You agree that it is your responsibility to monitor changes to our site.


            </p>
            <p>
                <strong><u>CLAUSE5 - MODIFICATIONS TO THE SERVICE AND PRICES</u></strong>
                <br />
                5.1 Prices for our products are subject to change without notice.<br /><br />
                5.2 We reserve the right at any time to modify or discontinue the Service (or any part or content thereof) without notice at any time.<br /><br />
                5.3 We shall not be liable to you or to any third-party for any modification, price change, suspension or discontinuance of the Service.


            </p>
            <p>
                <strong><u>CLAUSE 6 - PRODUCTS OR SERVICES (if applicable)</u></strong>
                <br />
                6.1 Certain products or services may be available exclusively online through the website. These products or services may have limited quantities and are subject to return or exchange only according to our Return Policy.<br /><br />
                6.2 We have made every effort to display as accurately as possible the colors and images of our products that appear at the store. We cannot guarantee that your computer monitor display of any color will be accurate.<br /><br />
                6.3 We reserve the right, but are not obligated, to limit the sales of our products or Services to any person, geographic region or jurisdiction. We may exercise this right on a case-by-case basis. We reserve the right to limit the quantities of any products or services that we offer. All descriptions of products or product pricing are subject to change at any time without notice, at the sole discretion of us. We reserve the right to discontinue any product at any time. Any offer for any product or service made on this site is void where prohibited.<br /><br />
                6.4 We do not warrant that the quality of any products, services, information, or other material purchased or obtained by you will meet your expectations, or that any errors in the Service will be corrected.


            </p>
            <p>
                <strong><u>CLAUSE 7 - ACCURACY OF BILLING AND ACCOUNT INFORMATION</u></strong>
                <br />
                7.1 We reserve the right to refuse any order you place with us. We may, in our sole discretion, limit or cancel quantities purchased per person, per household or per order. These restrictions may include orders placed by or under the same customer account, the same credit card, and/or orders that use the same billing and/or shipping address. In the event that we make a change to or cancel an order, we may attempt to notify you by contacting the e-mail and/or billing address/phone number provided at the time the order was made. We reserve the right to limit or prohibit orders that, in our sole judgment, appear to be placed by dealers, resellers or distributors.<br /><br />
                7.2 We may, but have no obligation to, monitor, edit or remove content that we determine in our sole discretion are unlawful, offensive, threatening, libelous, defamatory, pornographic, obscene or otherwise objectionable or violates any party’s intellectual property or these Terms of Service.<br /><br />
                For more detail, please review our Returns Policy.<br /><br />
                7.3 You agree to provide current, complete and accurate purchase and account information for all purchases made at our store. You agree to promptly update your account and other information, including your email address and credit card numbers and expiration dates, so that we can complete your transactions and contact you as needed.


            </p>
            <p>
                <strong><u>CLAUSE 8 - OPTIONAL TOOLS</u></strong>
                <br />
                8.1 We may provide you with access to third-party tools over which we neither monitor nor have any controls nor input.<br /><br />
                8.2 You acknowledge and agree that we provide access to such tools ”(link for third party website)as is” and “as available” without any warranties, representations or conditions of any kind and without any endorsement. We shall have no liability whatsoever arising from or relating to your use of optional third-party tools.<br /><br />
                8.3 Any use by you of optional tools offered through the site is entirely at your own risk and discretion and you should ensure that you are familiar with and approve of the terms on which tools are provided by the relevant third-party provider(s).<br /><br />
                8.4 We may also, in the future, offer new services and/or features through the website (including, the release of new tools and resources). Such new features and/or services shall also be subject to these Terms of Service.


            </p>
            <p>
                <strong><u>CLAUSE 9 - THIRD-PARTY LINKS</u></strong>
                <br />
                9.1 Certain content, products and services available via our Service may include materials from third-parties.<br /><br />
                9.2 Third-party links on this site may direct you to third-party websites that are not affiliated with us. We are not responsible for examining or evaluating the content or accuracy and we do not warrant and will not have any liability or responsibility for any third-party materials or websites, or for any other materials, products, or services of third-parties.<br /><br />
                9.3 We are not liable for any harm or damages related to the purchase or use of goods, services, resources, content, or any other transactions made in connection with any third-party websites. Please review carefully the third-party policies and practices and make sure you understand them before you engage in any transaction. Complaints, claims, concerns, or questions regarding third-party products should be directed to the third-party.


            </p>
            <p>
                <strong><u>CLAUSE 10 - USER COMMENTS, FEEDBACK AND OTHER SUBMISSIONS</u></strong>
                <br />
                10.1 If, at our request, you send certain specific submissions (for example contest entries) or without a request from us you send creative ideas, suggestions, proposals, plans, or other materials, whether online, by email, by postal mail, or otherwise (collectively, comments), you agree that we may, at any time, without restriction, edit, copy, publish, distribute, translate and otherwise use in any medium any comments that you forward to us. We are and shall be under no obligation (1) to maintain any comments in confidence; (2) to pay compensation for any comments; or (3) to respond to any comments.<br /><br />
                10.2 You agree that your comments will not violate any right of any third-party, including copyright, trademark, privacy, personality or other personal or proprietary right. You further agree that your comments will not contain libelous or otherwise unlawful, abusive or obscene material, or contain any computer virus or other malware that could in any way affect the operation of the Service or any related website. You may not use a false e-mail address, pretend to be someone other than yourself, or otherwise mislead us or third-parties as to the origin of any comments. You are solely responsible for any comments you make and their accuracy. We take no responsibility and assume no liability for any comments posted by you or any third-party.


            </p>
            <p>
                <strong><u>CLAUSE11 - PERSONAL INFORMATION</u></strong>
                <br />
                11.1 Your submission of personal information through the store is governed by our Privacy Policy. To view our Privacy Policy.


            </p>
            <p>
                <strong><u>CLAUSE12 - ERRORS, INACCURACIES AND OMISSIONS</u></strong>
                <br />
                12.1 Occasionally there may be information on our site or in the Service that contains typographical errors, inaccuracies or omissions that may relate to product descriptions, pricing, promotions, offers, product shipping charges, transit times and availability. We reserve the right to correct any errors, inaccuracies or omissions, and to change or update information or cancel orders if any information in the Service or on any related website is inaccurate at any time without prior notice (including after you have submitted your order).<br /><br />
                12.2 We undertake no obligation to update, amend or clarify information in the Service or on any related website, including without limitation, pricing information, except as required by law. No specified update or refresh date applied in the Service or on any related website should be taken to indicate that all information in the Service or on any related website has been modified or updated.


            </p>
            <p>
                <strong><u>CLAUSE13 - PROHIBITED USES</u></strong>
                <br />
                13.1 In addition to other prohibitions as set forth in the Terms of Service, you are prohibited from using the site or its content:<br /><br />
                a. For any unlawful purpose<br /><br />
                b. To solicit others to perform or participate in any unlawful acts;<br /><br />
                c. To violate any international, federal, provincial or state regulations, rules, laws, or local ordinances;<br /><br />
                d.To infringe upon or violate our intellectual property rights or the intellectual property rights of others;<br /><br />
                e. to harass, abuse, insult, harm, defame, slander, disparage, intimidate, or discriminate based on gender, sexual orientation, religion, ethnicity, race, age, national origin, or disability;<br /><br />
                f. To submit false or misleading information;<br /><br />
                g.To upload or transmit viruses or any other type of malicious code that will or may be used in any way that will affect the functionality or operation of the Service or of any related website, other websites, or the Internet;<br /><br />
                h.To collect or track the personal information of others;<br /><br />
                i.To spam, phish, pharm, pretext, spider, crawl, or scrape;<br /><br />
                j.For any obscene or immoral purpose; or to interfere with or circumvent the security features of the Service or any related website, other websites, or the Internet.


            </p>
            <p>
                <strong><u>CLAUSE14 - DISCLAIMER OF WARRANTIES; LIMITATION OF LIABILITY</u></strong>
                <br />
                14.1 We do not guarantee, represent or warrant that your use of our service will be uninterrupted, timely, secure or error-free.<br /><br />
                14.2 We do not warrant that the results that may be obtained from the use of the service will be accurate or reliable.<br /><br />
                14.3 You agree that from time to time we may remove the service for indefinite periods of time or cancel the service at any time, without notice to you.<br /><br />
                14.4 You expressly agree that your use of, or inability to use, the service is at your sole risk. The service and all products and services delivered to you through the service are (except as expressly stated by us) provided as is and as available for your use, without any representation, warranties or conditions of any kind, either express or implied, including all implied warranties or conditions of merchantability, merchantable quality, fitness for a particular purpose, durability, title, and non-infringement.<br /><br />
                14.5 In no case shall www.freshSandouk.com, our directors, officers, employees, affiliates, agents, contractors, interns, suppliers, service providers or licensors be liable for any injury, loss, claim, or any direct, indirect, incidental, punitive, special, or consequential damages of any kind, including, without limitation lost profits, lost revenue, lost savings, loss of data, replacement costs, or any similar damages, whether based in contract, tort (including negligence), strict liability or otherwise, arising from your use of any of the service or any products procured using the service, or for any other claim related in any way to your use of the service or any product, including, but not limited to, any errors or omissions in any content, or any loss or damage of any kind incurred as a result of the use of the service or any content (or product) posted, transmitted, or otherwise made available via the service, even if advised of their possibility. Because some states or jurisdictions do not allow the exclusion or the limitation of liability for consequential or incidental damages, in such states or jurisdictions, our liability shall be limited to the maximum extent permitted by law.


            </p>
            <p>
                <strong><u>CLAUSE15 - INDEMNIFICATION</u></strong>
                <br />
                15.1 You agree to indemnify, defend and hold harmless www.freshSandouk.com and our parent, subsidiaries, affiliates, partners, officers, directors, agents, contractors, licensors, service providers, subcontractors, suppliers, interns and employees, harmless from any claim or demand, including reasonable attorneys’ fees, made by any third-party due to or arising out of your breach of these Terms of Service or the documents they incorporate by reference, or your violation of any law or the rights of a third-party.


            </p>
            <p>
                <strong><u>CLAUSE16 - SEVERABILITY</u></strong>
                <br />
                16.1 In the event that any provision of these Terms of Service is determined to be unlawful, void or unenforceable, such provision shall nonetheless be enforceable to the fullest extent permitted by applicable law, and the unenforceable portion shall be deemed to be severed from these Terms of Service, such determination shall not affect the validity and enforceability of any other remaining provisions.

            </p>
            <p>
                <strong><u>CLAUSE17 - TERMINATION</u></strong>
                <br />
                17.1 The obligations and liabilities of the parties incurred prior to the termination date shall survive the termination of this agreement for all purposes.
                17.2 These Terms of Service are effective unless and until terminated by either you or us. You may terminate these Terms of Service at any time by notifying us that you no longer wish to use our Services, or when you cease using our site.<br /><br />
                17.3 If in our sole judgment you fail, or we suspect that you have failed, to comply with any term or provision of these Terms of Service, we also may terminate this agreement at any time without notice and you will remain liable for all amounts due up to and including the date of termination; and/or accordingly may deny you access to our Services (or any part thereof).<br /><br />
                17.4 We may, at our sole discretion and without prejudice to any of our rights and remedies and without any liability to you, we may limit, suspend or permanently withdraw your access of our Services, cancel any product(s) order and/or remove hosted content submitted by you for any reason including, without limitation, the following:<br /><br />
                a. if we believe that you have infringed, breached, violated, abused, or unethically manipulated or exploited any term of these Terms of Use;<br /><br />
                b. If you use our Platform or Services for any unlawful and fraudulent purposes or in connection with a criminal offence or abuse our policy and rules;<br /><br />
                c. You are suspected of inventory abuse or placing bulk order. The following shall (without limitation) constitute inventory abuse or placing bulk order:<br /><br />
                - Products ordered are not for self-consumption but for commercial resale;<br /><br />
                - Multiple orders placed for same product at the same address, depending on the product category;<br /><br />
                - Bulk quantity of the same product ordered;<br /><br />
                - Invalid address given in order details;<br /><br />
                - Any abuse or fraud used to place the order; or<br /><br />
                - Any order placed using a technological glitch/loophole.<br /><br />
                17.5 We reserve the right to terminate your use of the Service or any related website for violating any of the prohibited uses.


            </p>
            <p>
                <strong><u>CLAUSE18 - ENTIRE AGREEMENT</u></strong>
                <br />
                18.1 The failure of us to exercise or enforce any right or provision of these Terms of Service shall not constitute a waiver of such right or provision.<br /><br />
                18.2 These Terms of Service and any policies or operating rules posted by us on this site or in respect to The Service constitutes the entire agreement and understanding between you and us and govern your use of the Service, superseding any prior or contemporaneous agreements, communications and proposals, whether oral or written, between you and us (including, but not limited to, any prior versions of the Terms of Service).<br /><br />
                18.3 Any ambiguities in the interpretation of these Terms of Service shall not be construed against the drafting party.


            </p>
            <p>
                <strong><u>CLAUSE19 - GOVERNING LAW</u></strong>
                <br />
                19.1 These Terms of Service and any separate agreements whereby we provide you Services shall be governed by and construed in accordance with the laws of United Arab Emirates.

            </p>
            <p>
                <strong><u>CLAUSE 20 - CHANGES TO TERMS OF SERVICE</u></strong>
                <br />
                20.1 You can review the most current version of the Terms of Service at any time at this page.<br /><br />
                20.2 We reserve the right, at our sole discretion, to update, change or replace any part of these Terms of Service by posting updates and changes to our website. It is your responsibility to check our website periodically for changes. Your continued use of or access to our website or the Service following the posting of any changes to these Terms of Service constitutes acceptance of those changes.


            </p>
        </div >
    );
}

export default Terms;
