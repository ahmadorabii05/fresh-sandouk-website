import prepareErrorResponse from "@/errorHandlerLayer/errorHandler";
import { addToCartPostRequest, decreaseQuantityFromCartPatchRequest, removeItemsFromCartPatchRequest } from "@/network/cart";
import { toast } from "react-toastify";
import Cookies from "js-cookie";

interface CartItem {
  id: string;
  quantityInCart: number;
  createdAt: string;
  updatedAt: string;
  name: string;
  imageUrl: string;
  country: string;
  price: number;
  priceModel: string;
  description: string;
  discountValue: string;
  isDiscount: boolean;
  tags: string[];
  generalDescription: string;
  ingredient: string;
  storage: string;
  nutrition: any[]; // Update this with the appropriate type for nutrition
  finalPriceAfterDiscount: string;
}

interface CartState {
  cartItems: CartItem[];
}

const initialState: CartState = {
  cartItems: [],
};

export const addToCart = (item: CartItem) => (dispatch: any, getState: any) => {
  dispatch({
    type: "ADD_TO_CART",
    payload: item,
  });

  if (!Cookies.get("token")) {
    const cartData = getState().cart.cartItems;
    localStorage.setItem("cartItems", JSON.stringify(cartData));
  } else {
    const cartData = getState().cart.cartItems;
    requestAddToCart(cartData, false);
  }
};

export const removeFromCart = (item: CartItem) => (dispatch: any, getState: any) => {
  dispatch({
    type: "REMOVE_FROM_CART",
    payload: item,
  });

  if (!Cookies.get("token")) {
    const updatedCartItems = getState().cart.cartItems;
    localStorage.setItem("cartItems", JSON.stringify(updatedCartItems));
  } else {
    // const updatedCartItems = getState().cart.cartItems;
    requestAddToCart([item], true);
  }
};

export const decreaseCartItemQuantity = (item: CartItem) => (dispatch: any, getState: any) => {
  const existingCartItem = getState().cart.cartItems.find((cartItem: CartItem) => cartItem.id === item.id);

  if (existingCartItem && existingCartItem.quantityInCart > 1) {
    dispatch({
      type: "DECREASE_CART_ITEM_QUANTITY",
      payload: item,
    });
    if (!Cookies.get("token")) {
      localStorage.setItem("cartItems", JSON.stringify(getState().cart.cartItems));
    } else {
      decreaseQuantityFromCartPatchRequest({ productId: item.id });
    }
  } else {
    dispatch({
      type: "REMOVE_FROM_CART",
      payload: item,
    });
    if (!Cookies.get("token")) {
      localStorage.setItem("cartItems", JSON.stringify(getState().cart.cartItems));
    } else {
      removeItemsFromCartPatchRequest({ productIds: [item.id] });
    }
  }
};

export const setCartItems = (items: CartItem[]) => (dispatch: any) => {
  dispatch({
    type: "SET_CART_ITEMS",
    payload: items,
  });
};

const requestAddToCart = (cartItems: CartItem[], isDeleting: boolean) => {
  var productIds = cartItems.map((item) => ({
    productId: item.id,
    quantity: item.quantityInCart,
  }));
  productIds = productIds.filter((item) => { return item.productId != undefined })
  if (isDeleting) {
    removeItemsFromCartPatchRequest({ productIds: productIds.map((item) => { return item.productId }) })
      .then((res) => { })
      .catch((err) => {
        toast.error(prepareErrorResponse(err));
      });
  } else {
    addToCartPostRequest({ productIds })
      .then((res) => { })
      .catch((err) => {
        toast.error(prepareErrorResponse(err));
      });
  }
};

type CartAction =
  | { type: "ADD_TO_CART"; payload: CartItem }
  | { type: "REMOVE_FROM_CART"; payload: CartItem }
  | { type: "DECREASE_CART_ITEM_QUANTITY"; payload: CartItem }
  | { type: "SET_CART_ITEMS"; payload: CartItem[] };

const cartReducer = (state = initialState, action: CartAction): CartState => {
  switch (action.type) {
    case "ADD_TO_CART": {
      const { payload } = action;
      const existingCartItem = state.cartItems.find((item) => item.id === payload.id);

      const updatedCartItems = existingCartItem
        ? state.cartItems.map((item) =>
          item.id === payload.id ? { ...item, quantityInCart: item.quantityInCart + 1 } : item
        )
        : [...state.cartItems, { ...payload, quantityInCart: 1 }];

      return {
        ...state,
        cartItems: updatedCartItems,
      };
    }
    case "REMOVE_FROM_CART":
      return {
        ...state,
        cartItems: state.cartItems.filter((item) => item.id !== action.payload.id),
      };
    case "DECREASE_CART_ITEM_QUANTITY": {
      const { payload } = action;
      const existingCartItem = state.cartItems.find((item) => item.id === payload.id);

      if (existingCartItem && existingCartItem.quantityInCart > 1) {
        const updatedCartItems = state.cartItems.map((item) =>
          item.id === payload.id ? { ...item, quantityInCart: item.quantityInCart - 1 } : item
        );

        return {
          ...state,
          cartItems: updatedCartItems,
        };
      } else {
        return {
          ...state,
          cartItems: state.cartItems.filter((item) => item.id !== payload.id),
        };
      }
    }
    case "SET_CART_ITEMS":
      return {
        ...state,
        cartItems: action.payload,
      };
    default:
      return state;
  }
};

export default cartReducer;
