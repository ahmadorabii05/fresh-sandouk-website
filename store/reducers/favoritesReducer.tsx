import { updateMyFavorites } from "@/network/categories";
import { toggleFavoritePostRequest } from "@/network/favorites";
import Cookies from "js-cookie";

interface Item {
  id: string;
}

interface FavoritesState {
  favorites: Item[];
  shoppingList: Item[];
}

// const initialState: FavoritesState = {
//   favorites: typeof localStorage !== 'undefined' && localStorage.getItem('favorites')
//     ? JSON.parse(localStorage.getItem('favorites')!)
//     : [],
//   shoppingList: typeof localStorage !== 'undefined' && localStorage.getItem('shoppingList')
//     ? JSON.parse(localStorage.getItem('shoppingList')!)
//     : [],
// };


const initialState: FavoritesState = {
  favorites: [],
  shoppingList: []
};
export const toggleShoppingList = (item: Item) => (dispatch: any, getState: any) => {
  dispatch({
    type: 'TOGGLE_SHOPPING_LIST',
    payload: item,
  });

  if (!Cookies.get('token')) {
    const shoppingList = getState().favorites.shoppingList;
    localStorage.setItem('shoppingList', JSON.stringify(shoppingList));
  }
};

export const setShoppingList = (items: Item[]) => (dispatch: any) => {
  dispatch({
    type: 'SET_SHOPPING_LIST',
    payload: items,
  });
};

export const toggleFavoriteItem = (item: Item) => (dispatch: any, getState: any) => {4
  let called = 0
  dispatch({
    type: 'TOGGLE_FAVORITE',
    payload: item,
  });

  if (Cookies.get('token')) {
    const productIds = [item.id];
      toggleFavoritePostRequest({ productIds });
  } else {
    const favorites = getState().favorites.favorites;
    localStorage.setItem('favorites', JSON.stringify(favorites));
  }
};

export const setFavoritesItems = (items: Item[]) => (dispatch: any) => {
  dispatch({
    type: 'SET_FAVORITES_ITEMS',
    payload: items,
  });
};

const favoritesReducer = (state = initialState, action: any): FavoritesState => {
  switch (action.type) {
    case 'TOGGLE_SHOPPING_LIST':
      const Sindex = state.shoppingList.findIndex(
        (shoppingItem: Item) => shoppingItem.id === action.payload.id
      );
      if (Sindex === -1) {
        // Item is not in shoppingList, so add it
        return {
          ...state,
          shoppingList: [...state.shoppingList, action.payload],
        };
      } else {
        // Item is already in shoppingList, so remove it
        const newShoppingList = [...state.shoppingList];
        newShoppingList.splice(Sindex, 1);
        return {
          ...state,
          shoppingList: newShoppingList,
        };
      }
    case 'SET_SHOPPING_LIST':
      return {
        ...state,
        shoppingList: action.payload,
      };
    case 'TOGGLE_FAVORITE':
      const index = state.favorites.findIndex(
        (favorite: Item) => favorite.id === action.payload.id
      );
      let newFavorites: Item[] = [...state.favorites];
      if (index === -1) {
        // Item is not in favorites, so add it
        newFavorites.push(action.payload);
      } else {
        // Item is already in favorites, so remove it
        newFavorites.splice(index, 1);
      }
      const newFavoriteIds = newFavorites.map((favorite: Item) => favorite.id);
      if (Cookies.get('token')) {
        // updateMyFavorites(newFavoriteIds);
      } else {
        localStorage.setItem('favorites', JSON.stringify(newFavorites));
      }
      return {
        ...state,
        favorites: newFavorites,
      };
    case 'SET_FAVORITES_ITEMS':
      return {
        ...state,
        favorites: action.payload,
      };
    default:
      return state;
  }
};

export default favoritesReducer;
