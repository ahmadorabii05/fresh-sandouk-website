import {faker} from '@faker-js/faker';


const initialState: any = {
    categories: [{
        id: "c1",
        name: "Quick Meal",
        color: "red",
        imgMobile: "https://kibsons.com/assets/app/category/round/en/637861364754770566_L.png",
        img: "https://kibsons.com/assets/app/category/square/en/637861364754770566_L.png",
    },
        {
            id: "c2",
            name: "Fruits",
            color: "yellow",
            imgMobile: "https://kibsons.com/assets/app/category/round/en/637861364754770566_L.png",
            img: "https://kibsons.com/assets/app/category/square/en/637861385511922112_F.png",
            collections: [
                {
                    id: "cl1",
                    name: "New Arrivals",
                    imgMobile: "https://kibsons.com/assets/app/category/round/en/637861364754770566_L.png",
                    img: "https://kibsons.com/assets/app/featured/blue/en/637854415932307268_101.png",
                },
                {
                    id: "cl2",
                    name: "Ramadan 20%",
                    imgMobile: "https://kibsons.com/assets/app/category/round/en/637861364754770566_L.png",
                    img: "https://kibsons.com/assets/app/featured/blue/en/638139551232314053_143.png",
                },
                {
                    id: "cl3",
                    name: "Ramadan",
                    imgMobile: "https://kibsons.com/assets/app/category/round/en/637861364754770566_L.png",
                    img: "https://kibsons.com/assets/app/featured/blue/en/11801.png",
                },

                {
                    id: "cl4",
                    name: "Promotion",
                    imgMobile: "https://kibsons.com/assets/app/category/round/en/637861364754770566_L.png",
                    img: "https://kibsons.com/assets/app/featured/blue/en/638096443056887950_103.png",
                },
                {
                    id: "cl4",
                    name: "Imperefect",
                    imgMobile: "https://kibsons.com/assets/app/category/round/en/637861364754770566_L.png",
                    img: "https://kibsons.com/assets/app/featured/blue/en/107.png",
                },
                {
                    id: "cl4",
                    name: "BBQ",
                    imgMobile: "https://kibsons.com/assets/app/category/round/en/637861364754770566_L.png",
                    img: "https://kibsons.com/assets/app/featured/blue/en/115.png",
                },
                {
                    id: "cl4",
                    name: "Back In Stock",
                    imgMobile: "https://kibsons.com/assets/app/category/round/en/637861364754770566_L.png",
                    img: "https://kibsons.com/assets/app/featured/blue/en/125.png",
                },
                {
                    id: "cl4",
                    name: "Organic",
                    imgMobile: "https://kibsons.com/assets/app/category/round/en/637861364754770566_L.png",
                    img: "https://kibsons.com/assets/app/featured/blue/en/106.png",
                },
                {
                    id: "cl4",
                    name: "Vegan",
                    imgMobile: "https://kibsons.com/assets/app/category/round/en/637861364754770566_L.png",
                    img: "https://kibsons.com/assets/app/featured/blue/en/105.png",
                },
            ],
            items: [{
                id: "ar",
                name: "Apples & pears",
                imgMobile: "https://kibsecomimgstore.blob.core.windows.net/products/family/637892746157571060_102.png",
                img: "https://kibsecomimgstore.blob.core.windows.net/products/family/637892746157571060_102.png",
            },
                {
                    id: "ar",
                    name: "Avocados",
                    imgMobile: "https://kibsecomimgstore.blob.core.windows.net/products/family/637892746665514411_105.png",
                    img: "https://kibsecomimgstore.blob.core.windows.net/products/family/637892746665514411_105.png",
                },
                {
                    id: "ar",
                    name: "Bananas",
                    imgMobile: "https://kibsecomimgstore.blob.core.windows.net/products/family/637892747856314851_106.png",
                    img: "https://kibsecomimgstore.blob.core.windows.net/products/family/637892747856314851_106.png",
                }, {
                    id: "ar",
                    name: "Artichoke",
                    imgMobile: "https://media.istockphoto.com/id/636739634/photo/banana.jpg?s=612x612&w=0&k=20&c=pO0985tQi1LpWRlWqpRvbab8S5yxgnEOVcs5CHIlcDE=",
                    img: "https://pngimg.com/uploads/artichoke/artichoke_PNG22.png",
                },]
        },
        {
            id: "c3",
            name: "Vegetables",
            color: "green",
            imgMobile: "https://kibsons.com/assets/app/category/round/en/637861364754770566_L.png",
            img: "https://kibsons.com/assets/app/category/square/en/637861386523154565_V.png",
            items: [
                {
                    id: "ar",
                    name: "Artichokes",
                    imgMobile: "https://kibsons.com/assets/app/category/round/en/637861364754770566_L.png",
                    img: "https://pngimg.com/uploads/artichoke/artichoke_PNG22.png",
                },
                {
                    id: "ar",
                    name: "Artichokes",
                    imgMobile: "https://kibsons.com/assets/app/category/round/en/637861364754770566_L.png",
                    img: "https://pngimg.com/uploads/artichoke/artichoke_PNG22.png",
                },
                {
                    id: "ar",
                    name: "Artichokes",
                    imgMobile: "https://kibsons.com/assets/app/category/round/en/637861364754770566_L.png",
                    img: "https://pngimg.com/uploads/artichoke/artichoke_PNG22.png",
                },
                {
                    id: "ar",
                    name: "Artichokes",
                    imgMobile: "https://kibsons.com/assets/app/category/round/en/637861364754770566_L.png",
                    img: "https://pngimg.com/uploads/artichoke/artichoke_PNG22.png",
                },
                {
                    id: "ar",
                    name: "Artichokes",
                    imgMobile: "https://kibsons.com/assets/app/category/round/en/637861364754770566_L.png",
                    img: "https://pngimg.com/uploads/artichoke/artichoke_PNG22.png",
                },
                {
                    id: "ar",
                    name: "Artichokes",
                    imgMobile: "https://kibsons.com/assets/app/category/round/en/637861364754770566_L.png",
                    img: "https://pngimg.com/uploads/artichoke/artichoke_PNG22.png",
                },
                {
                    id: "ar",
                    name: "Artichokes",
                    imgMobile: "https://kibsons.com/assets/app/category/round/en/637861364754770566_L.png",
                    img: "https://pngimg.com/uploads/artichoke/artichoke_PNG22.png",
                },
                {
                    id: "ar",
                    name: "Artichokes",
                    imgMobile: "https://kibsons.com/assets/app/category/round/en/637861364754770566_L.png",
                    img: "https://pngimg.com/uploads/artichoke/artichoke_PNG22.png",
                },
                {
                    id: "ar",
                    name: "Artichokes",
                    imgMobile: "https://kibsons.com/assets/app/category/round/en/637861364754770566_L.png",
                    img: "https://pngimg.com/uploads/artichoke/artichoke_PNG22.png",
                },
                {
                    id: "ar",
                    name: "Artichokes",
                    imgMobile: "https://kibsons.com/assets/app/category/round/en/637861364754770566_L.png",
                    img: "https://pngimg.com/uploads/artichoke/artichoke_PNG22.png",
                },
                {
                    id: "ar",
                    name: "Artichokes",
                    imgMobile: "https://kibsons.com/assets/app/category/round/en/637861364754770566_L.png",
                    img: "https://pngimg.com/uploads/artichoke/artichoke_PNG22.png",
                },
                {
                    id: "ar",
                    name: "Artichokes",
                    imgMobile: "https://kibsons.com/assets/app/category/round/en/637861364754770566_L.png",
                    img: "https://pngimg.com/uploads/artichoke/artichoke_PNG22.png",
                },
                {
                    id: "ar",
                    name: "Artichokes",
                    imgMobile: "https://kibsons.com/assets/app/category/round/en/637861364754770566_L.png",
                    img: "https://pngimg.com/uploads/artichoke/artichoke_PNG22.png",
                },
                {
                    id: "ar",
                    name: "Artichokes",
                    imgMobile: "https://kibsons.com/assets/app/category/round/en/637861364754770566_L.png",
                    img: "https://pngimg.com/uploads/artichoke/artichoke_PNG22.png",
                },
                {
                    id: "ar",
                    name: "Artichokes",
                    imgMobile: "https://kibsons.com/assets/app/category/round/en/637861364754770566_L.png",
                    img: "https://pngimg.com/uploads/artichoke/artichoke_PNG22.png",
                },
                {
                    id: "ar",
                    name: "Artichokes",
                    imgMobile: "https://kibsons.com/assets/app/category/round/en/637861364754770566_L.png",
                    img: "https://pngimg.com/uploads/artichoke/artichoke_PNG22.png",
                },
            ],
            collections: [
                {
                    id: "cl1",
                    name: "New Arrivals",
                    imgMobile: "https://kibsons.com/assets/app/category/round/en/637861364754770566_L.png",
                    img: "https://kibsons.com/assets/app/featured/blue/en/637854415932307268_101.png",
                },
                {
                    id: "cl2",
                    name: "Ramadan 20%",
                    imgMobile: "https://kibsons.com/assets/app/category/round/en/637861364754770566_L.png",
                    img: "https://kibsons.com/assets/app/featured/blue/en/638139551232314053_143.png",
                },
                {
                    id: "cl3",
                    name: "Ramadan",
                    imgMobile: "https://kibsons.com/assets/app/category/round/en/637861364754770566_L.png",
                    img: "https://kibsons.com/assets/app/featured/blue/en/11801.png",
                },

                {
                    id: "cl4",
                    name: "Promotion",
                    imgMobile: "https://kibsons.com/assets/app/category/round/en/637861364754770566_L.png",
                    img: "https://kibsons.com/assets/app/featured/blue/en/638096443056887950_103.png",
                },
                {
                    id: "cl4",
                    name: "Imperefect",
                    imgMobile: "https://kibsons.com/assets/app/category/round/en/637861364754770566_L.png",
                    img: "https://kibsons.com/assets/app/featured/blue/en/107.png",
                },
                {
                    id: "cl4",
                    name: "BBQ",
                    imgMobile: "https://kibsons.com/assets/app/category/round/en/637861364754770566_L.png",
                    img: "https://kibsons.com/assets/app/featured/blue/en/115.png",
                },
                {
                    id: "cl4",
                    name: "Back In Stock",
                    imgMobile: "https://kibsons.com/assets/app/category/round/en/637861364754770566_L.png",
                    img: "https://kibsons.com/assets/app/featured/blue/en/125.png",
                },
                {
                    id: "cl4",
                    name: "Organic",
                    imgMobile: "https://kibsons.com/assets/app/category/round/en/637861364754770566_L.png",
                    img: "https://kibsons.com/assets/app/featured/blue/en/106.png",
                },
                {
                    id: "cl4",
                    name: "Vegan",
                    imgMobile: "https://kibsons.com/assets/app/category/round/en/637861364754770566_L.png",
                    img: "https://kibsons.com/assets/app/featured/blue/en/105.png",
                },
            ],
        },
        {
            id: "c1",
            name: "Quick Meal",
            imgMobile: "https://kibsons.com/assets/app/category/round/en/637861364754770566_L.png",
            img: "https://kibsons.com/assets/app/category/square/en/637861364754770566_L.png",
        },
        {
            id: "c1",
            name: "Quick Meal",
            imgMobile: "https://kibsons.com/assets/app/category/round/en/637861364754770566_L.png",
            img: "https://kibsons.com/assets/app/category/square/en/637861364754770566_L.png",
        },
        {
            id: "c1",
            name: "Quick Meal",
            imgMobile: "https://kibsons.com/assets/app/category/round/en/637861364754770566_L.png",
            img: "https://kibsons.com/assets/app/category/square/en/637861364754770566_L.png",
        },
        {
            id: "c1",
            name: "Quick Meal",
            imgMobile: "https://kibsons.com/assets/app/category/round/en/637861364754770566_L.png",
            img: "https://kibsons.com/assets/app/category/square/en/637861364754770566_L.png",
        },
        {
            id: "c1",
            name: "Quick Meal",
            imgMobile: "https://kibsons.com/assets/app/category/round/en/637861364754770566_L.png",
            img: "https://kibsons.com/assets/app/category/square/en/637861364754770566_L.png",
        },
        {
            id: "c1",
            name: "Quick Meal",
            imgMobile: "https://kibsons.com/assets/app/category/round/en/637861364754770566_L.png",
            img: "https://kibsons.com/assets/app/category/square/en/637861364754770566_L.png",
        },
        {
            id: "c1",
            name: "Quick Meal",
            imgMobile: "https://kibsons.com/assets/app/category/round/en/637861364754770566_L.png",
            img: "https://kibsons.com/assets/app/category/square/en/637861364754770566_L.png",
        },
        {
            id: "c1",
            name: "Quick Meal",
            imgMobile: "https://kibsons.com/assets/app/category/round/en/637861364754770566_L.png",
            img: "https://kibsons.com/assets/app/category/square/en/637861364754770566_L.png",
        },
        {
            id: "c1",
            name: "Quick Meal",
            imgMobile: "https://kibsons.com/assets/app/category/round/en/637861364754770566_L.png",
            img: "https://kibsons.com/assets/app/category/square/en/637861364754770566_L.png",
        },
        {
            id: "c1",
            name: "Quick Meal",
            imgMobile: "https://kibsons.com/assets/app/category/round/en/637861364754770566_L.png",
            img: "https://kibsons.com/assets/app/category/square/en/637861364754770566_L.png",
        },
        {
            id: "c1",
            name: "Quick Meal",
            imgMobile: "https://kibsons.com/assets/app/category/round/en/637861364754770566_L.png",
            img: "https://kibsons.com/assets/app/category/square/en/637861364754770566_L.png",
        },
    ],
    mainSliderImagesDesktop: [
        "https://freshsandouk.com/cdn/shop/files/b2.jpg?v=1649857701",
        "https://freshsandouk.com/cdn/shop/files/b3.jpg?v=1649857701",
        "https://freshsandouk.com/cdn/shop/files/b4.jpg?v=1649857701",
    ],
    mainSliderImagesMobile: [
        "https://freshsandouk.com/cdn/shop/files/b2.jpg?v=1649857701",
        "https://freshsandouk.com/cdn/shop/files/b3.jpg?v=1649857701",
    ],
    offerImages: [
        "https://freshsandouk.com/cdn/shop/files/b2.jpg?v=1649857701",
        "https://freshsandouk.com/cdn/shop/files/b3.jpg?v=1649857701",
    ],
    brands: [],
    products: [
        {
            name: "Strawuberries",
            brand: "Brand 1",
            nationality: "Greece",
            size: "Small",
            id: 661,
            image: "https://kibsecomimgstore.blob.core.windows.net/products/display/HPL_STRXXGRXX1KGA1_20230322102652.jpg",
            isOffer: true,
            isNew: false,
            isAvailable: true,
            price: 20.99,
            unit: 'Tray',
            discountedPrice: 15.99
        },
        {
            name: "Formosa papaya",
            brand: "Brand 2",
            nationality: "Country 2",
            size: "Medium",
            id: 26,
            image: "https://kibsecomimgstore.blob.core.windows.net/products/display/HPL_PAPXXBRXXKGSA1_20230322084150.jpg",
            isOffer: false,
            isNew: true,
            isAvailable: false,
            price: 15.50,
            unit: 'cartoon',
            discountedPrice: 12.22
        },
        {
            name: "Product 3",
            brand: "Brand 1",
            nationality: "Country 3",
            size: "Large",
            id: 63,
            image: "https://kibsecomimgstore.blob.core.windows.net/products/display/HPL_MGOALINXX03KL1_20230324205623.jpg",
            isOffer: false,
            isNew: false,
            isAvailable: true,
            price: 30.00,
            unit: 'Piece',
            unitDescription: '1.1 kg to 1.65 kg per piece',
            discountedPrice: 14.35
        },
        {
            name: "Strawuberries",
            brand: "Brand 1",
            nationality: "Greece",
            size: "Small",
            id: 7671,
            image: "https://kibsecomimgstore.blob.core.windows.net/products/display/HPL_STRXXGRXX1KGA1_20230322102652.jpg",
            isOffer: true,
            isNew: false,
            isAvailable: true,
            price: 20.99,
            unit: 'Tray',
            discountedPrice: 15.99
        },
        {
            name: "Product 3",
            brand: "Brand 1",
            nationality: "Country 3",
            size: "Large",
            id: 31217,
            image: "https://kibsecomimgstore.blob.core.windows.net/products/display/HPL_MGOALINXX03KL1_20230324205623.jpg",
            isOffer: false,
            isNew: false,
            isAvailable: true,
            price: 30.00,
            unit: 'Piece',
            unitDescription: '1.1 kg to 1.65 kg per piece',
            discountedPrice: 14.35
        },
        {
            name: "Formosa papaya",
            brand: "Brand 2",
            nationality: "Country 2",
            size: "Medium",
            id: 2221122,
            image: "https://kibsecomimgstore.blob.core.windows.net/products/display/HPL_PAPXXBRXXKGSA1_20230322084150.jpg",
            isOffer: false,
            isNew: true,
            isAvailable: false,
            price: 15.50,
            unit: 'cartoon',
            discountedPrice: 12.22
        },
        {
            name: "Strawuberries",
            brand: "Brand 1",
            nationality: "Greece",
            size: "Small",
            id: 132312,
            image: "https://kibsecomimgstore.blob.core.windows.net/products/display/HPL_STRXXGRXX1KGA1_20230322102652.jpg",
            isOffer: true,
            isNew: false,
            isAvailable: true,
            price: 20.99,
            unit: 'Tray',
            discountedPrice: 15.99
        },
        {
            name: "Product 3",
            brand: "Brand 1",
            nationality: "Country 3",
            size: "Large",
            id: 113,
            image: "https://kibsecomimgstore.blob.core.windows.net/products/display/HPL_MGOALINXX03KL1_20230324205623.jpg",
            isOffer: false,
            isNew: false,
            isAvailable: true,
            price: 30.00,
            unit: 'Piece',
            unitDescription: '1.1 kg to 1.65 kg per piece',
            discountedPrice: 14.35
        },
        {
            name: "Formosa papaya",
            brand: "Brand 2",
            nationality: "Country 2",
            size: "Medium",
            id: 212,
            image: "https://kibsecomimgstore.blob.core.windows.net/products/display/HPL_PAPXXBRXXKGSA1_20230322084150.jpg",
            isOffer: false,
            isNew: true,
            isAvailable: false,
            price: 15.50,
            unit: 'cartoon',
            discountedPrice: 12.22
        },
        {
            name: "Strawuberries",
            brand: "Brand 1",
            nationality: "Greece",
            size: "Small",
            id: 313121,
            image: "https://kibsecomimgstore.blob.core.windows.net/products/display/HPL_STRXXGRXX1KGA1_20230322102652.jpg",
            isOffer: true,
            isNew: false,
            isAvailable: true,
            price: 20.99,
            unit: 'Tray',
            discountedPrice: 15.99
        },
        {
            name: "Product 3",
            brand: "Brand 1",
            nationality: "Country 3",
            size: "Large",
            id: 2213,
            image: "https://kibsecomimgstore.blob.core.windows.net/products/display/HPL_MGOALINXX03KL1_20230324205623.jpg",
            isOffer: false,
            isNew: false,
            isAvailable: true,
            price: 30.00,
            unit: 'Piece',
            unitDescription: '1.1 kg to 1.65 kg per piece',
            discountedPrice: 14.35
        },
        {
            name: "Formosa papaya",
            brand: "Brand 2",
            nationality: "Country 2",
            size: "Medium",
            id: 232,
            image: "https://kibsecomimgstore.blob.core.windows.net/products/display/HPL_PAPXXBRXXKGSA1_20230322084150.jpg",
            isOffer: false,
            isNew: true,
            isAvailable: false,
            price: 15.50,
            unit: 'cartoon',
            discountedPrice: 12.22
        },
    ]
};

export const setProducts = (products: any): any => (dispatch: any, getState: any) => {
    dispatch({
        type: 'SET_PRODUCTS',
        payload: products
    });

};

for (let i = 1; i <= 115; i++) {
    const name = faker.commerce.productName();
    const image = 'https://kibsecomimgstore.blob.core.windows.net/products/brand/638138670761314451_2250.png'
    initialState.brands.push({id: i, name, image});
}

const homeReducer = (state = initialState, action: any) => {
    switch (action.type) {
        case 'SET_PRODUCTS':
            state.products = action.payload
            return state
        default:
            return state;
    }
};

export default homeReducer;
