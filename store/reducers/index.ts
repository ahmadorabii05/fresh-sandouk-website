// src/store/reducers/index.ts

import { combineReducers } from 'redux';
import cartReducer from './cartReducer';
import homeReducer from './homeReducer';
import favoritesReducer from './favoritesReducer';
import userReducer from './userReducer';

const rootReducer = combineReducers({
  // Add your reducers here
  home: homeReducer,
  cart:cartReducer,
  favorites:favoritesReducer,
  user:userReducer

});

export default rootReducer;
