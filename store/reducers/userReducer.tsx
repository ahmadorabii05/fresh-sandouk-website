import { faker } from "@faker-js/faker";
import Cookies from "js-cookie";

const initialState: any = {
  user: JSON.parse(
    Cookies.get("user") ||
      JSON.stringify({ id: "", info: { general: { name: "" } } })
  ),
  token: Cookies.get("token"),
};
// Define your reducer

export const signoutUser = (item:any) => (dispatch:any, getState:any) => {
  dispatch({
    type: 'REMOVE_USER',
    payload: item
  });
  window.location.reload()

};

export const saveUser = (data: any):any => (dispatch: any, getState: any) => {
  dispatch({
    type: "SET_USER",
    payload: data,
  });
};

const userReducer = (state = initialState, action: any) => {
  switch (action.type) {
    case "REMOVE_USER":
      Cookies.remove("token");
      Cookies.remove("user");
      state.user = { id: "", info: { general: { name: "" } } };
      state.token = '';
      return state;
    case "SET_USER":
      if(action.payload.token){
        Cookies.set("token", action.payload.token);
        state.token = action.payload.token;
      }
      Cookies.set("user", JSON.stringify(action.payload.user));
      state.user = action.payload.user;
      return state;
    default:
      return state;
  }
};

export default userReducer;
